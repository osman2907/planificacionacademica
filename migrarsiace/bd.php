<?php
class postgres{
	public $servidor="localhost";
    public $puerto="5432";
    public $db;
    public $usuario="postgres";
    public $clave="postgres";
    public $conexion;
    public $sql;
    public $resultado;


    //Método que realiza una conexión a la base de datos Mysql...
    public function conectar(){
    	$cadenaConexion = "host=$this->servidor 
                            port=$this->puerto 
                            dbname=$this->db 
                            user=$this->usuario 
                            password=$this->clave";
        $this->conexion = pg_connect($cadenaConexion) or die ("Error de conexion. ". pg_last_error());
    }

    //Método que ejecuta una consulta Mysql
    public function ejecutar(){
    	$this->conectar();
        $this->resultado=pg_query($this->conexion,$this->sql) or die($this->sql."<br><br>Error: ".pg_last_error());
        $this->cerrarConexion();
    }

    public function obtenerRegistro(){
        $registro=pg_fetch_array($this->resultado);
        return $registro;
    }


    //Método que cierra una conexión Mysql
    public function cerrarConexion(){
    	pg_close($this->conexion);
    }
}
?>