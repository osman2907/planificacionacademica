<?php
require_once("bd.php");

$siace=new postgres();
$siace->db="ngonzale_siace";

$sigepsi=new postgres();
$sigepsi->db="ngonzale_sigepsi";

$siace->sql="SELECT * FROM estudiantes WHERE cod_estatus IN(1,2,4) AND cod_carrera IN (1,4,5,7,8,9,88,94,95,99)";
$siace->ejecutar();

$estudiantesSiace=$siace->resultado;
while($estudiante=pg_fetch_array($estudiantesSiace)){
	$numEst=$estudiante['num_est'];
	$cedula=$estudiante['cedula'];
	$nombres=explode(" ",$estudiante['nombres']);
	$apellidos=explode(" ",$estudiante['apellidos']);
	$primerNombre=$nombres[0];
	$segundoNombre=isset($nombres[1])?$nombres[1]:'';
	$primerApellido=$apellidos[0];
	$segundoApellido=isset($apellidos[1])?$apellidos[1]:'';
	$fec_nac=explode("/",$estudiante['fec_nac']);
	$fec_nac=$fec_nac[2]."-".$fec_nac[1]."-".$fec_nac[0];
	$fec_nac=date("Y-m-d",strtotime($fec_nac));
	$telefonoCelular=$estudiante['celular'];
	$telefonoLocal=$estudiante['tel_hab'];
	$correo=$estudiante['correo'];
	$codCarrera=$estudiante['cod_carrera'];

	$sigepsi->sql="SELECT * FROM personas where cedula=$cedula";
	$sigepsi->ejecutar();

	if(pg_num_rows($sigepsi->resultado) == 0){
		$siace->sql="SELECT * FROM usu_estudiantes WHERE num_est=$numEst";
		$siace->ejecutar();

		if(pg_num_rows($siace->resultado) > 0){
			$usuarioEstudiante=$siace->obtenerRegistro();
			$usuario=$usuarioEstudiante['usuario'];
			$clave=$usuarioEstudiante['clave'];
			$registro=strtotime(date("Y-m-d"));

			$sigepsi->sql="INSERT INTO cruge_user(regdate,username,email,password,state) VALUES($registro,'$usuario','$correo','$clave',1)";
			$sigepsi->ejecutar();

			$sigepsi->sql="SELECT * FROM cruge_user WHERE username='$usuario'";
			$sigepsi->ejecutar();
			$usuarioCruge=$sigepsi->obtenerRegistro();
			$userId=$usuarioCruge['iduser'];

			$sigepsi->sql="INSERT INTO cruge_authassignment(userid,data,itemname) VALUES($userId,'N;','estudiante')";
			$sigepsi->ejecutar();

			$sigepsi->sql="INSERT INTO personas(cedula,primer_nombre,segundo_nombre,primer_apellido,segundo_apellido,fecha_nac,telefono_celular,telefono_local,id_usuario,id_estatus) VALUES($cedula,'$primerNombre','$segundoNombre','$primerApellido','$segundoApellido','$fec_nac','$telefonoCelular','$telefonoLocal',$userId,1)";
			$sigepsi->ejecutar();

			$sigepsi->sql="SELECT * FROM personas WHERE cedula='$cedula'";
			$sigepsi->ejecutar();
			$datosPersona=$sigepsi->obtenerRegistro();
			$idPersona=$datosPersona['id_persona'];

			$sigepsi->sql="INSERT INTO estudiantes(id_persona,id_carrera) VALUES($idPersona,$codCarrera)";
			$sigepsi->ejecutar();
		}
	}	
}

echo "Ejecución Exitosa";
?>