<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public function LlenarSelect($lista){
		echo "<option value=''>Seleccione</option>";
		foreach ($lista as $clave => $valor){
			echo "<option value=$clave>$valor</option>";
		}
	}

	public function getExtension($archivo){
		$partes=explode(".",$archivo);
		$extension=$partes[count($partes)-1];
		return $extension;
	}

	public function cambiarFormatoFecha($fecha){
		if(empty($fecha)){
			return $fecha;
		}
        if(strpos($fecha,"/")){ //Si está en formato dd/mm/yyyy lo convierte a yyyy-mm-dd
            $fecha=explode("/",$fecha);
            $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0];
        }else{ //Si está en formato yyyy-mm-dd lo convierte a dd/mm/yyyy
            $fecha=explode("-",$fecha);
            $fecha=$fecha[2]."/".$fecha[1]."/".$fecha[0];
        }
        return $fecha;
    }

    public function convertirDia($dia){
    	switch ($dia){
    		case 1: return "Lunes"; break;
    		case 2: return "Martes"; break;
    		case 3: return "Miércoles"; break;
    		case 4: return "Jueves"; break;
    		case 5: return "Viernes"; break;
    		case 6: return "Sábado"; break;
    	}
    }

	public function enviarMensaje($telefono,$mensaje){
		/*$model=Configuraciones::model()->findByPk(1);
		$ipServidor=$model->ip_mensaje;
		$invoke_url="http://$ipServidor:9090/sendsms";
		$cell_number = $telefono;
		$message = rawurlencode($mensaje);
		$response = file_get_contents($invoke_url . '?phone=' . $cell_number  . '&text=' . $message);*/
	}

	public function beforeAction($action) 
    {
        //Yii::log(__METHOD__ . ' isGuest: ' . (Yii::app()->getUser()->isGuest ? 'Si' : 'No'));

        // Cada vez que se ejecuta una acción se actualiza el tiempo de expiración
        // TODO: integrar con cruge o mejorar
        $sys = Yii::app()->user->um->getDefaultSystem();
        $duration = $sys->getn('sessionmaxdurationmins');
        // Encuentra la última sesión y actualiza la fecha de expiración
        $model = CrugeSession::model()->findLast(Yii::app()->user->id);
        if ($model != null) {
            $model->expire = CrugeUtil::makeExpirationDateTime($duration);
            $model->save();
        }

        return true;
    }

}