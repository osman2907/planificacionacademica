<?php

class PeriodosAcademicosController extends Controller{
	
	public $layout='//layouts/column1';

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	public function actionView($id){
		$model=$this->loadModel($id);
		$modelDepartamentos=Departamentos::model()->getModelDepartamentos();
		$this->render('view',compact('model','modelDepartamentos'));
	}

	
	public function actionCreate(){
		$model=new PeriodosAcademicos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PeriodosAcademicos'])){
			$model->attributes=$_POST['PeriodosAcademicos'];
			$model->fecha_inicio=$this->cambiarFormatoFecha($model->fecha_inicio);
			$model->fecha_fin=$this->cambiarFormatoFecha($model->fecha_fin);
			$model->fecha_inicio_plan=$this->cambiarFormatoFecha($model->fecha_inicio_plan);
			$model->fecha_fin_plan=$this->cambiarFormatoFecha($model->fecha_fin_plan);
			
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_periodo));
			}else{
				$model->fecha_inicio=$this->cambiarFormatoFecha($model->fecha_inicio);
				$model->fecha_fin=$this->cambiarFormatoFecha($model->fecha_fin);
				$model->fecha_inicio_plan=$this->cambiarFormatoFecha($model->fecha_inicio_plan);
				$model->fecha_fin_plan=$this->cambiarFormatoFecha($model->fecha_fin_plan);
			}
		}

		$listPeriodos=array("1"=>"I","2"=>"II","3"=>"III");
		$this->render('create',compact('model','listPeriodos'));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PeriodosAcademicos'])){
			$model->attributes=$_POST['PeriodosAcademicos'];
			$model->fecha_inicio=$this->cambiarFormatoFecha($model->fecha_inicio);
			$model->fecha_fin=$this->cambiarFormatoFecha($model->fecha_fin);
			$model->fecha_inicio_plan=$this->cambiarFormatoFecha($model->fecha_inicio_plan);
			$model->fecha_fin_plan=$this->cambiarFormatoFecha($model->fecha_fin_plan);
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_periodo));
			}
		}

		$listPeriodos=array("1"=>"I","2"=>"II","3"=>"III");
		$model->fecha_inicio=$this->cambiarFormatoFecha($model->fecha_inicio);
		$model->fecha_fin=$this->cambiarFormatoFecha($model->fecha_fin);
		$model->fecha_inicio_plan=$this->cambiarFormatoFecha($model->fecha_inicio_plan);
		$model->fecha_fin_plan=$this->cambiarFormatoFecha($model->fecha_fin_plan);
		$modelDepartamentos=Departamentos::model()->getModelDepartamentos();
		$this->render('update',compact('model','listPeriodos','modelDepartamentos'));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('PeriodosAcademicos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){
		$model=new PeriodosAcademicos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PeriodosAcademicos']))
			$model->attributes=$_GET['PeriodosAcademicos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	
	public function loadModel($id){
		$model=PeriodosAcademicos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='periodos-academicos-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionUpdateDocentes($id=null){
		$modelPeriodosDocentes=new PeriodosAcademicosDocentes;
		if($modelPeriodosDocentes->actualizarDocentes($id)){
			Yii::app()->user->setFlash('success', "Docentes Actualizados con éxito");
		}else{
			Yii::app()->user->setFlash('danger', "Sucedió un error actualizando los docentes. Por favor intente de nuevo.");
		}
		$this->redirect(array('view','id'=>$id));
	}


	public function actionUpdateSecciones($id=null){
		$modelPeriodosSecciones=new PeriodosAcademicosSecciones;
		if($modelPeriodosSecciones->actualizarSecciones($id)){
			Yii::app()->user->setFlash('success', "Secciones Actualizadas con éxito");
		}else{
			Yii::app()->user->setFlash('danger', "Sucedió un error actualizando las secciones. Por favor intente de nuevo.");
		}
		$this->redirect(array('view','id'=>$id));
	}


	public function actionEliminarPeriodoSeccion(){
		$idPeriodo=$_POST['id_periodo'];
		$idSeccion=$_POST['id_seccion'];

		$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion";
		$consultaHorarios=Horarios::model()->findAll($options);

		if(count($consultaHorarios) > 0){
			echo "Esta sección ya tiene horarios cargados por favor elimine la carga horaria para poder eliminar";
			exit();
		}

		$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion";
		$modelPeriodosSecciones=PeriodosAcademicosSecciones::model()->find($options);
		$modelPeriodosSecciones->delete();
		echo false;
	}


	public function actionEliminarPeriodoDocente(){
		$idPeriodo=$_POST['id_periodo'];
		$idDocente=$_POST['id_docente'];

		$options['condition']="id_periodo=$idPeriodo AND id_docente=$idDocente";
		$consultaHorarios=Horarios::model()->findAll($options);

		if(count($consultaHorarios) > 0){
			echo "Este docente ya tiene horarios cargados por favor elimine la carga horaria para poder eliminar";
			exit();
		}

		$options['condition']="id_periodo=$idPeriodo AND id_docente=$idDocente";
		$modelPeriodosDocentes=PeriodosAcademicosDocentes::model()->find($options);
		$modelPeriodosDocentes->delete();
		echo false;
	}


	public function actionModificarPeriodoSeccion($idPeriodo=null, $idSeccion=null){
		$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion";
		$model=PeriodosAcademicosSecciones::model()->find($options);
		$model->periodo=$model->idPeriodo->anioPeriodo;
		$model->seccion=$model->idSeccion->seccion;
		$model->trayecto=$model->idSeccion->trayecto;

		if(isset($_POST['PeriodosAcademicosSecciones'])){
			$model->attributes=$_POST['PeriodosAcademicosSecciones'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Sección actualizada con éxito");
			}else{
				Yii::app()->user->setFlash('danger', "Ocurrió un error modificando la sección");
			}
		}

		$listMallas=Mallas::model()->getMallasActivas($model->idSeccion->id_departamento);
		$listMallas=CHtml::listData($listMallas,'id_malla','nombre_malla');
		$this->render("modificarPeriodoSeccion",compact('model','idPeriodo','idSeccion','listMallas'));
	}


	public function actionModificarPeriodoDocente($idPeriodo=null, $idDocente=null){
		$options['condition']="id_periodo=$idPeriodo AND id_docente=$idDocente";
		$model=PeriodosAcademicosDocentes::model()->find($options);
		$model->periodo=$model->idPeriodo->anioPeriodo;
		$model->docente=$model->idDocente->nombreCompleto;
		$model->departamento=$model->idDocente->idDepartamento->departamento;

		if(isset($_POST['PeriodosAcademicosDocentes'])){
			$model->attributes=$_POST['PeriodosAcademicosDocentes'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Docente actualizado con éxito");
			}else{
				Yii::app()->user->setFlash('danger', "Ocurrió un error modificando el docente");
			}
		}

		$listTiposContratos=TiposContratos::getTiposContratos(true);
		$listDedicaciones=DedicacionesDocentes::getDedicacionesDocentes(true);
		$listCategorias=CategoriasDocentes::getCategoriasDocentes(true);

		$this->render("modificarPeriodoDocente",compact('model','idPeriodo','idDocente','listTiposContratos','listDedicaciones','listCategorias'));
	}

}
