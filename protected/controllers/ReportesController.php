<?php
class ReportesController extends Controller{
	
	public $layout='//layouts/column1';

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}


	public function actionHorariosSecciones(){
		$model=new Horarios;
		
		if(isset($_POST['Horarios'])){
			$horarios=$this->horariosSecciones($_POST['Horarios']);
		}

		$listPeriodos=PeriodosAcademicos::model()->getPeriodosAcademicos();
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('horariosSecciones',compact('model','listPeriodos','listDepartamentos','horarios'));
	}

	public function horariosSecciones($post){
		$idPeriodo=$post['id_periodo'];
		$idDepartamento=$post['id_departamento'];
		$trayectoInicial=$post['trayecto_inicial'];

		$listHoras=Horas::model()->getHorasClases();
		$listSecciones=Horarios::model()->getSeccionesPeriodos($idPeriodo,$idDepartamento,$trayectoInicial);

		$html=$this->renderPartial('_horariosSecciones',compact('listHoras','listSecciones'),true);
		return $html;
	}


	public function actionHorariosAulas(){
		$model=new Horarios;
		
		if(isset($_POST['Horarios'])){
			$horarios=$this->horariosAulas($_POST['Horarios']);
		}

		$listPeriodos=PeriodosAcademicos::model()->getPeriodosAcademicos();
		$listSedes=Sedes::getSedes(true);
		$this->render('horariosAulas',compact('model','listPeriodos','listSedes','horarios'));
	}


	public function horariosAulas($post){
		$idPeriodo=$post['id_periodo'];
		$idSede=$post['id_sede'];

		$listHoras=Horas::model()->getHorasClases();
		$listAulas=Horarios::model()->getAulasPeriodos($idPeriodo,$idSede);

		$html=$this->renderPartial('_horariosAulas',compact('listHoras','listAulas'),true);
		return $html;
	}


	public function actionHorariosDocentes(){
		$model=new Horarios;
		
		if(isset($_POST['Horarios'])){
			$horarios=$this->horariosDocentes($_POST['Horarios']);
		}

		$listPeriodos=PeriodosAcademicos::model()->getPeriodosAcademicos();
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('horariosDocentes',compact('model','listPeriodos','listDepartamentos','horarios'));
	}

	public function horariosDocentes($post){
		$idPeriodo=$post['id_periodo'];
		$idDepartamento=$post['id_departamento'];

		$listHoras=Horas::model()->getHorasClases();
		$listDocentes=Horarios::model()->getDocentesPeriodos($idPeriodo,$idDepartamento);

		$html=$this->renderPartial('_horariosDocentes',compact('listHoras','listDocentes'),true);
		return $html;
	}


	public function horariosDocentesDesglosado($post){
		$idPeriodo=$post['id_periodo'];
		$idDepartamento=$post['id_departamento'];

		$listHoras=Horas::model()->getHorasClases();
		$listDocentes=Horarios::model()->getDocentesPeriodos($idPeriodo,$idDepartamento);

		$this->renderPartial('_horariosDocentesDesglosado',compact('listHoras','listDocentes'));
	}



	public function actionAuditoriaDocente(){
		$model=new Horarios;
		
		if(isset($_POST['Horarios'])){
			$auditoria=$this->auditoriaDocente($_POST['Horarios']);
		}

		$listPeriodos=PeriodosAcademicos::model()->getPeriodosAcademicos();
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('auditoriaDocente',compact('model','listPeriodos','listDepartamentos','auditoria'));
	}


	public function auditoriaDocente($post){
		$idPeriodo=$post['id_periodo'];
		$idDepartamento=$post['id_departamento'];

		if(empty($idDepartamento)){
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos();
		}else{
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos($idDepartamento);
		}


		$html=$this->renderPartial('_auditoriaDocente',compact('listDepartamentos','post'),true);
		return $html;
	}


	public function actionCargaHorariaDocente(){
		$model=new Horarios;
		
		if(isset($_POST['Horarios'])){
			$cargaHorariaDocente=$this->cargaHorariaDocente($_POST['Horarios']);
		}

		$listPeriodos=PeriodosAcademicos::model()->getPeriodosAcademicos();
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$listTiposContratos=TiposContratos::getTiposContratos(true);
		$this->render('cargaHorariaDocente',compact('model','listPeriodos','listDepartamentos','listTiposContratos','cargaHorariaDocente'));
	}


	public function cargaHorariaDocente($post){
		$idPeriodo=$post['id_periodo'];
		$idDepartamento=$post['id_departamento'];
		$idTipoContrato=$post['id_tipo_contrato'];

		if(empty($idDepartamento)){
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos();
		}else{
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos($idDepartamento);
		}


		$html=$this->renderPartial('_cargaHorariaDocente',compact('listDepartamentos','post'),true);
		return $html;
	}


	public function actionVacantes(){
		$model=new Horarios;
		
		if(isset($_POST['Horarios'])){
			$vacantes=$this->vacantes($_POST['Horarios']);
		}

		$listPeriodos=PeriodosAcademicos::model()->getPeriodosAcademicos();
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('vacantes',compact('model','listPeriodos','listDepartamentos','vacantes'));
	}


	public function vacantes($post){
		$idPeriodo=$post['id_periodo'];
		$idDepartamento=$post['id_departamento'];

		if(empty($idDepartamento)){
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos();
		}else{
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos($idDepartamento);
		}

		$html=$this->renderPartial('_vacantes',compact('listDepartamentos','post'),true);
		return $html;
	}


	public function actionDocentesSinProvidencia(){
		$model=new Horarios;
		
		if(isset($_POST['Horarios'])){
			$docentesSinProvidencia=$this->docentesSinProvidencia($_POST['Horarios']);
		}

		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('docentesSinProvidencia',compact('model','listDepartamentos','docentesSinProvidencia'));
	}


	public function docentesSinProvidencia($post){
		$idDepartamento=$post['id_departamento'];

		if(empty($idDepartamento)){
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos();
		}else{
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos($idDepartamento);
		}

		$html=$this->renderPartial('_docentesSinProvidencia',compact('listDepartamentos','post'),true);
		return $html;
	}


	public function actionImprimirHorariosSecciones($idPeriodo,$idDepartamento){
		$mpdf = Yii::app()->ePdf->mpdf();
		$mpdf = new mPDF();
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/reportes.css');
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->AddPage('L',
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            5, // margin bottom
            18, // margin header
            12); // margin footer
		$parametros=array('id_periodo'=>$idPeriodo,'id_departamento'=>$idDepartamento);
		$mpdf->WriteHTML($this->horariosSecciones($parametros));
		$mpdf->Output("HorariosSecciones.pdf","D");
	}


	public function actionImprimirHorariosAulas($idPeriodo,$idSede){
		$mpdf = Yii::app()->ePdf->mpdf();
		$mpdf = new mPDF();
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/reportes.css');
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->AddPage('L',
            '', '', '', '',
            5, // margin_left
            5, // margin right
            5, // margin top
            5, // margin bottom
            18, // margin header
            12); // margin footer
		$parametros=array('id_periodo'=>$idPeriodo,'id_sede'=>$idSede);
		$mpdf->WriteHTML($this->horariosAulas($parametros));
		$mpdf->Output("HorariosAulas","D");
	}


	public function actionImprimirHorariosDocentes($idPeriodo,$idDepartamento){
		$mpdf = Yii::app()->ePdf->mpdf();
		$mpdf = new mPDF();
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/reportes.css');
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->AddPage('L',
            '', '', '', '',
            5, // margin_left
            5, // margin right
            5, // margin top
            5, // margin bottom
            18, // margin header
            12); // margin footer
		$parametros=array('id_periodo'=>$idPeriodo,'id_departamento'=>$idDepartamento);
		$mpdf->WriteHTML($this->horariosDocentes($parametros));
		$mpdf->Output("HorariosDocentes","D");
	}


	public function actionImprimirHorariosDocentesDesglosado($idPeriodo,$idDepartamento){
		$parametros=array('id_periodo'=>$idPeriodo,'id_departamento'=>$idDepartamento);
		$this->horariosDocentesDesglosado($parametros);
	}

	public function actionImprimirCargaHorariaDocente($idPeriodo,$idDepartamento,$idTipoContrato){
		$mpdf = Yii::app()->ePdf->mpdf();
		$mpdf = new mPDF();
		$header="<img src='".Yii::app()->request->baseUrl."/images/membrete-iutoms.jpg' width='100%'>";
		$header.="<br><b><span style='font-size: 8pt'>Fecha del Reporte: ".date("d/m/Y")."</span></b>";
		$mpdf->SetHTMLHeader($header);
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/reportes.css');
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->AddPage('P',
            '', '', '', '',
            20, // margin_left
            20, // margin right
            20, // margin top
            20, // margin bottom
            5, // margin header
            12); // margin footer
		$parametros=array('id_periodo'=>$idPeriodo,'id_departamento'=>$idDepartamento,'id_tipo_contrato'=>$idTipoContrato);
		$mpdf->WriteHTML($this->cargaHorariaDocente($parametros));
		$mpdf->Output("CargaAcademica.pdf","D");
	}


	public function actionExcelAuditoria($idPeriodo,$idDepartamento){
		if(empty($idDepartamento)){
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos();
		}else{
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos($idDepartamento);
		}
		$this->renderPartial("excelAuditoria",compact('idPeriodo','listDepartamentos'));
	}


	public function actionExcelDocentesSinProvidencia($idDepartamento){
		if(empty($idDepartamento)){
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos();
		}else{
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos($idDepartamento);
		}
		$this->renderPartial("excelDocentesSinProvidencia",compact('listDepartamentos'));
	}


	public function actionExcelVacantes($idPeriodo,$idDepartamento){
		if(empty($idDepartamento)){
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos();
		}else{
			$listDepartamentos=Departamentos::model()->obtenerDepartamentos($idDepartamento);
		}
		$this->renderPartial("excelVacantes",compact('idPeriodo','listDepartamentos'));
	}

}