<?php

class TurnosController extends Controller{

	public $layout='//layouts/column1';

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	
	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate(){
		$model=new Turnos;

		// $this->performAjaxValidation($model);

		if(isset($_POST['Turnos'])){
			$model->attributes=$_POST['Turnos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_turno));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Turnos'])){
			$model->attributes=$_POST['Turnos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_turno));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Turnos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){
		$model=new Turnos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Turnos']))
			$model->attributes=$_GET['Turnos'];
		$model->dbCriteria->order="id_turno ASC";
		$listStatus=array("1"=>"ACTIVO","0"=>"INACTIVO");

		$this->render('admin',compact('model','listStatus'));
	}

	
	public function loadModel($id){
		$model=Turnos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='turnos-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
