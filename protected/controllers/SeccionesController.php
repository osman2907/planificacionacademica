<?php
class SeccionesController extends Controller{

	public $layout='//layouts/column1';


	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	
	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate(){
		$model=new Secciones;

		// $this->performAjaxValidation($model);

		if(isset($_POST['Secciones'])){
			$model->attributes=$_POST['Secciones'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_seccion));
		}

		$listDepartamentos=Departamentos::getDepartamentos(TRUE);
		$listTurnos=Turnos::getTurnos(TRUE);
		$this->render('create',compact('model','listDepartamentos','listTurnos'));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Secciones'])){
			$model->attributes=$_POST['Secciones'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_seccion));
		}

		$listDepartamentos=Departamentos::getDepartamentos(TRUE);
		$listTurnos=Turnos::getTurnos(TRUE);
		$this->render('update',compact('model','listDepartamentos','listTurnos'));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Secciones');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){
		$model=new Secciones('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Secciones']))
			$model->attributes=$_GET['Secciones'];
		$model->dbCriteria->order="id_departamento ASC, trayecto ASC, id_turno ASC, seccion ASC";

		$listStatus=array("1"=>"ACTIVO","0"=>"INACTIVO");
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$listTurnos=Turnos::getTurnos(true);
		$this->render('admin',compact('model','listStatus','listDepartamentos','listTurnos'));
	}

	
	public function loadModel($id){
		$model=Secciones::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='secciones-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
