<?php
class HorariosController extends Controller{
	
	public $layout='//layouts/column1';


	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}


	public function actionBusqueda(){
		$model=new Horarios;
		$model->setScenario('busqueda');

		if(isset($_POST['Horarios'])){
			$model->attributes=$_POST['Horarios'];
			$model->validate();

			if(count($model->errors) == 0){
				$this->redirect(array('admin','id_periodo'=>$model->id_periodo,'id_seccion'=>$model->id_seccion));
			}
		}

		$modelPeriodo=PeriodosAcademicos::model()->getPeriodoActivo();
		$listDepartamentos=Departamentos::getDepartamentos(true);
		if(!empty($model->id_departamento)){
			$listTrimestres=Horarios::model()->getListTrimestresDepartamentos($modelPeriodo->id_periodo,$model->id_departamento);
		}else{
			$listTrimestres=array();
		}

		if(!empty($model->trimestre)){
			$listSecciones=Horarios::model()->getListSeccionesDepartamentos($modelPeriodo->id_periodo,$model->id_departamento,$model->trimestre);
		}else{
			$listSecciones=array();
		}

		$this->render('busqueda',compact("model","modelPeriodo","listDepartamentos","listTrimestres","listSecciones"));
	}

	
	public function actionAdmin($id_periodo,$id_seccion){
		$model=new Horarios;
		$model->id_periodo=$id_periodo;
		$model->id_seccion=$id_seccion;
		$horas=Horas::getHorasClases();
		$horas=json_encode($horas);
		$modelPeriodo=PeriodosAcademicos::model()->findByPk($id_periodo);
		$options['condition']="id_periodo=$id_periodo AND id_seccion=$id_seccion";
		$modelPeriodoSeccion=PeriodosAcademicosSecciones::model()->find($options);
		$listEventos=$model->getConsultarEventos();
		$listEventos=json_encode($listEventos);

		$this->render('admin',compact('model','horas','modelPeriodo','modelPeriodoSeccion','listEventos'));
	}


	public function actionListarTrimestresDepartamentos(){
		$idPeriodo=$_POST['id_periodo'];
		$idDepartamento=$_POST['id_departamento'];
		$trimestres=Horarios::model()->getTrimestresDepartamentos($idPeriodo,$idDepartamento);
		if(count($trimestres) > 0){
			echo "<option value=''>SELECCIONE</option>";
			foreach ($trimestres as $elemento) {
				$trimestre=$elemento['trimestre'];
				echo "<option value='$trimestre'>$trimestre</option>";
			}
		}
	}


	public function actionListarSeccionesDepartamentos(){
		$idPeriodo=$_POST['id_periodo'];
		$idDepartamento=$_POST['id_departamento'];
		$trimestre=$_POST['trimestre'];
		$secciones=Horarios::model()->getSeccionesDepartamentos($idPeriodo,$idDepartamento,$trimestre);
		if(count($secciones) > 0){
			echo "<option value=''>SELECCIONE</option>";
			foreach ($secciones as $elemento) {
				$idSeccion=$elemento['id_seccion'];
				$seccion=$elemento['seccion'];
				echo "<option value='$idSeccion'>$seccion</option>";
			}
		}
	}


	public function actionRegistrarHorario(){
		$idPeriodo=$_POST['id_periodo'];
		$idSeccion=$_POST['id_seccion'];
		$dia=$_POST['dia'];
		$hora=$_POST['hora'];

		$model=new Horarios;
		$model->id_periodo=$idPeriodo;
		$model->id_seccion=$idSeccion;
		$model->id_dia=$dia;
		$model->dia=$this->convertirDia($dia);
		$model->cantidad_horas=1;
		

		$model->SetScenario('registrarHorario');
		$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion";
		$modelPeriodoSeccion=PeriodosAcademicosSecciones::model()->find($options);
		$listHorasInicio=Horas::model()->getListHorasInicio($hora);
		$model->hora_inicio=date("H:i:s",strtotime(current($listHorasInicio)));
		$model->hora_fin=date("h:i a",(strtotime($model->hora_inicio))+(45*60));
		$listAsignaturas=Asignaturas::model()->getListAsignaturasMallas($modelPeriodoSeccion->id_malla);
		$listDocentes=Docentes::model()->getListDocentes();
		$listAulas=Aulas::model()->getListAulas(TRUE);

		$this->renderPartial('_registrarHorario',compact('model','modelPeriodoSeccion','listHorasInicio','listAsignaturas','listDocentes','listAulas'));
	}


	public function actionValidarRegistrarHorario(){
		$model=new Horarios;
		$model->setScenario('registrarHorario');
		$model->attributes=$_POST['Horarios'];
		$model->validate();
		if(count($model->errors) > 0){
			$errores=$model->errors;
			$error=current($errores);
			echo $error[0];
			exit();
		}

		if(!$model->validarHoraSeccion($_POST['Horarios'])){
			echo "No es posible la operación, usted está tratando de registrar una hora que se encuentra ocupada en esta sección";
			exit();
		}

		if(!empty($_POST['Horarios']['id_docente'])){
			if(!$model->validarHoraDocente($_POST['Horarios'])){
				echo "No es posible la operación, usted está tratando de registrar una hora que se encuentra ocupada para este docente.";
				exit();
			}
		}

		if(!empty($_POST['Horarios']['id_aula'])){
			if(!$model->validarHoraAula($_POST['Horarios'])){
				echo "No es posible la operación, usted está tratando de registrar una hora que se encuentra ocupada para esta aula.";
				exit();
			}
		}
	}


	public function actionRegistrarHoras(){
		$registro=Horarios::model()->registrarHoras($_POST);
		if($registro){
			$evento=Horarios::model()->getEventoCalendario($_POST);
			echo json_encode($evento);
		}
	}


	public function actionModificarHorario(){
		$idPeriodo=$_POST['id_periodo'];
		$idSeccion=$_POST['id_seccion'];
		$id_dia=$_POST['id_dia'];
		$id_hora=$_POST['id_hora'];
		$cantidadHoras=$_POST['cantidad_horas'];
		$_POST['id_docente_old']=$_POST['id_docente'];
		$_POST['id_aula_old']=$_POST['id_aula'];

		$model=Horarios::model()->validarHorarios($_POST);
		if(!$model){
			throw new CHttpException(403,'Otro usuario actualizó el horario desde otra sesión, intente nuevamente');
		}
		$listHoras=Horas::model()->getHorasClases();
		$model->hora_inicio=date("h:i a",strtotime($listHoras[$model->id_hora-1]['start']));
		$model->hora_fin=date("h:i a",strtotime($listHoras[$model->id_hora+($cantidadHoras-2)]['end']));
		$model->cantidad_horas=$cantidadHoras;
		$model->dia=$this->convertirDia($id_dia);
		$model->asignatura=$model->idAsignatura->codigoAsignatura;

		$model->SetScenario('modificarHorario');
		$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion";
		$modelPeriodoSeccion=PeriodosAcademicosSecciones::model()->find($options);
		$listDocentes=Docentes::model()->getListDocentes();
		$listAulas=Aulas::model()->getListAulas(TRUE);

		$this->renderPartial('_modificarHorario',compact('model','modelPeriodoSeccion','listDocentes','listAulas'));
	}


	public function actionValidarModificarHorario(){
		$model=new Horarios;
		$model->setScenario('modificarHorario');
		$model->attributes=$_POST['Horarios'];
		$model->validate();
		if(count($model->errors) > 0){
			$errores=$model->errors;
			$error=current($errores);
			echo $error[0];
		}

		/*if(!empty($_POST['Horarios']['id_docente'])){
			if(!$model->validarHoraDocenteModificar($_POST['Horarios'])){
				echo "No es posible la operación, usted está tratando de registrar una hora que se encuentra ocupada para este docente";
				exit();
			}
		}*/


		if(!empty($_POST['Horarios']['id_aula'])){
			if(!$model->validarHoraAulaModificar($_POST['Horarios'])){
				echo "No es posible la operación, usted está tratando de registrar una hora que se encuentra ocupada para esta aula.";
				exit();
			}
		}

	}


	public function actionModificarHoras(){
		$model=Horarios::model()->validarHorarios($_POST['Horarios']);
		if(!$model){
			throw new CHttpException(403,'Otro usuario actualizó el horario desde otra sesión, intente nuevamente');
		}
		$modificacion=Horarios::model()->modificarHoras($_POST);
		if(!$modificacion){
			throw new CHttpException(403,'Ocurrió un error modificando horas, intente nuevamente');
		}
	}


	public function actionEliminarHoras(){
		$model=Horarios::model()->validarHorarios($_POST['Horarios']);
		if(!$model){
			throw new CHttpException(403,'Otro usuario actualizó el horario desde otra sesión, intente nuevamente');
		}
		$eliminacion=Horarios::model()->eliminarHoras($_POST);
		if(!$eliminacion){
			throw new CHttpException(403,'Ocurrió un error eliminando horas, intente nuevamente');
		}
	}


	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='horarios-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
