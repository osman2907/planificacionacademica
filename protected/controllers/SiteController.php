<?php
class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	* Logs out the current user and redirect to homepage.
	*/
	public function actionLogout(){
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionValidarSesion(){
		echo !Yii::app()->user->isGuest;
	}

	/*public function actionRegistroEstudiante(){
		$modelPersonas=new Personas;

		if(isset($_POST['cedula'])){
			$cedula=$_POST['cedula'];
			$modelEstudiantesSiace=EstudiantesSiace::model()->find(array('condition'=>"cedula='$cedula'"));
			$modelPersonas=Personas::model()->find(array('condition'=>"cedula='$cedula'"));
			if(!isset($modelPersonas)){
				$modelPersonas=new Personas;
			}
			$modelPersonas->setScenario('registrarEstudiante');
			$this->renderPartial("formularioRegistroEstudiante",compact('modelEstudiantesSiace','modelPersonas'));
			exit();
		}

		if(isset($_POST['Personas'])){
			$modelPersonas->setScenario('registrarEstudiante');
			$modelPersonas->attributes=$_POST['Personas'];
			if($modelPersonas->validate()){
				$userModel=new CrugeUserModel;
				$userModel->attributes=$_POST['Personas'];
				$userModel->regdate=strtotime(date("Y-m-d H:i:s"));
				$userModel->password=md5($userModel->password);
				$userModel->state=1;
				$userModel->save();
				
				$userAsignaModel=new CrugeAuthassignment;
				$userAsignaModel->itemname="estudiante";
				$userAsignaModel->data="N;";
				$userAsignaModel->userid=$userModel->iduser;
				$userAsignaModel->save();

				$modelPersonas->id_usuario=$userModel->iduser;
				$modelPersonas->username=1;
				$modelPersonas->id_estatus=1;
				$modelPersonas->save();

				$modelEstudiantes=new Estudiantes;
				$modelEstudiantes->id_persona=$modelPersonas->id_persona;
				$modelEstudiantes->id_carrera=$_POST['Personas']['id_carrera'];
				$modelEstudiantes->save();

				$this->redirect(array('verRegistroEstudiante','id'=>$modelPersonas->id_persona));
			}
		}

		$this->render('registroEstudiante');
	}*/

	public function actionVerRegistroEstudiante($id){
		$model=Personas::model()->findByPk($id);
		$this->render("verRegistroEstudiante",compact('model'));
	}

	public function actionListaCarreras(){
		$idProcedencia=$_POST['id_procedencia'];
		$listaCarreras=Carreras::getCarreras($idProcedencia);
		$this->LlenarSelect($listaCarreras);
	}

	/*public function actionRegistroComunidad(){
		$model=new Comunidades;
		$model->setScenario('registroComunidad');
		$this->performAjaxValidation($model);

		if(isset($_POST['Comunidades'])){
			$model->attributes=$_POST['Comunidades'];
			if($model->validate()){
				$userModel=new CrugeUserModel;
				$userModel->attributes=$_POST['Comunidades'];
				$userModel->regdate=strtotime(date("Y-m-d H:i:s"));
				$userModel->password=md5($userModel->password);
				$userModel->state=1;
				$userModel->save();
				
				$userAsignaModel=new CrugeAuthassignment;
				$userAsignaModel->itemname="comunidad";
				$userAsignaModel->data="N;";
				$userAsignaModel->userid=$userModel->iduser;
				$userAsignaModel->save();

				$model->id_usuario=$userModel->iduser;
				$model->username=1;
				$model->id_estatus=1;
				$model->save();
				$this->redirect(array('verRegistroComunidad','id'=>$model->id_comunidad));
			}
		}

		$listaTiposComunidades=TiposComunidades::getTiposComunidades();
		$listaEstados=Estados::getEstados();
		$this->render('registroComunidad',compact('model','listaTiposComunidades','listaEstados'));
	}*/

	public function actionVerRegistroComunidad($id){
		$model=Comunidades::model()->findByPk($id);
		$this->render("verRegistroComunidad",compact('model'));
	}

	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && ($_POST['ajax']==='registro-estudiantes-form' || $_POST['ajax']==='registro-comunidades-form')){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionValidarRegistroEstudiante(){
		$model=new Personas;
		$model->setScenario('registrarEstudiante');
		$model->attributes=$_POST['Personas'];
		$model->validate();
		if(count($model->errors) > 0){
			echo array_values($model->errors)[0][0];
		}
	}

	/*public function actionChangePassword(){
		$userId=Yii::app()->user->getid();
		$model=CrugeUserModel::model()->findByPk($userId);
		$model->password="";
		$this->render("changePassword",compact('model'));
	}*/

	public function actionValidarContrasena(){
		$usuario=$_POST['usuario'];
		$contrasena=md5($_POST['contrasena']);

		$model=CrugeUserModel::model()->findAll(array('condition'=>"username='$usuario' AND password='$contrasena'"));
		echo count($model);
	}

	public function actionCambiarContrasena(){
		$usuario=$_POST['usuario'];
		$contrasena=md5($_POST['contrasena']);

		$model=CrugeUserModel::model()->find(array('condition'=>"username='$usuario'"));
		$model->password=$contrasena;
		$model->save();
		echo 1;
	}

	public function actionRecuperar(){
		$this->render("recuperar");
	}

	public function actionValidarIdentificacion(){
		$identificacion=$_POST['identificacion'];

		if(is_numeric($identificacion)){
			$modelPersona=Personas::model()->findAll(array("condition"=>"cedula=$identificacion"));
			if(count($modelPersona) > 0){
				echo $modelPersona[0]->idUsuario->iduser;
				return false;
			}
		}

		$modelPersona=CrugeUserModel::model()->findAll(array("condition"=>"username='$identificacion'"));
		if(count($modelPersona) > 0){
			echo $modelPersona[0]->iduser;	
			return false;
		}
	}


	public function actionRestablecerClave(){
		$key = '';
		$longitud=6;
		$pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
		$max = strlen($pattern)-1;
		for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
		$nuevaClave=$key;

		$modo=$_POST['modo'];

		$idUsuario=$_POST['id_usuario'];
		$modelUsuario=CrugeUserModel::model()->findByPk($idUsuario);
		$modelUsuario->password=md5($nuevaClave);
		$modelUsuario->save();

		if($modo == 'mail'){
			$this->layout="correos";
			$cuerpo=$this->render("restablecerClave",compact('modelUsuario','nuevaClave'),true);

			$asunto="Recuperación de Contraseña";
			$destinatarios[]=$modelUsuario->email;
			Correos::enviarCorreo($asunto,$destinatarios,$cuerpo);
		}

		if($modo == 'sms'){
			$modelPersona=Personas::model()->find(array("condition"=>"id_usuario=$idUsuario"));
			$telefono=$modelPersona->telefono_celular;
			$mensaje="Servicio Comunitario IUTOMS informa que su usuario para ingresar al sistema es $modelUsuario->username y su nueva contraseña es $nuevaClave";
			$this->enviarMensaje($telefono,$mensaje);
		}
	}
}