<?php
class DedicacionesDocentesController extends Controller{
	
	public $layout='//layouts/column1';

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	
	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function actionCreate(){
		$model=new DedicacionesDocentes;

		// $this->performAjaxValidation($model);

		if(isset($_POST['DedicacionesDocentes'])){
			$model->attributes=$_POST['DedicacionesDocentes'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_dedicacion_docente));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['DedicacionesDocentes'])){
			$model->attributes=$_POST['DedicacionesDocentes'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_dedicacion_docente));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('DedicacionesDocentes');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){
		$model=new DedicacionesDocentes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DedicacionesDocentes']))
			$model->attributes=$_GET['DedicacionesDocentes'];
		$model->dbCriteria->order="id_dedicacion_docente ASC";

		$listStatus=array("1"=>"ACTIVO","0"=>"INACTIVO");
		$this->render('admin',compact('model','listStatus'));
	}

	
	public function loadModel($id){
		$model=DedicacionesDocentes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='dedicaciones-docentes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
