<?php

class AulasController extends Controller{
	
	public $layout='//layouts/column1';

	
	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}


	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate(){
		$model=new Aulas;

		// $this->performAjaxValidation($model);

		if(isset($_POST['Aulas'])){
			$model->attributes=$_POST['Aulas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_aula));
		}

		$listSedes=Sedes::getSedes(true);
		$this->render('create',compact('model','listSedes'));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Aulas'])){
			$model->attributes=$_POST['Aulas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_aula));
		}

		$listSedes=Sedes::getSedes(true);
		$this->render('update',compact('model','listSedes'));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Aulas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){
		$model=new Aulas('search');
		$model->unsetAttributes();
		if(isset($_GET['Aulas']))
			$model->attributes=$_GET['Aulas'];
		$model->dbCriteria->order="nombre_aula ASC";

		$listStatus=array("1"=>"ACTIVO","0"=>"INACTIVO");
		$listSedes=Sedes::getSedes(true);
		$this->render('admin',compact('model','listStatus','listSedes'));
	}


	public function loadModel($id){
		$model=Aulas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='aulas-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
