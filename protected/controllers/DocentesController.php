<?php
class DocentesController extends Controller{
	
	public $layout='//layouts/column1';


	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate(){
		$model=new Docentes;

		// $this->performAjaxValidation($model);

		if(isset($_POST['Docentes'])){
			$model->attributes=$_POST['Docentes'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Docente registrado con éxito");
				$this->redirect(array('view','id'=>$model->id_docente));
			}
		}

		$listDepartamentos=Departamentos::getDepartamentos(true);
		$listSexos=array("1"=>"MASCULINO","2"=>"FEMENINO");
		$listTiposContratos=TiposContratos::getTiposContratos(true);
		$listDedicaciones=DedicacionesDocentes::getDedicacionesDocentes(true);
		$listCategorias=CategoriasDocentes::getCategoriasDocentes(true);
		$listProvidencias=array("1"=>"Con Providencia","0"=>"Sin Providencia");
		$this->render('create',compact('model','listDepartamentos','listSexos','listTiposContratos','listDedicaciones','listCategorias','listProvidencias'));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Docentes'])){
			$model->attributes=$_POST['Docentes'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Docente modificado con éxito");
				$this->redirect(array('view','id'=>$model->id_docente));
			}
		}

		$listDepartamentos=Departamentos::getDepartamentos(true);
		$listSexos=array("1"=>"MASCULINO","2"=>"FEMENINO");
		$listTiposContratos=TiposContratos::getTiposContratos(true);
		$listDedicaciones=DedicacionesDocentes::getDedicacionesDocentes(true);
		$listCategorias=CategoriasDocentes::getCategoriasDocentes(true);
		$listProvidencias=array("1"=>"Con Providencia","0"=>"Sin Providencia");

		$model->providencia=!$model->providencia?0:1;
		$this->render('update',compact('model','listDepartamentos','listSexos','listTiposContratos','listDedicaciones','listCategorias','listProvidencias'));
	}


	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Docentes');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){
		$model=new Docentes('search');
		$model->unsetAttributes();
		if(isset($_GET['Docentes']))
			$model->attributes=$_GET['Docentes'];
		$model->dbCriteria->order="nombre1 ASC";

		$listSexos=array("1"=>"MASCULINO","2"=>"FEMENINO");
		$listStatus=array("1"=>"ACTIVO","0"=>"INACTIVO");
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('admin',compact('model','listSexos','listStatus','listDepartamentos'));
	}

	
	public function loadModel($id){
		$model=Docentes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	 
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='docentes-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
