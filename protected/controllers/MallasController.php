<?php
class MallasController extends Controller{
	
	public $layout='//layouts/column1';

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	
	public function actionView($id){
		$model=$this->loadModel($id);
		$options['condition']="id_malla=$id";
		$options['order']="trimestre ASC, asignatura ASC";
		$modelAsignaturas=Asignaturas::model()->findAll($options);
		
		$this->render('view',compact('model','modelAsignaturas'));
	}

	
	public function actionCreate(){
		$model=new Mallas;

		// $this->performAjaxValidation($model);

		if(isset($_POST['Mallas'])){
			$model->attributes=$_POST['Mallas'];
			$model->fecha_creacion=$this->cambiarFormatoFecha($model->fecha_creacion);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_malla));
		}

		$model->fecha_creacion=$this->cambiarFormatoFecha($model->fecha_creacion);

		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('create',compact('model','listDepartamentos'));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Mallas'])){
			$model->attributes=$_POST['Mallas'];
			$model->fecha_creacion=$this->cambiarFormatoFecha($model->fecha_creacion);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_malla));
		}

		$model->fecha_creacion=$this->cambiarFormatoFecha($model->fecha_creacion);

		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('update',compact('model','listDepartamentos'));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Mallas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){
		$model=new Mallas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Mallas']))
			$model->attributes=$_GET['Mallas'];
		$model->dbCriteria->order="id_status DESC, id_departamento ASC, fecha_creacion DESC";

		$listStatus=array("1"=>"ACTIVO","0"=>"INACTIVO");
		$listDepartamentos=Departamentos::getDepartamentos(true);
		$this->render('admin',compact('model','listStatus','listDepartamentos'));
	}

	
	public function loadModel($id){
		$model=Mallas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='mallas-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAsignaturas($id = null){
		$model=$this->loadModel($id);
		$options['condition']="id_malla=$id";
		$options['order']="trimestre ASC, asignatura ASC";
		$modelAsignaturas=Asignaturas::model()->findAll($options);
		$this->render('asignaturas',compact('model','modelAsignaturas'));
	}


	public function actionregistrarAsignaturas($id = null){
		$modelMalla=$this->loadModel($id);
		$model=new Asignaturas;
		$model->id_malla=$modelMalla->id_malla;

		if(isset($_POST['Asignaturas'])){
			$model->attributes=$_POST['Asignaturas'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Asignatura registrada con éxito");
				$this->redirect(array('viewAsignatura','id'=>$model->id_asignatura));
			}
		}

		$this->render('registrarAsignaturas',compact('model','modelMalla'));
	}


	public function actionViewAsignatura($id = null){
		$model=Asignaturas::model()->findByPk($id);
		$this->render('viewAsignatura',compact('model'));
	}


	public function actionUpdateAsignaturas($id = null){
		$model=Asignaturas::model()->findByPk($id);

		if(isset($_POST['Asignaturas'])){
			$model->attributes=$_POST['Asignaturas'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Asignatura Modificada con éxito");
				$this->redirect(array('viewAsignatura','id'=>$model->id_asignatura));
			}
		}
		
		$this->render('updateAsignaturas',compact('model'));
	}


	public function actionDeleteAsignaturas($id){
		$model=Asignaturas::model()->findByPk($id);
		$idMalla=$model->id_malla;
		$model->delete();
		$this->redirect(array('asignaturas','id'=>$idMalla));
	}

}
