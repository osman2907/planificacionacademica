<?php

class DepartamentosController extends Controller{

	public $layout='//layouts/column1';

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate(){
		$model=new Departamentos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Departamentos'])){
			$model->attributes=$_POST['Departamentos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_departamento));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Departamentos'])){
			$model->attributes=$_POST['Departamentos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_departamento));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Departamentos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin()
	{
		$model=new Departamentos('search');
		$model->unsetAttributes();
		if(isset($_GET['Departamentos']))
			$model->attributes=$_GET['Departamentos'];
		$model->dbCriteria->order="id_departamento ASC";

		$listStatus=array("1"=>"ACTIVO","0"=>"INACTIVO");
		$this->render('admin',compact('model','listStatus'));
	}

	
	public function loadModel($id){
		$model=Departamentos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='departamentos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
