<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aulas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'nombre_aula'); ?>
			<?php echo $form->textField($model,'nombre_aula',array('class'=>'form-control','maxlength'=>200)); ?>
			<?php echo $form->error($model,'nombre_aula'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'capacidad'); ?>
			<?php echo $form->textField($model,'capacidad',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'capacidad'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_sede'); ?>
			<?php echo $form->dropDownList($model,'id_sede',$listSedes,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_sede'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'piso'); ?>
			<?php echo $form->textField($model,'piso',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'piso'); ?>
		</div>

		<?php if(!$model->isNewRecord){ ?>
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkbox($model,'id_status'); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->