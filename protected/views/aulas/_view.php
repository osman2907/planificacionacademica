<?php
/* @var $this AulasController */
/* @var $data Aulas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_aula')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_aula), array('view', 'id'=>$data->id_aula)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_aula')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_aula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('capacidad')); ?>:</b>
	<?php echo CHtml::encode($data->capacidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sede')); ?>:</b>
	<?php echo CHtml::encode($data->id_sede); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('piso')); ?>:</b>
	<?php echo CHtml::encode($data->piso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />


</div>