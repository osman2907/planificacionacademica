<?php
$this->breadcrumbs=array(
	'Gestionar Aulas'=>array('admin'),
	'Registrar Aulas',
);
?>

<h1>Registrar Aulas</h1>

<?php $this->renderPartial('_form', compact('model','listSedes')); ?>