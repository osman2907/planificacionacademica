<?php
$this->breadcrumbs=array(
	'Gestionar Aulas'=>array('admin'),
	'Modificar Aulas',
);
?>

<h1>Modificar Aulas</h1>

<?php $this->renderPartial('_form', compact('model','listSedes')); ?>