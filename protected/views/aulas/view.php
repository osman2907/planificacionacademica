<?php
$this->breadcrumbs=array(
	'Gestionar Aulas'=>array('admin'),
	'Consultar Aulas',
);
?>

<h1>Consultar Aulas</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_aula',
		'nombre_aula',
		'capacidad',
		array(
			'name'=>'id_sede',
			'value'=>$model->idSede->sede
		),
		'piso',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Aulas',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
