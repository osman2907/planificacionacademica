<?php

$this->breadcrumbs=array(
	'Gestionar Aulas',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#aulas-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
var decorarGrilla=function(){
	$('.grid-view .items thead tr:first-child th:first-child').addClass('brsi');
    $('.grid-view .items thead tr:first-child th:last-child').addClass('brsd');
    $('.grid-view .items thead .filters td input').addClass('form-control');
    $('.grid-view .items thead .filters td select').addClass('form-control');
}
decorarGrilla();
");
?>

<h1>Gestionar Aulas</h1>

<?php echo CHtml::link(
			'Registrar Aulas',
			array('create'),
			array('class'=>'btn btn-success')
);
$template="{view}{update}";
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'aulas-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'decorarGrilla',
	'columns'=>array(
		/*array(
			'name'=>'id_aula',
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),*/
		'nombre_aula',
		'capacidad',
		array(
			'name'=>'id_sede',
			'type'=>'raw',
			'value'=>'$data->idSede->sede',
			'filter'=>$listSedes
		),
		'piso',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>'$data->getStatusBandeja()',
			'filter'=>$listStatus
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>$template,
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),
	),
)); ?>
