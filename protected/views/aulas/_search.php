<?php
/* @var $this AulasController */
/* @var $model Aulas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_aula'); ?>
		<?php echo $form->textField($model,'id_aula'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre_aula'); ?>
		<?php echo $form->textField($model,'nombre_aula',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'capacidad'); ?>
		<?php echo $form->textField($model,'capacidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_sede'); ?>
		<?php echo $form->textField($model,'id_sede'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'piso'); ?>
		<?php echo $form->textField($model,'piso',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_status'); ?>
		<?php echo $form->checkBox($model,'id_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->