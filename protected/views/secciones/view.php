<?php
$this->breadcrumbs=array(
	'Gestionar Secciones'=>array('admin'),
	'Consultar Secciones',
);
?>

<h1>Consultar Secciones</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'seccion',
		array(
			'name'=>'id_departamento',
			'value'=>$model->idDepartamento->departamento
		),
		array(
			'name'=>'id_turno',
			'value'=>$model->idTurno->turno
		),
		'trayecto',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Secciones',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
