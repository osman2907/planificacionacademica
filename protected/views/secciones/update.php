<?php
$this->breadcrumbs=array(
	'Gestionar Secciones'=>array('admin'),
	'Modificar Secciones',
);
?>

<h1>Modificar Secciones</h1>

<?php $this->renderPartial('_form', compact('model','listDepartamentos','listTurnos')); ?>