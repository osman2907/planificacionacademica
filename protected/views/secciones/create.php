<?php
$this->breadcrumbs=array(
	'Gestionar Secciones'=>array('admin'),
	'Registrar Secciones',
);
?>

<h1>Registrar Secciones</h1>

<?php $this->renderPartial('_form', compact('model','listDepartamentos','listTurnos')); ?>