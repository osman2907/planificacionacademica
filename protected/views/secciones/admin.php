<?php

$this->breadcrumbs=array(
	'Gestionar Secciones',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#secciones-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
var decorarGrilla=function(){
	$('.grid-view .items thead tr:first-child th:first-child').addClass('brsi');
    $('.grid-view .items thead tr:first-child th:last-child').addClass('brsd');
    $('.grid-view .items thead .filters td input').addClass('form-control');
    $('.grid-view .items thead .filters td select').addClass('form-control');
}
decorarGrilla();
");
?>

<h1>Gestionar Secciones</h1>

<?php echo CHtml::link(
			'Registrar Secciones',
			array('create'),
			array('class'=>'btn btn-success')
);
$template="{view}{update}";
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sedes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'decorarGrilla',
	'columns'=>array(
		/*array(
			'name'=>'id_sede',
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),*/
		'seccion',
		array(
			'name'=>'id_departamento',
			'value'=>'$data->idDepartamento->departamento',
			'filter'=>$listDepartamentos
		),
		array(
			'name'=>'id_turno',
			'value'=>'$data->idTurno->turno',
			'filter'=>$listTurnos
		),
		'trayecto',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>'$data->getStatusBandeja()',
			'filter'=>$listStatus
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>$template,
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),
	),
)); ?>
