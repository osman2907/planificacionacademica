<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'secciones-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'seccion'); ?>
			<?php echo $form->textField($model,'seccion',array('class'=>'form-control','maxlength'=>20)); ?>
			<?php echo $form->error($model,'seccion'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->dropDownList($model,'id_departamento',$listDepartamentos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_turno'); ?>
			<?php echo $form->dropDownList($model,'id_turno',$listTurnos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_turno'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'trayecto'); ?>
			<?php echo $form->textField($model,'trayecto',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'trayecto'); ?>
		</div>

		<?php if(!$model->isNewRecord){ ?>
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkbox($model,'id_status'); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->