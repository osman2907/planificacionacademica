<?php
/* @var $this TiposComunidadesController */
/* @var $model TiposComunidades */
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array('Auditoría','Seguimiento de Tablas'))
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#especialidades-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
var decorarGrilla=function(){
	$('.grid-view .items thead tr:first-child th:first-child').addClass('brsi');
    $('.grid-view .items thead tr:first-child th:last-child').addClass('brsd');
    $('.grid-view .items thead .filters td input').addClass('form-control');
    $('.grid-view .items thead .filters td select').addClass('form-control');
}
decorarGrilla();
");
?>

<h1>Seguimiento de Tablas</h1>

<?php 
$template="{view}";
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'especialidades-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'decorarGrilla',
	'columns'=>array(
		array(
			'name'=>'id',
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),
		//'old_value',
		'new_value',
		array(
			'name'=>'action',
			'value'=>'$data->getAccion()'
		),
		'model',
		'field',
		'model_id',
		'stamp',
		array(
            'name'=>'user_id',
            'value'=>'$data->getUsuario()',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>''),
        ),
		array(
			'class'=>'CButtonColumn',
			'template'=>$template,
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),
	),
)); ?>
