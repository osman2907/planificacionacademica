<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array('Auditoría','Seguimiento de Tablas','Consulta de Transacción'))
);

$this->menu=array(
	array('label'=>'List TblAuditTrail', 'url'=>array('index')),
	array('label'=>'Create TblAuditTrail', 'url'=>array('create')),
	array('label'=>'Update TblAuditTrail', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TblAuditTrail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TblAuditTrail', 'url'=>array('admin')),
);
?>

<h1>Consulta de Transacción</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'old_value',
		'new_value',
		array(
			'name'=>'action',
			'value'=>$model->getAccion(),
		),
		'model',
		'field',
		'stamp',
		array(
			'name'=>'user_id',
			'value'=>$model->getUsuario(),
		),
		'model_id',
	),
)); 


echo "<br><center>";
echo CHtml::link(
		'Regresar',
		array('admin'),
		array('class'=>'btn btn-danger'));
echo "</center>";
?>



