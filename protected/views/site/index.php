<?php
$this->pageTitle=Yii::app()->name;
?>

<div class="row">
    <div class="col-xs-3">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/iutoms.png" title="Instituto Universitario de Tecnología del Oeste Mariscal Sucre" style='height: 350px;'>
    </div>

    <div class="col-xs-9">
        <div class="row">
            <div class="col-xs-12">
                <div class="titulo-portada">Bienvenidos al <?php echo CHtml::encode(Yii::app()->name); ?> (SPLV2)</div>
            </div>
        </div>  

        <div class="row">

            <div class="col-xs-12">
                <div class="descripcion-proyecto">
                    La implementación de este sistema tiene como objetivo agilizar el proceso de Planificación Académica del Instituto Universitario de Tecnología del Oeste Mariscal Sucre.<br><br>

                    <div class="integrantes">
                        <b>Desarrollador:</b><br>
                        <ul>
                            <li>Osman Pérez</li>
                        </ul>

                        <b>Personal Colaborador:</b>
                        <ul>
                            <li>Lic. Cuiman Moreno</li>
                            <li>Lic. Mariela Vargas</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


