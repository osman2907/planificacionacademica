<script>
	$(document).ready(function(){
		$("#consultar").click(function(){
			cedula=$("#cedula").val();
			if(cedula.trim() == ''){
				$("#formulario-registro-estudiante").html('');	
				bootbox.alert("Por favor ingrese el número de cédula");
				return false;
			}

			url="<?php echo $this->createUrl('site/registroEstudiante')?>";
			data={cedula:cedula};
			respuesta=consultarPHP(url,data,'html',false);
			$("#formulario-registro-estudiante").html(respuesta);
		});

		$(document).on("submit","#registro-estudiantes-form",function(){
			url="<?php echo $this->createUrl('site/validarRegistroEstudiante')?>";
			data=$(this).serialize();
			respuesta=consultarPHP(url,data,'html',false);

			if(respuesta.trim() != ''){
				bootbox.alert(respuesta);
				return false;
			}
		});

		$(document).on('click', '.datepicker', function (){
			$.datepicker.setDefaults($.datepicker.regional['es']);
		    $(this).datepicker({
		    	dateFormat:'yy-mm-dd',
		    	changeYear: true,
		    	changeMonth: true,
		    	yearRange: "-100:+0",
		    }).datepicker("show");
		});
	});
</script>

<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Registro de Estudiantes')
    )
);
?>
<h1>Registro de Estudiantes</h1>

<div class="form">

	<p>Por favor ingrese su número de cédula para validar que exista en el Sistema de Control de Estudios del IUTOMS, luego presione consultar</p>

	<div class="row">
		<div class="col-md-2 form-group">
			<?php
			echo CHtml::label('Cédula', 'cedula');
			echo CHtml::textField('Cedula','',
				array('id'=>'cedula',
			       'maxlength'=>8,
			       'class'=>'form-control'
		       	)
			);
   			?>
		</div>

		<div class="col-md-4 form-group" style="padding-top:20px;">
			<?php echo CHtml::button('Consultar', array('id'=>'consultar','class'=>'btn btn-primary')); ?>
		</div>
	</div>

	<div id='formulario-registro-estudiante'></div>
</div>