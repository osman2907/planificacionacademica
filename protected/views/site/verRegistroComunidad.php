<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Registro de Comunidades','Registro exitoso')
    )
);
?>
<h1>Registro exitoso</h1>

<table class="table table-bordered">
	<tr>
		<th class="active">Nombre comunidad</th>
		<td><?php echo $model->nombre ?></td>
		<th class="active">Tipo comunidad</th>
		<td><?php echo $model->idTipoComunidad->tipo_comunidad?></td>
		<th class="active">Teléfono</th>
		<td><?php echo $model->telefono?></td>
	</tr>

	<tr>
		<th class="active">Estado</th>
		<td><?php echo $model->idParroquia->idMunicipio->idEstado->estado; ?></td>
		<th class="active">Municipio</th>
		<td><?php echo $model->idParroquia->idMunicipio->municipio; ?></td>
		<th class="active">Parroquia</th>
		<td><?php echo $model->idParroquia->parroquia; ?></td>
	</tr>

	<tr>
		<th class="active">Dirección</th>
		<td colspan="5"><?php echo $model->direccion ?></td>
	</tr>

	<tr>
		<th class="active">Persona Contacto</th>
		<td><?php echo $model->persona_contacto ?></td>
		<th class="active">Usuario</th>
		<td><?php echo $model->idUsuario->username ?></td>
		<th class="active">Correo Electrónico</th>
		<td><?php echo $model->idUsuario->email ?></td>
	</tr>

	<tr>
		<td colspan="6" class="centro">
			<b>Para iniciar sesión haga click en el siguiente botón</b>
			&nbsp;
			<?php echo CHtml::link(
				'Iniciar sesión',
				array('/cruge/ui/login'),
				array('class'=>'btn btn-info btn-xs')
			);?>
		</td>
	</tr>
</table>