<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click","#recuperar",function(){
			identificacion=$("#identificacion").val();
			modo=$("#modo").val();

			if(identificacion.trim() == ''){
				bootbox.alert("Por favor ingrese el usuario o número de cédula");
				return false;
			}

			if(modo.trim() == ''){
				bootbox.alert("Por favor seleccione el modo de recuperación");
				return false;
			}

			bootbox.confirm("¿Seguro que desea restablecer la contraseña?",function(response){
				if(response){
					url="<?php echo $this->createUrl('site/validarIdentificacion')?>";
					data={identificacion:identificacion};
					respuesta=consultarPHP(url,data,'html',false);

					if(respuesta){
						$("#recuperar").attr('disabled','disabled');
						url="<?php echo $this->createUrl('site/restablecerClave')?>";
						data={id_usuario:respuesta,modo:modo};
						respuesta=consultarPHP(url,data,'html',false);
						bootbox.alert("Recuperación de contraseña realizada con éxito",function(){
							location.reload();
						});
					}else{
						bootbox.alert("El número de cédula o usuario ingresado no se encuentra registrado");
					}
				}
			});
		});
	});
</script>
<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Recuperación de Contraseña')
    )
);
?>
<h1>Recuperación de Contraseña</h1>

<form id="formulario">
	<table class="table table-bordered">
		<tr>
			<th class="active" width="30%">Usuario o Número de Cédula: </th>
			<td>
				<input type="text" id="identificacion" name="identificacion" class="form-control">
			</td>
		</tr>

		<tr>
			<th class="active">Modo de Recuperación: </th>
			<td>
				<select id="modo" name="modo" class="form-control">
					<option value="">Seleccione</option>
					<option value="mail">Correo Electrónico</option>
					<option value="sms">Mensaje de Texto</option>
				</select>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="text-align:center">
				<input type="button" id="recuperar" value="Recuperar Contraseña" class="btn btn-success">
			</td>	
		</tr>
	</table>
</form>
