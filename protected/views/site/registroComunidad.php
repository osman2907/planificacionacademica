<script>
	$(document).ready(function(){
		$(document).on("change","#estado",function(){
			idEstado=$(this).val();
			$("#parroquia").html("<option value=''>Seleccione</option>");
			if(idEstado != ''){
				url="<?php echo $this->createUrl('municipios/listaMunicipios')?>";
				data={id_estado:idEstado};
				respuesta=consultarPHP(url,data,'html',false);
				$("#municipio").html(respuesta);
			}else{
				$("#municipio").html("<option value=''>Seleccione</option>");
			}
		});

		$(document).on("change","#municipio",function(){
			idMunicipio=$(this).val();
			if(idMunicipio != ''){
				url="<?php echo $this->createUrl('parroquias/listaParroquias')?>";
				data={id_municipio:idMunicipio};
				respuesta=consultarPHP(url,data,'html',false);
				$("#parroquia").html(respuesta);
			}else{
				$("#parroquia").html("<option value=''>Seleccione</option>");
			}
		});
	});
</script>

<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Registro de Comunidades')
    )
);
?>
<h1>Registro de Comunidades</h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registro-comunidades-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); 
echo $form->hiddenField($model,'id_usuario',array('value'=>'1'));
?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'nombre'); ?>
			<?php echo $form->textField($model,'nombre',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'nombre'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'id_tipo_comunidad'); ?>
			<?php echo $form->dropDownList($model,'id_tipo_comunidad',$listaTiposComunidades,array('class'=>'form-control','empty'=>'Seleccione','id'=>'tipo-comunidad')); ?>
			<?php echo $form->error($model,'id_tipo_comunidad'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'telefono'); ?>
			<?php echo $form->textField($model,'telefono',array('class'=>'form-control solo-numero','maxlength'=>'11')); ?>
			<?php echo $form->error($model,'telefono'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'id_estado'); ?>
			<?php echo $form->dropDownList($model,'id_estado',$listaEstados,array('class'=>'form-control','empty'=>'Seleccione','id'=>'estado')); ?>
			<?php echo $form->error($model,'id_estado'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'id_municipio'); ?>
			<?php echo $form->dropDownList($model,'id_municipio',array(),array('class'=>'form-control','empty'=>'Seleccione','id'=>'municipio')); ?>
			<?php echo $form->error($model,'id_municipio'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'id_parroquia'); ?>
			<?php echo $form->dropDownList($model,'id_parroquia',array(),array('class'=>'form-control','empty'=>'Seleccione','id'=>'parroquia')); ?>
			<?php echo $form->error($model,'id_parroquia'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 form-group">
			<?php echo $form->labelEx($model,'direccion'); ?>
			<?php echo $form->textArea($model,'direccion',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'direccion'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'persona_contacto'); ?>
			<?php echo $form->textField($model,'persona_contacto',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'persona_contacto'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('class'=>'form-control minuscula')); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('class'=>'form-control minuscula')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'conf_password'); ?>
			<?php echo $form->passwordField($model,'conf_password',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'conf_password'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-success'));?>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div>