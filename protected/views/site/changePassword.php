<script type="text/javascript">
	$(document).ready(function(){
		$("form").submit(function(){
			usuario=$("#CrugeUserModel_username").val();
			contrasenaActual=$("#CrugeUserModel_password").val();
			contrasenaNueva=$("#CrugeUserModel_password_new").val();
			contrasenaConfirme=$("#CrugeUserModel_password_new_confirm").val();

			if(contrasenaActual == ''){
				bootbox.alert("Por favor ingrese la contraseña actual");
				return false;
			}

			if(contrasenaNueva == ''){
				bootbox.alert("Por favor ingrese la contraseña nueva");
				return false;
			}

			if(contrasenaNueva == contrasenaActual){
				bootbox.alert("La contraseña nueva debe ser distinta a la actual");
				return false;
			}

			if(contrasenaConfirme == ''){
				bootbox.alert("Por favor confirme la contraseña nueva");
				return false;
			}

			if(contrasenaNueva != contrasenaConfirme){
				bootbox.alert("No coincide la confirmación de contraseña");
				return false;
			}

			url="<?php echo $this->createUrl('site/validarContrasena')?>";
			data={'usuario':usuario,'contrasena':contrasenaActual};
		    respuesta=consultarPHP(url,data,'html',false);

		    if(respuesta == 0){
		    	bootbox.alert("La contraseña actual es incorrecta");
				return false;	
		    }

		    bootbox.confirm("¿Seguro que desea cambiar la contraseña?",function(respuesta){
		    	if(respuesta){
		    		url="<?php echo $this->createUrl('site/cambiarContrasena')?>";
					data={'usuario':usuario,'contrasena':contrasenaNueva};
				    respuesta=consultarPHP(url,data,'html',false);

				    if(respuesta == 1){
				    	bootbox.alert("Contraseña modificada exitosamente",function(){
				    		location.reload();
				    	});
				    }
		    	}
		    });

			return false;
		});
	});
</script>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'change-password-form',
)); ?>
	<div>
		<div class="panel panel-info" style='width:400px; margin: 0 auto;'>
	        <div class="panel-heading">
	            <div class="panel-title">Cambiar Contraseña</div>
	        </div>     

	        <div style="padding-top:30px" class="panel-body" >

				<div class="form-group has-feedback has-feedback-left">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username',array('class'=>'form-control','readonly'=>'readonly')); ?>
			        <i class="form-control-feedback glyphicon glyphicon-user"></i>
			        <?php echo $form->error($model,'username'); ?>
			    </div>

			    <div class="form-group has-feedback has-feedback-left">
					<?php echo $form->labelEx($model,'password'); ?>
					<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
			        <i class="form-control-feedback glyphicon glyphicon-lock"></i>
			        <?php echo $form->error($model,'password'); ?>
			    </div>

			    <div class="form-group has-feedback has-feedback-left">
					<?php echo $form->labelEx($model,'password_new'); ?>
					<?php echo $form->passwordField($model,'password_new',array('class'=>'form-control')); ?>
			        <i class="form-control-feedback glyphicon glyphicon-lock"></i>
			        <?php echo $form->error($model,'password_new'); ?>
			    </div>

			    <div class="form-group has-feedback has-feedback-left">
					<?php echo $form->labelEx($model,'password_new_confirm'); ?>
					<?php echo $form->passwordField($model,'password_new_confirm',array('class'=>'form-control')); ?>
			        <i class="form-control-feedback glyphicon glyphicon-lock"></i>
			        <?php echo $form->error($model,'password_new_confirm'); ?>
			    </div>

			    <div class="row buttons" style="padding-top:20px;">
					<?php echo CHtml::submitButton('Enviar',array('class'=>'btn btn-success')); ?>
				</div>
			</div>
		</div>
	</div>
<?php $this->endWidget(); ?>