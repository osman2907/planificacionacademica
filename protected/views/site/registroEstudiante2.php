<script>
	$(document).ready(function(){
		$(document).on("change","#procedencia",function(){
			idProcedencia=$(this).val();
			if(idProcedencia != ''){
				url="<?php echo $this->createUrl('listaProcedenciasEspecialidades')?>";
				data={id_procedencia:idProcedencia};
				respuesta=consultarPHP(url,data,'html',false);
				$("#especialidad").html(respuesta);
			}else{
				$("#especialidad").html("<option value=''>Seleccione</option>")
			}
		});
	});
</script>

<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Registro de Estudiantes')
    )
);
?>
<h1>Registro de Estudiantes</h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registro-estudiantes-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); 
echo $form->hiddenField($model,'id_usuario',array('value'=>'1'));
?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'cedula'); ?>
			<?php echo $form->textField($model,'cedula',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'cedula'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'primer_nombre'); ?>
			<?php echo $form->textField($model,'primer_nombre',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'primer_nombre'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'segundo_nombre'); ?>
			<?php echo $form->textField($model,'segundo_nombre',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'segundo_nombre'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'primer_apellido'); ?>
			<?php echo $form->textField($model,'primer_apellido',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'primer_apellido'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'segundo_apellido'); ?>
			<?php echo $form->textField($model,'segundo_apellido',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'segundo_apellido'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'fecha_nac'); ?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model'=>$model,
			    'attribute'=>'fecha_nac',
			    'language'=>'es',
			    'options'=>array('showAnim'=>'fold','changeMonth'=>true,'changeYear'=>true,
			    	'yearRange'=>'1950:2099','dateFormat' => 'yy-mm-dd'),
			    'htmlOptions'=>array('class'=>'form-control')
			));
			?>
			<?php echo $form->error($model,'fecha_nac'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'id_procedencia'); ?>
			<?php echo $form->dropDownList($model,'id_procedencia',$listaProcedencias,array('class'=>'form-control','empty'=>'Seleccione','id'=>'procedencia')); ?>
			<?php echo $form->error($model,'id_procedencia'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'id_procedencia_especialidad'); ?>
			<?php echo $form->dropDownList($model,'id_procedencia_especialidad',array(),array('class'=>'form-control','empty'=>'Seleccione','id'=>'especialidad')); ?>
			<?php echo $form->error($model,'id_procedencia_especialidad'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'telefono_celular'); ?>
			<?php echo $form->textField($model,'telefono_celular',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'telefono_celular'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'telefono_local'); ?>
			<?php echo $form->textField($model,'telefono_local',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'telefono_local'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($model,'conf_password'); ?>
			<?php echo $form->passwordField($model,'conf_password',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'conf_password'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-success'));?>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div>