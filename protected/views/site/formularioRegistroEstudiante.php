<?php
if(!isset($modelEstudiantesSiace)){
	?>
	<div class="alert alert-warning titulo-portada" role="alert">
		El número de cédula que acaba de ingresar no se encuentra registrado en la base de datos de Control de Estudios del IUTOMS.
	</div>
	<?php
	exit();
}

if(!$modelPersonas->isNewRecord && count($modelPersonas->estudiantes) > 0){
	?>
	<div class="alert alert-warning titulo-portada" role="alert">
		El estudiante con este número de cédula ya se encuentra registrado.
	</div>
	<?php
	exit();
}

/*echo "<pre>";
print_r($modelPersonas);
echo "</pre>";*/

$nombres=explode(" ",$modelEstudiantesSiace->nombres);
$apellidos=explode(" ",$modelEstudiantesSiace->apellidos);
$modelPersonas->cedula=$modelEstudiantesSiace->cedula;
$modelPersonas->primer_nombre=$nombres[0];
$modelPersonas->segundo_nombre=(isset($nombres[1])?$nombres[1]:'');
$modelPersonas->primer_apellido=$apellidos[0];
$modelPersonas->segundo_apellido=(isset($apellidos[1])?$apellidos[1]:'');
$modelPersonas->especialidad=$modelEstudiantesSiace->idCarrera->carreraProcedencia;
$modelPersonas->id_carrera=$modelEstudiantesSiace->id_carrera;
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registro-estudiantes-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
));?>
	<?php echo $form->hiddenField($modelPersonas,'id_carrera');?>
	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'cedula'); ?>
			<?php echo $form->textField($modelPersonas,'cedula',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelPersonas,'cedula'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'primer_nombre'); ?>
			<?php echo $form->textField($modelPersonas,'primer_nombre',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelPersonas,'primer_nombre'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'segundo_nombre'); ?>
			<?php echo $form->textField($modelPersonas,'segundo_nombre',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelPersonas,'segundo_nombre'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'primer_apellido'); ?>
			<?php echo $form->textField($modelPersonas,'primer_apellido',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelPersonas,'primer_apellido'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'segundo_apellido'); ?>
			<?php echo $form->textField($modelPersonas,'segundo_apellido',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelPersonas,'segundo_apellido'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'especialidad'); ?>
			<?php echo $form->textField($modelPersonas,'especialidad',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelPersonas,'especialidad'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'fecha_nac'); ?>
			<?php echo $form->textField($modelPersonas,'fecha_nac',array('class'=>'form-control datepicker','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelPersonas,'fecha_nac'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'telefono_celular'); ?>
			<?php echo $form->textField($modelPersonas,'telefono_celular',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($modelPersonas,'telefono_celular'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'telefono_local'); ?>
			<?php echo $form->textField($modelPersonas,'telefono_local',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($modelPersonas,'telefono_local'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'username'); ?>
			<?php echo $form->textField($modelPersonas,'username',array('class'=>'form-control minuscula')); ?>
			<?php echo $form->error($modelPersonas,'username'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'email'); ?>
			<?php echo $form->textField($modelPersonas,'email',array('class'=>'form-control minuscula')); ?>
			<?php echo $form->error($modelPersonas,'email'); ?>
		</div>

		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'password'); ?>
			<?php echo $form->passwordField($modelPersonas,'password',array('class'=>'form-control')); ?>
			<?php echo $form->error($modelPersonas,'password'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo $form->labelEx($modelPersonas,'conf_password'); ?>
			<?php echo $form->passwordField($modelPersonas,'conf_password',array('class'=>'form-control')); ?>
			<?php echo $form->error($modelPersonas,'conf_password'); ?>
		</div>
	</div>


	<div class="row">
		<div class="col-md-4 form-group">
			<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-success'));?>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>