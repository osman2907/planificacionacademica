<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Registro de Estudiantes','Registro exitoso')
    )
);
?>
<h1>Registro exitoso</h1>

<table class="table table-bordered">
	<tr>
		<th class="active">Cédula</th>
		<td><?php echo $model->cedula ?></td>
		<th class="active">Nombres</th>
		<td><?php echo $model->primer_nombre." ".$model->segundo_nombre ?></td>
		<th class="active">Apellidos</th>
		<td><?php echo $model->primer_apellido." ".$model->segundo_apellido ?></td>
	</tr>

	<tr>
		<th class="active">Fecha de Nacimiento</th>
		<td><?php echo $model->fecha_nac ?></td>
		<th class="active">Procedencia</th>
		<td><?php echo $model->estudiantes[0]->idCarrera->idProcedencia->procedencia ?></td>
		<th class="active">Especialidad</th>
		<td><?php echo $model->estudiantes[0]->idCarrera->carrera ?></td>
	</tr>

	<tr>
		<th class="active">Teléfono celular</th>
		<td><?php echo $model->telefono_celular ?></td>
		<th class="active">Teléfono local</th>
		<td><?php echo $model->telefono_local ?></td>
		<th class="active">Usuario</th>
		<td><?php echo $model->idUsuario->username ?></td>
	</tr>

	<tr>
		<th class="active">Correo Electrónico</th>
		<td colspan="5"><?php echo $model->idUsuario->email ?></td>
	</tr>

	<tr>
		<td colspan="6" class="centro">
			<b>Para iniciar sesión haga click en el siguiente botón</b>
			&nbsp;
			<?php echo CHtml::link(
				'Iniciar sesión',
				array('/cruge/ui/login'),
				array('class'=>'btn btn-info btn-xs')
			);?>
		</td>
	</tr>
</table>