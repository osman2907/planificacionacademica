<?php
switch ($code) {
	case 401:
		$this->widget(
		    'booster.widgets.TbBreadcrumbs',
		    array('links' => array(
		    	'Sesión Finalizada')
		    )
		);
		?>

		<h2>Sesión Finalizada</h2>

		<div class="alert alert-danger">
			<b>Estimado usuario ha finalizado la sesión, si desea continuar utilizando el sistema presione la opción Iniciar Sesión del menú.</b>
		</div>
		<?php
	break;
	
	default:
		$this->widget(
		    'booster.widgets.TbBreadcrumbs',
		    array('links' => array(
		    	'Error')
		    )
		);
		?>

		<h2>Error</h2>

		<div class="alert alert-danger">
			<b>Ha ocurrido un error en el sistema. Si el error persiste envíe un correo a la dirección de correo electrónico soporte.sigepsi@gmail.com</b>
		</div>
		<!--<div class="error">
			<?php //echo CHtml::encode($message); ?>
		</div>-->
		<?php
	break;
}
