<?php
Yii::import('ext.phpexcel.XPHPExcel');
$objPHPExcel= XPHPExcel::createPHPExcel();


function generarListados($pestana,$departamento,$idPeriodo,$objPHPExcel){

	$estiloTituloReporte = array(
		'font' => array(
			'name'      => 'Verdana',
			'bold'      => true,
			'italic'    => false,
			'strike'    => false,
			'size' =>14,
			'color'     => array(
				'rgb' => '000000'
			)
		),
		'alignment' =>  array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'rotation'   => 0,
			'wrap'          => TRUE
		)
	);

	$tituloCuadro = array(
	    'fill' => array(
	        'type' => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'CCCCCC')
	    ),
		'borders' => array(
		    'allborders' => array(
		        'style' => PHPExcel_Style_Border::BORDER_THIN,
		        'color' => array('rgb' => '000000')
		    )
		),
		'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		),
		'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => '000000'),
	        'size'  => 10
	    )
	);

	$bordesTodos=array(
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	        )
	    )
	);

	$letraRoja=array('font'  => array(
	    //'bold'  => false,
	    'color' => array('rgb' => 'FE2E2E'),
	    'size'  => 10
	));

	$negrita=array('font'  => array(
	    'bold'  => true,
	    'color' => array('rgb' => '000000'),
	));

	$centrado = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

	$objPHPExcel->setActiveSheetIndex($pestana);

	$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(30);
	$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(20);
	$objPHPExcel->getActiveSheet()->getRowDimension(4)->setRowHeight(20);
	$objPHPExcel->getActiveSheet()->getRowDimension(5)->setRowHeight(20);

	$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
	$objPHPExcel->getActiveSheet()->setCellValue("A2","AUDITORIA DOCENTE - DEPARTAMENTO DE ".strtoupper($departamento->departamento));
	$objPHPExcel->getActiveSheet()->getStyle("A2")->applyFromArray($estiloTituloReporte);

	$fila=5;
	$filaInfo=$fila-1;
	$filaPer=$filaInfo-1;

	$modelPeriodo=PeriodosAcademicos::model()->findByPk($idPeriodo);

	$objPHPExcel->getActiveSheet()->mergeCells("A$filaPer:C$filaPer");
	$objPHPExcel->getActiveSheet()->setCellValue("A$filaPer","Período Académico: ".$modelPeriodo->anioPeriodo);
	$objPHPExcel->getActiveSheet()->getStyle("A$filaPer")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->mergeCells("A$filaInfo:C$filaInfo");
	$objPHPExcel->getActiveSheet()->setCellValue("A$filaInfo","Fecha Reporte: ".date("d/m/Y"));
	$objPHPExcel->getActiveSheet()->getStyle("A$filaInfo")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->mergeCells("E$filaInfo:I$filaInfo");
	$objPHPExcel->getActiveSheet()->setCellValue("E$filaInfo","Hora Reporte: ".date("h:i a"));
	$objPHPExcel->getActiveSheet()->getStyle("E$filaInfo")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->applyFromArray($tituloCuadro);
	$objPHPExcel->getActiveSheet()->setCellValue("A$fila","CÉDULA");
	$objPHPExcel->getActiveSheet()->setCellValue("B$fila","APELLIDOS Y NOMBRES");
	$objPHPExcel->getActiveSheet()->setCellValue("C$fila","TIPO CONTRATO");
	$objPHPExcel->getActiveSheet()->setCellValue("D$fila","CATEGORÍA");
	$objPHPExcel->getActiveSheet()->setCellValue("E$fila","DEDICACIÓN");
	$objPHPExcel->getActiveSheet()->setCellValue("F$fila","HORAS REGLAMENTO");
	$objPHPExcel->getActiveSheet()->setCellValue("G$fila","HORAS ASIGNADAS");
	$objPHPExcel->getActiveSheet()->setCellValue("H$fila","HORAS FALTANTES");
	$objPHPExcel->getActiveSheet()->setCellValue("I$fila","HORAS EXCESO");


	$opciones['condition']="id_periodo=$idPeriodo AND t.id_departamento=$departamento->id_departamento";
	$opciones['order']='"idDocente"."apellido1" ASC';
	$modelPeriodosDocentes=PeriodosAcademicosDocentes::model()->with('idDocente')->findAll($opciones);	
		

	foreach ($modelPeriodosDocentes as $periodoDocente){
		$options['condition']="id_periodo=$idPeriodo AND id_docente=$periodoDocente->id_docente";
		$horasAsignadas=count(Horarios::model()->findAll($options));
		$horasReglamento=$periodoDocente->idDedicacionDocente->cantidad_horas;
		$horasFaltantes=$horasReglamento-$horasAsignadas > 0?$horasReglamento-$horasAsignadas:0;
		$horasExceso=$horasAsignadas-$horasReglamento > 0?$horasAsignadas-$horasReglamento:0;
		$fila++;
		
		$objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->applyFromArray($bordesTodos);
		$objPHPExcel->getActiveSheet()->setCellValue("A$fila",$periodoDocente->idDocente->cedula);
		$objPHPExcel->getActiveSheet()->setCellValue("B$fila",$periodoDocente->idDocente->nombreCompleto);
		$objPHPExcel->getActiveSheet()->setCellValue("C$fila",$periodoDocente->idTipoContrato->tipo_contrato);
		$objPHPExcel->getActiveSheet()->setCellValue("D$fila",$periodoDocente->idCategoriaDocente->categoria_docente);
		$objPHPExcel->getActiveSheet()->setCellValue("E$fila",$periodoDocente->idDedicacionDocente->dedicacion_docente);
		$objPHPExcel->getActiveSheet()->setCellValue("F$fila",$horasReglamento);
		$objPHPExcel->getActiveSheet()->setCellValue("G$fila",$horasAsignadas);
		$objPHPExcel->getActiveSheet()->setCellValue("H$fila",$horasFaltantes);
		$objPHPExcel->getActiveSheet()->setCellValue("I$fila",$horasExceso);

		$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setRowHeight(20);
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
}


$objPHPExcel->getProperties()->setCreator("IUTOMS") //Autor
    ->setLastModifiedBy("IUTOMS") //Ultimo usuario que lo modificó
    ->setTitle("IUTOMS")
    ->setSubject("IUTOMS")
    ->setDescription("IUTOMS")
    ->setKeywords("IUTOMS")
    ->setCategory("Reporte excel");


$i=0;
foreach ($listDepartamentos as $departamento){
	if($i != 0){
		$objPHPExcel->createSheet($i);
	}
	$objPHPExcel->setActiveSheetIndex($i)->setTitle($departamento->departamento);
	generarListados($i,$departamento,$idPeriodo,$objPHPExcel);
	$i++;
}


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="AuditoriaDocente.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>