<?php
if(count($listDocentes) == 0){
	echo "<div class='centro letra-roja negrita'>No hay horarios registrados con los parámetros seleccionados</div>";
}

foreach($listDocentes as $docente){ 
	$idPeriodo=$docente['id_periodo'];
	$idDocente=$docente['id_docente'];
	$departamento=$docente['departamento'];
	?>

	<table class="table table-bordered" cellspacing="0">
		<tr class="active">
			<th class="centro" colspan="19">
				<img class="horario-membrete" src="<?php echo Yii::app()->request->baseUrl; ?>/images/membrete-iutoms.jpg" title="Instituto Universitario de Tecnología del Oeste Mariscal Sucre">
			</th>
		</tr>

		<tr>
			<td colspan="19">
				<table class="tabla-encabezado" width="100%">
					<tr>
						<td>
							<b>Departamento: </b><?php echo strtoupper($docente['departamento']); ?>
						</td>

						<td>
							<b>Período: </b><?php echo $docente['anio']."-".$docente['periodo']; ?>		
						</td>
					</tr>
					
					<tr>
						<td>
							<b>Docente: </b><?php echo strtoupper($docente['nombre_completo']); ?>
						</td>

						<td>
							<b>Inicio </b><?php echo date("d/m/Y",strtotime($docente['fecha_inicio'])); ?> <b>Hasta </b><?php echo date("d/m/Y",strtotime($docente['fecha_fin'])); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr class="active">
			<th rowspan="2" class="centro vcentro">Horas</th>
			<th class="centro" colspan="3" width="15%">Lunes</th>
			<th class="centro" colspan="3" width="15%">Martes</th>
			<th class="centro" colspan="3" width="15%">Miércoles</th>
			<th class="centro" colspan="3" width="15%">Jueves</th>
			<th class="centro" colspan="3" width="15%">Viernes</th>
			<th class="centro" colspan="3" width="15%">Sábado</th>
		</tr>

		<tr>
			<td class="horarios-detalles">Mat.</td>
			<td class="horarios-detalles">Secc.</td>
			<td class="horarios-detalles">Aula</td>

			<td class="horarios-detalles">Mat.</td>
			<td class="horarios-detalles">Secc.</td>
			<td class="horarios-detalles">Aula</td>

			<td class="horarios-detalles">Mat.</td>
			<td class="horarios-detalles">Secc.</td>
			<td class="horarios-detalles">Aula</td>

			<td class="horarios-detalles">Mat.</td>
			<td class="horarios-detalles">Secc.</td>
			<td class="horarios-detalles">Aula</td>

			<td class="horarios-detalles">Mat.</td>
			<td class="horarios-detalles">Secc.</td>
			<td class="horarios-detalles">Aula</td>

			<td class="horarios-detalles">Mat.</td>
			<td class="horarios-detalles">Secc.</td>
			<td class="horarios-detalles">Aula</td>
		</tr>

		<?php
		reset($listHoras);
		foreach($listHoras as $hora){
			$idHora=$hora['id_hora'];
			$horaInicio=date("h:i",strtotime($hora['start']));
			$horaFin=date("h:i",strtotime($hora['end']));
			?>
			<tr>
				<td><?php echo $horaInicio."-".$horaFin; ?></td>
				<?php
				for($idDia=1; $idDia<=6; $idDia++){
					$options['condition']="id_periodo=$idPeriodo AND id_docente=$idDocente AND id_hora=$idHora AND id_dia=$idDia";
					$modelHorario=Horarios::model()->findAll($options);

					if(count($modelHorario) > 0){ 
						$modelHorario=$modelHorario[0];
						$nombreAula=isset($modelHorario->idAula)?$modelHorario->idAula->nombre_aula:'';
						$docente=isset($modelHorario->idDocente)?$modelHorario->idDocente->apellidoNombre:'';
						?>
						<td colspan="3" class="datos-horas">
							<?php 
							echo $modelHorario->idAsignatura->codigo."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							echo $modelHorario->idSeccion->seccion."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							echo $nombreAula."<br>";
							echo $docente;
							?>
						</td>
					<?php }else{ ?>
						<td colspan="3"><?php echo "&nbsp;"; ?></td>
					<?php }
				}
				?>
			</tr>
			<?php
		}
		?>
	</table>
	<div class="horario"></div>
<?php } ?>