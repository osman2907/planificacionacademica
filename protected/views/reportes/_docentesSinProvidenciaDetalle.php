<?php
$opciones['condition']="id_departamento=$idDepartamento AND providencia<>TRUE";
$opciones['order']='apellido1 ASC';
$docentes=Docentes::model()->findAll($opciones);
?>
<table class="table table-bordered">
	<tr class="active">
		<th>Cédula</th>
		<th>Apellidos y Nombres</th>
		<th>Tipo Contrato</th>
		<th>Categoría</th>
		<th>Dedicación</th>
	</tr>
	
	<?php
	if(count($docentes) == 0){
		?>
		<tr>
			<td class="letra-roja centro negrita" colspan="5">
				No hay docentes sin providencia en esta especialidad.
			</td>
		</tr>
		<?php
	}

	foreach ($docentes as $docente){
		?>
		<tr>
			<td><?php echo $docente->cedula; ?></td>
			<td><?php echo $docente->nombreCompleto; ?></td>
			<td><?php echo $docente->idTipoContrato->tipo_contrato; ?></td>
			<td><?php echo $docente->idCategoriaDocente->categoria_docente; ?></td>
			<td><?php echo $docente->idDedicacionDocente->dedicacion_docente; ?></td>
		</tr>
		<?php
	}
	?>
</table>