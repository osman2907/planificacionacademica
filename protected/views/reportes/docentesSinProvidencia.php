<?php
$this->breadcrumbs=array(
	'Reportes',
	'Docentes Sin Providencia'
);
?>

<h1>Docentes Sin Providencia</h1>

<div class="alert alert-info">Por favor seleccione los parámetros de búsqueda para mostrar los docentes sin providencia.</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'horarios-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->dropDownList($model,'id_departamento',$listDepartamentos,array('class'=>'form-control','empty'=>'Todos')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>

		<div class="col-xs-4" style="padding-top: 25px;">
			<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-success')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

<?php
if(isset($docentesSinProvidencia)){
	$idDepartamento=!empty($_POST['Horarios']['id_departamento'])?$_POST['Horarios']['id_departamento']:'';
	echo "<br>";
	echo CHtml::link(
		'Descargar Excel',
		array("reportes/excelDocentesSinProvidencia?idDepartamento=$idDepartamento"),
		array('class'=>'btn btn-success'));
	echo "<br>";
	?>
	<br>
	<table class="table table-bordered">
		<tr class="active">
			<th colspan="4">PARÁMETROS SELECCIONADOS</th>
		</tr>

		<tr>
			<th>Departamento</th>
			<td>
				<?php
				if(empty($_POST['Horarios']['id_departamento'])){
					echo "TODOS";
				}else{
					$options['condition']="id_departamento=".$_POST['Horarios']['id_departamento'];
					echo strtoupper(Departamentos::model()->find($options)->departamento); 
				}
				?>
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<?php echo $docentesSinProvidencia ?>
			</td>
		</tr>
	</table>
	<?php
}
?>