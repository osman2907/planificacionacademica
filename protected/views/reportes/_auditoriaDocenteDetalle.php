<?php
$opciones['condition']="id_periodo=$idPeriodo AND t.id_departamento=$idDepartamento";
$opciones['order']='"idDocente"."apellido1" ASC';
$modelPeriodosDocentes=PeriodosAcademicosDocentes::model()->with('idDocente')->findAll($opciones);
?>
<table class="table table-bordered">
	<tr class="active">
		<th>Cédula</th>
		<th>Apellidos y Nombres</th>
		<th>Tipo Contrato</th>
		<th>Categoría</th>
		<th>Dedicación</th>
		<th>Horas Reglamento</th>
		<th>Horas Asignadas</th>
		<th>Horas Faltantes</th>
		<th>Horas Exceso</th>
	</tr>
	
	<?php
	foreach ($modelPeriodosDocentes as $periodoDocente){
		$options['condition']="id_periodo=$idPeriodo AND id_docente=$periodoDocente->id_docente";
		$horasAsignadas=count(Horarios::model()->findAll($options));
		$horasReglamento=$periodoDocente->idDedicacionDocente->cantidad_horas;
		$horasFaltantes=$horasReglamento-$horasAsignadas > 0?$horasReglamento-$horasAsignadas:0;
		$horasExceso=$horasAsignadas-$horasReglamento > 0?$horasAsignadas-$horasReglamento:0;
		?>
		<tr>
			<td><?php echo $periodoDocente->idDocente->cedula; ?></td>
			<td><?php echo $periodoDocente->idDocente->nombreCompleto; ?></td>
			<td><?php echo $periodoDocente->idTipoContrato->tipo_contrato; ?></td>
			<td><?php echo $periodoDocente->idCategoriaDocente->categoria_docente; ?></td>
			<td><?php echo $periodoDocente->idDedicacionDocente->dedicacion_docente; ?></td>
			<td><?php echo $horasReglamento; ?></td>
			<td><?php echo $horasAsignadas; ?></td>
			<td><?php echo $horasFaltantes; ?></td>
			<td><?php echo $horasExceso; ?></td>
		</tr>
		<?php
	}
	?>
</table>