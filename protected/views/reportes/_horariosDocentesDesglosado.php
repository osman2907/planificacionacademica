<?php
$directorio="reportes/".date("Y-m-d-H:i:s");
mkdir($directorio,"0777");
system("chmod -R 777 $directorio");

foreach($listDocentes as $docente){ 
	$idPeriodo=$docente['id_periodo'];
	$idDocente=$docente['id_docente'];
	$departamento=$docente['departamento'];

	$mpdf = Yii::app()->ePdf->mpdf();
	$mpdf = new mPDF();
	$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/reportes.css');
	$mpdf->WriteHTML($stylesheet, 1);
	$mpdf->AddPage('L',
        '', '', '', '',
        10, // margin_left
        10, // margin right
        10, // margin top
        5, // margin bottom
        18, // margin header
        12); // margin footer
	

	$html="
	<table class='table table-bordered' cellspacing='0'>
		<tr class='active'>
			<th class='centro' colspan='19'>
				<img class='horario-membrete' src='".Yii::app()->request->baseUrl."/images/membrete-iutoms.jpg' title='Instituto Universitario de Tecnología del Oeste Mariscal Sucre'>
			</th>
		</tr>

		<tr>
			<td colspan='19'>
				<table class='tabla-encabezado' width='100%'>
					<tr>
						<td>
							<b>Departamento: </b>".strtoupper($docente['departamento'])."
						</td>

						<td>
							<b>Período: </b>".$docente['anio']."-".$docente['periodo']."
						</td>
					</tr>
					
					<tr>
						<td>
							<b>Docente: </b>".strtoupper($docente['nombre_completo'])."
						</td>

						<td>
							<b>Inicio </b>".date("d/m/Y",strtotime($docente['fecha_inicio']))." <b>Hasta </b>".date("d/m/Y",strtotime($docente['fecha_fin']))."
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr class='active'>
			<th rowspan='2' class='centro vcentro'>Horas</th>
			<th class='centro' colspan='3' width='15%'>Lunes</th>
			<th class='centro' colspan='3' width='15%'>Martes</th>
			<th class='centro' colspan='3' width='15%'>Miércoles</th>
			<th class='centro' colspan='3' width='15%'>Jueves</th>
			<th class='centro' colspan='3' width='15%'>Viernes</th>
			<th class='centro' colspan='3' width='15%'>Sábado</th>
		</tr>

		<tr>
			<td class='horarios-detalles'>Mat.</td>
			<td class='horarios-detalles'>Secc.</td>
			<td class='horarios-detalles'>Aula</td>

			<td class='horarios-detalles'>Mat.</td>
			<td class='horarios-detalles'>Secc.</td>
			<td class='horarios-detalles'>Aula</td>

			<td class='horarios-detalles'>Mat.</td>
			<td class='horarios-detalles'>Secc.</td>
			<td class='horarios-detalles'>Aula</td>

			<td class='horarios-detalles'>Mat.</td>
			<td class='horarios-detalles'>Secc.</td>
			<td class='horarios-detalles'>Aula</td>

			<td class='horarios-detalles'>Mat.</td>
			<td class='horarios-detalles'>Secc.</td>
			<td class='horarios-detalles'>Aula</td>

			<td class='horarios-detalles'>Mat.</td>
			<td class='horarios-detalles'>Secc.</td>
			<td class='horarios-detalles'>Aula</td>
		</tr>";


		$docente2=$docente;
		reset($listHoras);
		foreach($listHoras as $hora){
			$idHora=$hora['id_hora'];
			$horaInicio=date("h:i",strtotime($hora['start']));
			$horaFin=date("h:i",strtotime($hora['end']));

			$html.="
			<tr>
				<td>".$horaInicio."-".$horaFin."</td>";

				for($idDia=1; $idDia<=6; $idDia++){
					$options['condition']="id_periodo=$idPeriodo AND id_docente=$idDocente AND id_hora=$idHora AND id_dia=$idDia";
					$modelHorario=Horarios::model()->findAll($options);

					if(count($modelHorario) > 0){ 
						$modelHorario=$modelHorario[0];
						$nombreAula=isset($modelHorario->idAula)?$modelHorario->idAula->nombre_aula:'';
						$docente=isset($modelHorario->idDocente)?$modelHorario->idDocente->apellidoNombre:'';
						
						$html.="
						<td colspan='3' class='datos-horas'>".
							$modelHorario->idAsignatura->codigo."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".
							$modelHorario->idSeccion->seccion."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".
							$nombreAula."<br>".
							$docente."
							
						</td>";
					}else{
						$html.="<td colspan='3'>&nbsp;</td>";
					}
				}
				
			$html.="</tr>";
			
		}
		
	$html.="</table>";
	
	$mpdf->WriteHTML($html);
	$mpdf->Output("$directorio/".$docente2['cedula']."-".$docente2['nombre_completo'].".pdf");
	?>

<?php }
system("chmod -R 777 $directorio");

$zip = new ZipArchive();
// Creamos y abrimos un archivo zip temporal
$zip->open("horariosDocentes.zip",ZipArchive::CREATE);
// Añadimos un directorio
$dir = 'Horarios';
$zip->addEmptyDir($dir);


if ($gestor = opendir($directorio)){
    while (false !== ($entrada = readdir($gestor))) {
        if ($entrada != "." && $entrada != "..") {
            // Añadimos un archivo en la raid del zip.
			$zip->addFile("$directorio/$entrada","Horarios/$entrada");
        }
    }
    closedir($gestor);
}

$zip->close();
// Creamos las cabezeras que forzaran la descarga del archivo como archivo zip.
header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=horariosDocentes.zip");
// leemos el archivo creado
readfile('horariosDocentes.zip');
unlink('horariosDocentes.zip');//Destruye el archivo temporal
//unlink($directorio);
?>