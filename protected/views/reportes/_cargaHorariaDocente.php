<table class="table table-bordered" cellspacing="0">
	<!--<tr class="active">
		<th class="centro" colspan="2">
			<img class="horario-membrete" src="<?php echo Yii::app()->request->baseUrl; ?>/images/membrete-iutoms.jpg" title="Instituto Universitario de Tecnología del Oeste Mariscal Sucre" width="100%">
		</th>
	</tr>-->
<?php
/*echo "<pre>";
print_r($post);
echo "</pre>";
exit();*/



foreach($listDepartamentos as $departamento){ 
	$idDepartamento=$departamento['id_departamento'];
	$departamento=$departamento['departamento'];
	?>
	<tr>
		<td>
			<span class="negrita mayuscula">PNF: <?php echo $departamento; ?></span>
		</td>

		<td>
			<span class="negrita mayuscula">Tipo de Contratación: </span>
			<?php
			if(empty($post['id_tipo_contrato'])){
				echo "TODOS";
			}else{
				$options['condition']="id_tipo_contrato=".$post['id_tipo_contrato'];
				echo strtoupper(TiposContratos::model()->find($options)->tipo_contrato);
			}
			?>
		</td>
	</tr>

	<?php
	$idPeriodo=$post['id_periodo'];
	$idTipoContrato=$post['id_tipo_contrato'];
	$opciones['condition']="id_periodo=$idPeriodo AND t.id_departamento=$idDepartamento";
	if(!empty($idTipoContrato)){
		$opciones['condition'].=" AND t.id_tipo_contrato=$idTipoContrato";
	}
	$opciones['order']='"idDocente"."apellido1" ASC';
	$modelPeriodosDocentes=PeriodosAcademicosDocentes::model()->with('idDocente')->findAll($opciones);
	

	foreach($modelPeriodosDocentes as $periodoDocente){
		$idDocente=$periodoDocente->id_docente;
		?>
		<tr>
			<td colspan="2">
				<table width="100%" cellspacing="0" class="sin-borde titulo-docente-carga">
					<tr>
						<td class="negrita mayuscula">Apellidos y Nombres: <?php echo $periodoDocente->idDocente->nombreCompleto; ?></td>

						<td class="negrita mayuscula">Cédula: <?php echo $periodoDocente->idDocente->cedula; ?></td>

						<td class="negrita mayuscula">Tipo Contrato: <?php echo $periodoDocente->idTipoContrato->tipo_contrato; ?></td>
					</tr>

					<tr>
						<td class="negrita mayuscula">Categoría: <?php echo $periodoDocente->idCategoriaDocente->categoria_docente; ?></td>
						
						<td class="negrita mayuscula">Dedicación: <?php echo $periodoDocente->idDedicacionDocente->dedicacion_docente; ?></td>					
					</tr>

					<tr>
						<td colspan="3">
							<table class="table table-bordered carga-detalle" width="100%" cellspacing="0">
								<tr class="active">
									<th>CÓDIGO</th>
									<th>ASIGNATURA</th>
									<th>SECCIÓN</th>
									<th>TRAYECTO</th>
									<th>HORAS</th>
								</tr>

								<?php
								$sql="
									SELECT MIN(a.codigo) AS \"codigo\",MIN(a.asignatura) AS \"asignatura\",
										MIN(s.seccion) AS \"seccion\",COUNT(id_periodo) AS \"cantidad\",
										MIN(s.trayecto) AS \"trayecto\" 
									FROM horarios h
										INNER JOIN asignaturas a ON h.id_asignatura=a.id_asignatura
										INNER JOIN secciones s ON s.id_seccion=h.id_seccion
									WHERE id_periodo=$idPeriodo AND id_docente=$idDocente
									GROUP BY h.id_seccion,s.trayecto
									ORDER BY s.trayecto";

								$horas= Yii::app()->db->createCommand($sql)->queryAll();

								$total=0;
								foreach ($horas as $hora) {
									$total=$total+$hora['cantidad'];
									?>
									<tr>
										<td><?php echo $hora['codigo'] ?></td>
										<td><?php echo $hora['asignatura'] ?></td>
										<td class="centrar"><?php echo $hora['seccion'] ?></td>
										<td class="centrar"><?php echo $hora['trayecto'] ?></td>
										<td class="centrar"><?php echo $hora['cantidad'] ?></td>
									</tr>
									<?php
								}
								?>

								<tr>
									<td colspan="4" class="negrita">TOTAL HORAS SEMANALES</td>
									<td class="centrar"><?php echo $total ?></td>
								</tr>

								<tr>
									<td colspan="5"><b>Observaciones:</b></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php
	}
}
?>

</table>