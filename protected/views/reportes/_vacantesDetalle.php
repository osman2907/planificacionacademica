<?php
$sql="
	SELECT min(seccion) AS \"seccion\",min(s.trayecto) AS \"trayecto\",min(pas.trimestre) AS \"trimestre\",
		min(asignatura) AS \"asignatura\",count(h.id_periodo) AS \"cantidad_horas\",min(h.id_asignatura) AS \"id_asignatura\",min(h.id_seccion) AS \"id_seccion\"
	FROM horarios h
		INNER JOIN secciones s ON h.id_seccion=s.id_seccion
		INNER JOIN asignaturas a ON h.id_asignatura=a.id_asignatura
		INNER JOIN horas hor ON h.id_hora=hor.id_hora
		INNER JOIN periodos_academicos_secciones pas ON s.id_seccion=pas.id_seccion AND pas.id_periodo=$idPeriodo
	WHERE id_docente is null AND h.id_periodo=$idPeriodo AND id_departamento=$idDepartamento
	GROUP BY h.id_periodo,h.id_seccion,h.id_asignatura
	ORDER BY trayecto ASC, trimestre ASC, asignatura";
$vacantes=Yii::app()->db->createCommand($sql)->queryAll();
?>

<table class="table table-bordered">
	<tr class="active">
		<th>Nro.</th>
		<th>Asignatura</th>
		<th>Trayecto</th>
		<th>Trimestre</th>
		<th>Sección</th>
		<th>Día</th>
		<th>Hora Inicio</th>
		<th>Hora Fin</th>
		<th>Cantidad Horas</th>
	</tr>
	
	<?php

	if(count($vacantes) == 0){
		?>
		<tr>
			<td colspan="9" class="centro">No hay vacantes para este departamento</td>
		</tr>
		<?php
	}
	$i=0;
	foreach ($vacantes as $vacante){
		$i++;
		$idAsignatura=$vacante['id_asignatura'];
		$idSeccion=$vacante['id_seccion'];
		$sql="
			SELECT min(hora_inicio) AS \"hora_inicio\",max(hora_fin) AS \"hora_fin\",id_dia
			FROM horarios h
				INNER JOIN horas hor ON h.id_hora=hor.id_hora
			WHERE id_docente is null AND h.id_periodo=$idPeriodo AND id_asignatura=$idAsignatura 
				AND id_seccion=$idSeccion
			GROUP BY h.id_periodo,h.id_seccion,h.id_asignatura,id_dia
			";
		$dias=Yii::app()->db->createCommand($sql)->queryAll();
		$combinaFilas=count($dias);
		?>
		<tr>
			<td rowspan="<?php echo $combinaFilas; ?>"><?php echo $i; ?></td>
			<td rowspan="<?php echo $combinaFilas; ?>"><?php echo $vacante['asignatura']; ?></td>
			<td rowspan="<?php echo $combinaFilas; ?>" class="centro"><?php echo $vacante['trayecto']; ?></td>
			<td rowspan="<?php echo $combinaFilas; ?>" class="centro"><?php echo $vacante['trimestre']; ?></td>
			<td rowspan="<?php echo $combinaFilas; ?>" class="centro"><?php echo $vacante['seccion']; ?></td>
			<td class="centro"><?php echo convertirDia($dias[0]['id_dia']); ?></td>
			<td class="centro"><?php echo date("h:ia",strtotime($dias[0]['hora_inicio'])); ?></td>
			<td class="centro"><?php echo date("h:ia",strtotime($dias[0]['hora_fin'])); ?></td>
			<td rowspan="<?php echo $combinaFilas; ?>" class="centro"><?php echo $vacante['cantidad_horas']; ?></td>
		</tr>
		<?php
		if(count($dias) > 1){
			for($cont=1; $cont<count($dias); $cont++){
				?>
				<tr>
					<td class="centro"><?php echo convertirDia($dias[$cont]['id_dia']); ?></td>
					<td class="centro"><?php echo date("h:ia",strtotime($dias[$cont]['hora_inicio'])); ?></td>
					<td class="centro"><?php echo date("h:ia",strtotime($dias[$cont]['hora_fin'])); ?></td>
				</tr>
				<?php
			}
		}
	}
	?>
</table>