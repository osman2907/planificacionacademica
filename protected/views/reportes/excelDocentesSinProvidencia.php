<?php
Yii::import('ext.phpexcel.XPHPExcel');
$objPHPExcel= XPHPExcel::createPHPExcel();


function generarListados($pestana,$departamento,$objPHPExcel){

	$estiloTituloReporte = array(
		'font' => array(
			'name'      => 'Verdana',
			'bold'      => true,
			'italic'    => false,
			'strike'    => false,
			'size' =>14,
			'color'     => array(
				'rgb' => '000000'
			)
		),
		'alignment' =>  array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'rotation'   => 0,
			'wrap'          => TRUE
		)
	);

	$tituloCuadro = array(
	    'fill' => array(
	        'type' => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'CCCCCC')
	    ),
		'borders' => array(
		    'allborders' => array(
		        'style' => PHPExcel_Style_Border::BORDER_THIN,
		        'color' => array('rgb' => '000000')
		    )
		),
		'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		),
		'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => '000000'),
	        'size'  => 10
	    )
	);

	$bordesTodos=array(
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	        )
	    )
	);

	$letraRoja=array('font'  => array(
	    //'bold'  => false,
	    'color' => array('rgb' => 'FE2E2E'),
	    'size'  => 10
	));

	$negrita=array('font'  => array(
	    'bold'  => true,
	    'color' => array('rgb' => '000000'),
	));

	$centrado = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

	$objPHPExcel->setActiveSheetIndex($pestana);

	$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(30);
	$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(20);
	$objPHPExcel->getActiveSheet()->getRowDimension(4)->setRowHeight(20);
	$objPHPExcel->getActiveSheet()->getRowDimension(5)->setRowHeight(20);

	$objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
	$objPHPExcel->getActiveSheet()->setCellValue("A2","DOCENTES SIN PROVIDENCIA - DEPARTAMENTO DE ".strtoupper($departamento->departamento));
	$objPHPExcel->getActiveSheet()->getStyle("A2")->applyFromArray($estiloTituloReporte);

	$fila=5;
	$filaInfo=$fila-1;
	$filaPer=$filaInfo-1;


	$objPHPExcel->getActiveSheet()->mergeCells("A$filaPer:C$filaPer");
	//$objPHPExcel->getActiveSheet()->setCellValue("A$filaPer","Período Académico: ".$modelPeriodo->anioPeriodo);
	$objPHPExcel->getActiveSheet()->getStyle("A$filaPer")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->mergeCells("A$filaInfo:C$filaInfo");
	$objPHPExcel->getActiveSheet()->setCellValue("A$filaInfo","Fecha Reporte: ".date("d/m/Y"));
	$objPHPExcel->getActiveSheet()->getStyle("A$filaInfo")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->mergeCells("E$filaInfo:E$filaInfo");
	$objPHPExcel->getActiveSheet()->setCellValue("D$filaInfo","Hora Reporte: ".date("h:i a"));
	$objPHPExcel->getActiveSheet()->getStyle("D$filaInfo")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->getStyle("A$fila:E$fila")->applyFromArray($tituloCuadro);
	$objPHPExcel->getActiveSheet()->setCellValue("A$fila","CÉDULA");
	$objPHPExcel->getActiveSheet()->setCellValue("B$fila","APELLIDOS Y NOMBRES");
	$objPHPExcel->getActiveSheet()->setCellValue("C$fila","TIPO CONTRATO");
	$objPHPExcel->getActiveSheet()->setCellValue("D$fila","CATEGORÍA");
	$objPHPExcel->getActiveSheet()->setCellValue("E$fila","DEDICACIÓN");


	$opciones['condition']="id_departamento=$departamento->id_departamento AND providencia<> TRUE";
	$opciones['order']='apellido1 ASC';
	$docentes=Docentes::model()->findAll($opciones);


	foreach ($docentes as $docente){
		$fila++;
		
		$objPHPExcel->getActiveSheet()->getStyle("A$fila:E$fila")->applyFromArray($bordesTodos);
		$objPHPExcel->getActiveSheet()->setCellValue("A$fila",$docente->cedula);
		$objPHPExcel->getActiveSheet()->setCellValue("B$fila",$docente->nombreCompleto);
		$objPHPExcel->getActiveSheet()->setCellValue("C$fila",$docente->idTipoContrato->tipo_contrato);
		$objPHPExcel->getActiveSheet()->setCellValue("D$fila",$docente->idCategoriaDocente->categoria_docente);
		$objPHPExcel->getActiveSheet()->setCellValue("E$fila",$docente->idDedicacionDocente->dedicacion_docente);
		$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setRowHeight(20);
	}


	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
}


$objPHPExcel->getProperties()->setCreator("IUTOMS") //Autor
    ->setLastModifiedBy("IUTOMS") //Ultimo usuario que lo modificó
    ->setTitle("IUTOMS")
    ->setSubject("IUTOMS")
    ->setDescription("IUTOMS")
    ->setKeywords("IUTOMS")
    ->setCategory("Reporte excel");


$i=0;
foreach ($listDepartamentos as $departamento){
	if($i != 0){
		$objPHPExcel->createSheet($i);
	}
	$objPHPExcel->setActiveSheetIndex($i)->setTitle($departamento->departamento);
	generarListados($i,$departamento,$objPHPExcel);
	$i++;
}


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="AuditoriaDocente.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>