<?php
function convertirDia($dia){
	switch ($dia){
		case 1: return "Lunes"; break;
		case 2: return "Martes"; break;
		case 3: return "Miércoles"; break;
		case 4: return "Jueves"; break;
		case 5: return "Viernes"; break;
		case 6: return "Sábado"; break;
	}
}
?>

<ul class="nav nav-tabs" role="tablist">
	<?php
	$i=0;
	reset($listDepartamentos);
	foreach ($listDepartamentos as $departamento){
		$i++;
		$activo=$i==1?'active':'';
		$idDepartamento=$departamento->id_departamento;
		?>
		<li role="presentation" class="<?php echo $activo; ?>">
        	<a href="#docentes-<?php echo $idDepartamento ?>" aria-controls="<?php echo $idDepartamento ?>" role="tab" data-toggle="tab">
        		<?php echo $departamento->departamento; ?>
        	</a>
        </li>
		<?php
	}
	?>
</ul>

<div class="tab-content" style="padding:10px;">
	<?php
	reset($listDepartamentos);
	$i=0;
	foreach ($listDepartamentos as $departamento){
		$i++;
		$activo=$i==1?'active':'';
		$idDepartamento=$departamento->id_departamento;
		?>
		<div role="tabpanel" class="tab-pane <?php echo $activo ?>" id="docentes-<?php echo $idDepartamento; ?>">
            <?php 
            echo "<div class='centro negrita'>".strtoupper($departamento->departamento)."</div>";
            $this->renderPartial("_vacantesDetalle",array("idDepartamento"=>$idDepartamento,"idPeriodo"=>$post['id_periodo']));
            ?>
        </div>	
		<?php	
	} 
	?>
</div>