<ul class="nav nav-tabs" role="tablist">
	<?php
	$i=0;
	reset($listDepartamentos);
	foreach ($listDepartamentos as $departamento){
		$i++;
		$activo=$i==1?'active':'';
		$idDepartamento=$departamento->id_departamento;
		?>
		<li role="presentation" class="<?php echo $activo; ?>">
        	<a href="#docentes-<?php echo $idDepartamento ?>" aria-controls="<?php echo $idDepartamento ?>" role="tab" data-toggle="tab">
        		<?php echo $departamento->departamento; ?>
        	</a>
        </li>
		<?php
	}
	?>
</ul>

<div class="tab-content" style="padding:10px;">
	<?php
	reset($listDepartamentos);
	$i=0;
	foreach ($listDepartamentos as $departamento){
		$i++;
		$activo=$i==1?'active':'';
		$idDepartamento=$departamento->id_departamento;
		?>
		<div role="tabpanel" class="tab-pane <?php echo $activo ?>" id="docentes-<?php echo $idDepartamento; ?>">
            <?php 
            echo "<div class='centro negrita'>".strtoupper($departamento->departamento)."</div>";
            $this->renderPartial("_docentesSinProvidenciaDetalle",array("idDepartamento"=>$idDepartamento));
            ?>
        </div>	
		<?php	
	} 
	?>
</div>