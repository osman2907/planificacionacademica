<style type="text/css">
	.horario-membrete{
		width: 100%;
	}
</style>

<?php
$this->breadcrumbs=array(
	'Reportes',
	'Horarios por Docentes'
);
?>

<h1>Horarios por Docentes</h1>

<div class="alert alert-info">Por favor seleccione los parámetros de búsqueda para mostrar los horarios.</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'horas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_periodo'); ?>
			<?php echo $form->dropDownList($model,'id_periodo',$listPeriodos,array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'id_periodo'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->dropDownList($model,'id_departamento',$listDepartamentos,array('class'=>'form-control','empty'=>'Todos')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>

		<div class="col-xs-4" style="padding-top: 25px;">
			<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-success')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

<?php
if(isset($horarios)){
	if(isset($_POST['Horarios']['id_periodo']) && $_POST['Horarios']['id_departamento']){
		$idPeriodo=$_POST['Horarios']['id_periodo'];
		$idDepartamento=$_POST['Horarios']['id_departamento'];
		echo "<br>";
		echo CHtml::link(
			'Descargar PDF',
			array("reportes/imprimirHorariosDocentes?idPeriodo=$idPeriodo&idDepartamento=$idDepartamento"),
			array('class'=>'btn btn-danger'));
		echo "&nbsp;&nbsp;";

		echo CHtml::link(
			'Descargar ZIP',
			array("reportes/imprimirHorariosDocentesDesglosado?idPeriodo=$idPeriodo&idDepartamento=$idDepartamento"),
			array('class'=>'btn btn-danger'));
		echo "<br>";
	}
	?>
	<br>
	<table class="table table-bordered">
		<tr class="active">
			<th colspan="4">PARÁMETROS SELECCIONADOS</th>
		</tr>

		<tr>
			<th>Período Académico</th>
			<td>
				<?php
				$options['condition']="id_periodo=".$_POST['Horarios']['id_periodo'];
				echo PeriodosAcademicos::model()->find($options)->anioPeriodo; 
				?>
			</td>
			<th>Departamento</th>
			<td>
				<?php
				if(empty($_POST['Horarios']['id_departamento'])){
					echo "TODOS";
				}else{
					$options['condition']="id_departamento=".$_POST['Horarios']['id_departamento'];
					echo strtoupper(Departamentos::model()->find($options)->departamento); 
				}
				?>
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<?php echo $horarios ?>
			</td>
		</tr>
	</table>
	<?php
}
?>