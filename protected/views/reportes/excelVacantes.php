<?php
Yii::import('ext.phpexcel.XPHPExcel');
$objPHPExcel= XPHPExcel::createPHPExcel();


function convertirDia($dia){
	switch ($dia){
		case 1: return "Lunes"; break;
		case 2: return "Martes"; break;
		case 3: return "Miércoles"; break;
		case 4: return "Jueves"; break;
		case 5: return "Viernes"; break;
		case 6: return "Sábado"; break;
	}
}


function generarListados($pestana,$departamento,$idPeriodo,$objPHPExcel){

	$estiloTituloReporte = array(
		'font' => array(
			'name'      => 'Verdana',
			'bold'      => true,
			'italic'    => false,
			'strike'    => false,
			'size' =>14,
			'color'     => array(
				'rgb' => '000000'
			)
		),
		'alignment' =>  array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'rotation'   => 0,
			'wrap'          => TRUE
		)
	);

	$tituloCuadro = array(
	    'fill' => array(
	        'type' => PHPExcel_Style_Fill::FILL_SOLID,
	        'color' => array('rgb' => 'CCCCCC')
	    ),
		'borders' => array(
		    'allborders' => array(
		        'style' => PHPExcel_Style_Border::BORDER_THIN,
		        'color' => array('rgb' => '000000')
		    )
		),
		'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		),
		'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => '000000'),
	        'size'  => 10
	    )
	);

	$bordesTodos=array(
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	        )
	    )
	);

	$letraRoja=array('font'  => array(
	    //'bold'  => false,
	    'color' => array('rgb' => 'FE2E2E'),
	    'size'  => 10
	));

	$negrita=array('font'  => array(
	    'bold'  => true,
	    'color' => array('rgb' => '000000'),
	));

	$centrado = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

	$objPHPExcel->setActiveSheetIndex($pestana);

	$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(30);
	$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(20);
	$objPHPExcel->getActiveSheet()->getRowDimension(4)->setRowHeight(20);
	$objPHPExcel->getActiveSheet()->getRowDimension(5)->setRowHeight(20);

	$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
	$objPHPExcel->getActiveSheet()->setCellValue("A2","VACANTES - DEPARTAMENTO DE ".strtoupper($departamento->departamento));
	$objPHPExcel->getActiveSheet()->getStyle("A2")->applyFromArray($estiloTituloReporte);

	$fila=5;
	$filaInfo=$fila-1;
	$filaPer=$filaInfo-1;

	$modelPeriodo=PeriodosAcademicos::model()->findByPk($idPeriodo);

	$objPHPExcel->getActiveSheet()->mergeCells("A$filaPer:D$filaPer");
	$objPHPExcel->getActiveSheet()->setCellValue("A$filaPer","Período Académico: ".$modelPeriodo->anioPeriodo);
	$objPHPExcel->getActiveSheet()->getStyle("A$filaPer")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->mergeCells("A$filaInfo:D$filaInfo");
	$objPHPExcel->getActiveSheet()->setCellValue("A$filaInfo","Fecha Reporte: ".date("d/m/Y"));
	$objPHPExcel->getActiveSheet()->getStyle("A$filaInfo")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->mergeCells("F$filaInfo:I$filaInfo");
	$objPHPExcel->getActiveSheet()->setCellValue("F$filaInfo","Hora Reporte: ".date("h:i a"));
	$objPHPExcel->getActiveSheet()->getStyle("F$filaInfo")->applyFromArray($negrita);

	$objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->applyFromArray($tituloCuadro);
	$objPHPExcel->getActiveSheet()->setCellValue("A$fila","NRO");
	$objPHPExcel->getActiveSheet()->setCellValue("B$fila","ASIGNATURA");
	$objPHPExcel->getActiveSheet()->setCellValue("C$fila","TRAYECTO");
	$objPHPExcel->getActiveSheet()->setCellValue("D$fila","TRIMESTRE");
	$objPHPExcel->getActiveSheet()->setCellValue("E$fila","SECCIÓN");
	$objPHPExcel->getActiveSheet()->setCellValue("F$fila","DÍA");
	$objPHPExcel->getActiveSheet()->setCellValue("G$fila","HORA INICIO");
	$objPHPExcel->getActiveSheet()->setCellValue("H$fila","HORA FIN");
	$objPHPExcel->getActiveSheet()->setCellValue("I$fila","CANTIDAD HORAS");


	$sql="
		SELECT min(seccion) AS \"seccion\",min(s.trayecto) AS \"trayecto\",min(pas.trimestre) AS \"trimestre\",
			min(asignatura) AS \"asignatura\",count(h.id_periodo) AS \"cantidad_horas\",min(h.id_asignatura) AS \"id_asignatura\",min(h.id_seccion) AS \"id_seccion\"
		FROM horarios h
			INNER JOIN secciones s ON h.id_seccion=s.id_seccion
			INNER JOIN asignaturas a ON h.id_asignatura=a.id_asignatura
			INNER JOIN horas hor ON h.id_hora=hor.id_hora
			INNER JOIN periodos_academicos_secciones pas ON s.id_seccion=pas.id_seccion AND pas.id_periodo=$idPeriodo
		WHERE id_docente is null AND h.id_periodo=$idPeriodo AND id_departamento=$departamento->id_departamento
		GROUP BY h.id_periodo,h.id_seccion,h.id_asignatura
		ORDER BY trayecto ASC, trimestre ASC, asignatura";
	$vacantes=Yii::app()->db->createCommand($sql)->queryAll();


	$i=0;
	foreach ($vacantes as $vacante){
		$i++;
		$fila++;
		$idAsignatura=$vacante['id_asignatura'];
		$idSeccion=$vacante['id_seccion'];
		$sql="
			SELECT min(hora_inicio) AS \"hora_inicio\",max(hora_fin) AS \"hora_fin\",id_dia
			FROM horarios h
				INNER JOIN horas hor ON h.id_hora=hor.id_hora
			WHERE id_docente is null AND h.id_periodo=$idPeriodo AND id_asignatura=$idAsignatura 
				AND id_seccion=$idSeccion
			GROUP BY h.id_periodo,h.id_seccion,h.id_asignatura,id_dia
			";
		$dias=Yii::app()->db->createCommand($sql)->queryAll();
		$combinaFilas=count($dias);
		$filaFinal=($fila+$combinaFilas)-1;
		
		$objPHPExcel->getActiveSheet()->getStyle("A$fila:I$filaFinal")->applyFromArray($bordesTodos);

		$objPHPExcel->getActiveSheet()->mergeCells("A$fila:A$filaFinal");
		$objPHPExcel->getActiveSheet()->setCellValue("A$fila",$i);

		$objPHPExcel->getActiveSheet()->mergeCells("B$fila:B$filaFinal");
		$objPHPExcel->getActiveSheet()->setCellValue("B$fila",$vacante['asignatura']);

		$objPHPExcel->getActiveSheet()->mergeCells("C$fila:C$filaFinal");
		$objPHPExcel->getActiveSheet()->setCellValue("C$fila",$vacante['trayecto']);

		$objPHPExcel->getActiveSheet()->mergeCells("D$fila:D$filaFinal");
		$objPHPExcel->getActiveSheet()->setCellValue("D$fila",$vacante['trimestre']);

		$objPHPExcel->getActiveSheet()->mergeCells("E$fila:E$filaFinal");
		$objPHPExcel->getActiveSheet()->setCellValue("E$fila",$vacante['seccion']);

		$objPHPExcel->getActiveSheet()->setCellValue("F$fila",convertirDia($dias[0]['id_dia']));

		$objPHPExcel->getActiveSheet()->setCellValue("G$fila",date("h:ia",strtotime($dias[0]['hora_inicio'])));

		$objPHPExcel->getActiveSheet()->setCellValue("H$fila",date("h:ia",strtotime($dias[0]['hora_fin'])));

		$objPHPExcel->getActiveSheet()->mergeCells("I$fila:I$filaFinal");
		$objPHPExcel->getActiveSheet()->setCellValue("I$fila",$vacante['cantidad_horas']);

		$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setRowHeight(20);
		
		if(count($dias) > 1){
			for($cont=1; $cont<count($dias); $cont++){
				$fila++;

				$objPHPExcel->getActiveSheet()->setCellValue("F$fila",convertirDia($dias[$cont]['id_dia']));
				$objPHPExcel->getActiveSheet()->setCellValue("G$fila",date("h:ia",strtotime($dias[$cont]['hora_inicio'])));
				$objPHPExcel->getActiveSheet()->setCellValue("H$fila",date("h:ia",strtotime($dias[$cont]['hora_fin'])));
				
				$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setRowHeight(20);
			}
		}

	}

	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
	//$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
}


$objPHPExcel->getProperties()->setCreator("IUTOMS") //Autor
    ->setLastModifiedBy("IUTOMS") //Ultimo usuario que lo modificó
    ->setTitle("IUTOMS")
    ->setSubject("IUTOMS")
    ->setDescription("IUTOMS")
    ->setKeywords("IUTOMS")
    ->setCategory("Reporte excel");


$i=0;
foreach ($listDepartamentos as $departamento){
	if($i != 0){
		$objPHPExcel->createSheet($i);
	}
	$objPHPExcel->setActiveSheetIndex($i)->setTitle($departamento->departamento);
	generarListados($i,$departamento,$idPeriodo,$objPHPExcel);
	$i++;
}


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Vacantes.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>