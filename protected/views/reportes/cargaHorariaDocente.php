<?php
$this->breadcrumbs=array(
	'Reportes',
	'Carga Horaria Docente'
);
?>

<h1>Carga Horaria Docente</h1>

<div class="alert alert-info">Por favor seleccione los parámetros de búsqueda para mostrar la carga horaria de los docentes.</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'horarios-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_periodo'); ?>
			<?php echo $form->dropDownList($model,'id_periodo',$listPeriodos,array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'id_periodo'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->dropDownList($model,'id_departamento',$listDepartamentos,array('class'=>'form-control','empty'=>'Todos')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>


		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_tipo_contrato'); ?>
			<?php echo $form->dropDownList($model,'id_tipo_contrato',$listTiposContratos,array('class'=>'form-control','empty'=>'Todos')); ?>
			<?php echo $form->error($model,'id_tipo_contrato'); ?>
		</div>

		<div class="col-xs-4" style="padding-top: 25px;">
			<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-success')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

<?php
if(isset($cargaHorariaDocente)){
	$idPeriodo=$_POST['Horarios']['id_periodo'];
	$idTipoContrato=$_POST['Horarios']['id_tipo_contrato'];
	$idDepartamento=!empty($_POST['Horarios']['id_departamento'])?$_POST['Horarios']['id_departamento']:'';
	echo "<br>";
	echo CHtml::link(
		'Descargar PDF',
		array("reportes/imprimirCargaHorariaDocente?idPeriodo=$idPeriodo&idDepartamento=$idDepartamento&idTipoContrato=$idTipoContrato"),
		array('class'=>'btn btn-danger'));
	echo "<br>";
	?>

	<br>
	<table class="table table-bordered">
		<tr class="active">
			<th colspan="6">PARÁMETROS SELECCIONADOS</th>
		</tr>

		<tr>
			<th>Período Académico</th>
			<td>
				<?php
				$options['condition']="id_periodo=".$_POST['Horarios']['id_periodo'];
				echo PeriodosAcademicos::model()->find($options)->anioPeriodo; 
				?>
			</td>
			<th>Departamento</th>
			<td>
				<?php
				if(empty($_POST['Horarios']['id_departamento'])){
					echo "TODOS";
				}else{
					$options['condition']="id_departamento=".$_POST['Horarios']['id_departamento'];
					echo strtoupper(Departamentos::model()->find($options)->departamento); 
				}
				?>
			</td>
			<th>Tipo de Contrato</th>
			<td>
				<?php
				if(empty($_POST['Horarios']['id_tipo_contrato'])){
					echo "TODOS";
				}else{
					$options['condition']="id_tipo_contrato=".$_POST['Horarios']['id_tipo_contrato'];
					echo strtoupper(TiposContratos::model()->find($options)->tipo_contrato);
				}
				?>
			</td>
		</tr>

		<tr>
			<td colspan="6">
				<?php echo $cargaHorariaDocente ?>
			</td>
		</tr>
	</table>
	<?php
}
?>