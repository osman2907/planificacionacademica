<?php
/* @var $this DedicacionesDocentesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dedicaciones Docentes',
);

$this->menu=array(
	array('label'=>'Create DedicacionesDocentes', 'url'=>array('create')),
	array('label'=>'Manage DedicacionesDocentes', 'url'=>array('admin')),
);
?>

<h1>Dedicaciones Docentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
