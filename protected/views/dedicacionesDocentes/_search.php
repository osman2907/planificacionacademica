<?php
/* @var $this DedicacionesDocentesController */
/* @var $model DedicacionesDocentes */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_dedicacion_docente'); ?>
		<?php echo $form->textField($model,'id_dedicacion_docente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dedicacion_docente'); ?>
		<?php echo $form->textField($model,'dedicacion_docente',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_status'); ?>
		<?php echo $form->checkBox($model,'id_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->