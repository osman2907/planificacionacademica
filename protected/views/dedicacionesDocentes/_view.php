<?php
/* @var $this DedicacionesDocentesController */
/* @var $data DedicacionesDocentes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_dedicacion_docente')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_dedicacion_docente), array('view', 'id'=>$data->id_dedicacion_docente)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dedicacion_docente')); ?>:</b>
	<?php echo CHtml::encode($data->dedicacion_docente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />


</div>