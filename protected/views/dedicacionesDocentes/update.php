<?php
$this->breadcrumbs=array(
	'Gestionar Dedicaciones Docentes'=>array('admin'),
	'Modificar Dedicaciones Docentes',
);
?>

<h1>Modificar Dedicaciones Docentes</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>