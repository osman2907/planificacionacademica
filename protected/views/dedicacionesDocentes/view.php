<?php
$this->breadcrumbs=array(
	'Gestionar Dedicaciones Docentes'=>array('admin'),
	'Consultar Dedicaciones Docentes',
);
?>

<h1>Consultar Dedicaciones Docentes</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_dedicacion_docente',
		'dedicacion_docente',
		'cantidad_horas',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Dedicaciones Docentes',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
