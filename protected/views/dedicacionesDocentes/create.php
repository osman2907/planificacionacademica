<?php
$this->breadcrumbs=array(
	'Gestionar Dedicaciones Docentes'=>array('admin'),
	'Registrar Dedicaciones Docentes',
);
?>

<h1>Registrar Dedicaciones Docentes</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>