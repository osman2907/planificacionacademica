<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dedicaciones-docentes-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_dedicacion_docente'); ?>
			<?php echo $form->textField($model,'id_dedicacion_docente',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'id_dedicacion_docente'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'dedicacion_docente'); ?>
			<?php echo $form->textField($model,'dedicacion_docente',array('class'=>'form-control','maxlength'=>200)); ?>
			<?php echo $form->error($model,'dedicacion_docente'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'cantidad_horas'); ?>
			<?php echo $form->textField($model,'cantidad_horas',array('class'=>'form-control solo-numero','maxlength'=>3)); ?>
			<?php echo $form->error($model,'cantidad_horas'); ?>
		</div>

		<?php if(!$model->isNewRecord){ ?>
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkbox($model,'id_status'); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->