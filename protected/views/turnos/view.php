<?php
$this->breadcrumbs=array(
	'Gestionar Turnos'=>array('admin'),
	'Consultar Turnos',
);
?>

<h1>Consultar Turnos</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_turno',
		'turno',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Turnos',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
