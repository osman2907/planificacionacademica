<?php
$this->breadcrumbs=array(
	'Gestionar Turnos'=>array('admin'),
	'Registrar Turnos',
);
?>

<h1>Registrar Turnos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>