<?php
$this->breadcrumbs=array(
	'Gestionar Períodos Académicos'=>array('admin'),
	'Consultar Períodos Académicos',
);
?>

<h1>Consultar Períodos Académicos</h1>

<div>
	<?php
	echo CHtml::link(
		'Gestionar Períodos Académicos',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>

<br>
<table class="table table-bordered">
	<tr class="active">
		<th>Año</th>
		<th>Período</th>
		<th>Fecha Inicio</th>
		<th>Fecha Fin</th>
	</tr>

	<tr>
		<td><?php echo $model->anio; ?></td>
		<td><?php echo $model->getPeriodoRomano(); ?></td>
		<td><?php echo date("d/m/Y",strtotime($model->fecha_inicio)); ?></td>
		<td><?php echo date("d/m/Y",strtotime($model->fecha_fin)); ?></td>
	</tr>

	<tr class="active">
		<th>Fecha Inicio Planif.</th>
		<th>Fecha Fin Planif.</th>
		<th colspan="2">Estatus</th>
	</tr>

	<tr>
		<td><?php echo date("d/m/Y",strtotime($model->fecha_inicio_plan)); ?></td>
		<td><?php echo date("d/m/Y",strtotime($model->fecha_fin_plan)); ?></td>
		<td colspan="2"><?php echo  $model->getStatusBandeja();?></td>
	</tr>
</table>


<br><br>
<table class="table table-bordered">
	<tr class="active">
		<th class="centro">SECCIONES ACTIVAS PARA ESTE PERÍODO ACADÉMICO</th>
	</tr>

	<tr>
		<td>

			<ul class="nav nav-tabs" role="tablist">
				<?php
				$i=0;
				foreach ($modelDepartamentos as $departamento){
					$i++;
					$activo=$i==1?'active':'';
					$idDepartamento=$departamento->id_departamento;
					?>
					<li role="presentation" class="<?php echo $activo; ?>">
			        	<a href="#secciones-<?php echo $idDepartamento ?>" aria-controls="<?php echo $idDepartamento ?>" role="tab" data-toggle="tab">
			        		<?php echo $departamento->departamento; ?>
			        	</a>
			        </li>
					<?php	
				} 
				?>
		    </ul>
		    
		    <div class="tab-content" style="padding:10px;">
		    	<?php
		    	reset($modelDepartamentos);
		    	$i=0;
				foreach ($modelDepartamentos as $departamento){
					$i++;
					$activo=$i==1?'active':'';
					$idDepartamento=$departamento->id_departamento;
					?>
					<div role="tabpanel" class="tab-pane <?php echo $activo ?>" id="secciones-<?php echo $idDepartamento; ?>">
			            <?php 
			            echo "<div class='centro negrita'>".strtoupper($departamento->departamento)."</div>";
			            $this->renderPartial("_secciones",array("idPeriodo"=>$model->id_periodo,"idDepartamento"=>$idDepartamento));
			            ?>
			        </div>	
					<?php	
				} 
				?>
		    </div>

		</td>
	</tr>
</table>


<br><br>
<table class="table table-bordered">
	<tr class="active">
		<th class="centro">DOCENTES ACTIVOS PARA ESTE PERÍODO ACADÉMICO</th>
	</tr>

	<tr>
		<td>

			<ul class="nav nav-tabs" role="tablist">
				<?php
				$i=0;
				reset($modelDepartamentos);
				foreach ($modelDepartamentos as $departamento){
					$i++;
					$activo=$i==1?'active':'';
					$idDepartamento=$departamento->id_departamento;
					?>
					<li role="presentation" class="<?php echo $activo; ?>">
			        	<a href="#docentes-<?php echo $idDepartamento ?>" aria-controls="<?php echo $idDepartamento ?>" role="tab" data-toggle="tab">
			        		<?php echo $departamento->departamento; ?>
			        	</a>
			        </li>
					<?php	
				} 
				?>
		    </ul>
		    
		    <div class="tab-content" style="padding:10px;">
		    	<?php
		    	reset($modelDepartamentos);
		    	$i=0;
				foreach ($modelDepartamentos as $departamento){
					$i++;
					$activo=$i==1?'active':'';
					$idDepartamento=$departamento->id_departamento;
					?>
					<div role="tabpanel" class="tab-pane <?php echo $activo ?>" id="docentes-<?php echo $idDepartamento; ?>">
			            <?php 
			            echo "<div class='centro negrita'>".strtoupper($departamento->departamento)."</div>";
			            $this->renderPartial("_dedicacionesDocentes",array("idPeriodo"=>$model->id_periodo,"idDepartamento"=>$idDepartamento));
			            ?>
			        </div>	
					<?php	
				} 
				?>
		    </div>

		</td>
	</tr>
</table>
