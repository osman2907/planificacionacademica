<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click","#btn-modificar",function(){
			bootbox.confirm("¿Seguro que desea modificar esta sección?",function(respuesta){
				if(respuesta){
					$("form").submit();
				}
			});
			return false;
		});
	});
</script>

<?php
$this->breadcrumbs=array(
	'Gestionar Períodos Académicos'=>array('admin'),
	'Modificar Períodos Académicos'=>array("update?id=$idPeriodo"),
	'Modificar Sección en Período Académico'
);
?>

<h1>Modificar Sección en Período Académico</h1>

<div>
	<?php
	echo CHtml::link(
		'Regresar',
		array("update?id=$idPeriodo'"),
		array('class'=>'btn btn-danger'));
	?>
</div>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'periodos-academicos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'periodo'); ?>
			<?php echo $form->hiddenField($model,'id_periodo',array()); ?>
			<?php echo $form->textField($model,'periodo',array('class'=>'form-control solo-numero','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'periodo'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'seccion'); ?>
			<?php echo $form->textField($model,'seccion',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'seccion'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'trayecto'); ?>
			<?php echo $form->textField($model,'trayecto',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'trayecto'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_malla'); ?>
			<?php echo $form->dropDownList($model,'id_malla',$listMallas,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_malla'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'trimestre'); ?>
			<?php echo $form->textField($model,'trimestre',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'trimestre'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-3">
			<?php echo CHtml::submitButton('Modificar',array('class'=>'btn btn-primary','id'=>'btn-modificar')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->