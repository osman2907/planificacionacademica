<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click",".actualizar-docentes",function(){
			url=$(this).attr("href");
			bootbox.confirm("¿Seguro que desea actualizar los datos docentes?",function(resp){
				if(resp){
					document.location=url;
				}
			});
			return false;
		});
	});
</script>

<?php
$this->breadcrumbs=array(
	'Gestionar Períodos Académicos',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#periodos-academicos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
var decorarGrilla=function(){
	$('.grid-view .items thead tr:first-child th:first-child').addClass('brsi');
    $('.grid-view .items thead tr:first-child th:last-child').addClass('brsd');
    $('.grid-view .items thead .filters td input').addClass('form-control');
    $('.grid-view .items thead .filters td select').addClass('form-control');
}
decorarGrilla();
");
?>

<h1>Gestionar Períodos Académicos</h1>

<?php echo CHtml::link(
			'Registrar Períodos Académicos',
			array('create'),
			array('class'=>'btn btn-success')
);
$template="{view}{update}";
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'periodos-academicos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'decorarGrilla',
	'columns'=>array(
		/*array(
			'name'=>'id_periodo',
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),*/
		'anio',
		array(
			'name'=>'periodo',
			'type'=>'raw',
			'value'=>'$data->getPeriodoRomano()'
		),
		array(
			'name'=>'fecha_inicio',
			'value'=>'date("d/m/Y",strtotime($data->fecha_inicio))'
		),
		array(
			'name'=>'fecha_fin',
			'value'=>'date("d/m/Y",strtotime($data->fecha_fin))'
		),
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>'$data->getStatusBandeja()'
		),
		array(
            'header'=>'Acciones',
            "type"=>'raw',
            'value'=>function($data){
                $ruta=Yii::app()->request->baseUrl."/index.php";
                $idPeriodo=$data->id_periodo;
                
                $html="
                    <div class='dropdown'>
                        <button class='btn btn-default dropdown-toggle type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                            <i class='glyphicon glyphicon-cog'></i>
                            <span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu' aria-labelledby='dropdownMenu1' style='text-align:left'>
                            <li>
                                <a href='$ruta/periodosAcademicos/view/$idPeriodo'>
                                    <i class='glyphicon glyphicon-eye-open'></i>Ver
                                </a>
                            </li>

                            <li>
                                <a href='$ruta/periodosAcademicos/update/$idPeriodo'>
                                    <i class='glyphicon glyphicon-pencil'></i>Modificar
                                </a>
                            </li>

                            <li>
                                <a class='actualizar-docentes' href='$ruta/periodosAcademicos/updateDocentes/$idPeriodo'>
                                    <i class='glyphicon glyphicon-refresh'></i>Actualizar Docentes
                                </a>
                            </li>

                            <li>
                                <a class='actualizar-docentes' href='$ruta/periodosAcademicos/updateSecciones/$idPeriodo'>
                                    <i class='glyphicon glyphicon-refresh'></i>Actualizar Secciones
                                </a>
                            </li>
                        </ul>
                    </div>
                ";
                return $html;
            },
            'htmlOptions'=>array('style'=>'text-align:center; width:80px;'),
        ),
	),
)); ?>
