<?php
$opciones['condition']='id_periodo='.$idPeriodo.' AND "idSeccion"."id_departamento"='.$idDepartamento;
$opciones['order']='"idSeccion"."trayecto" ASC, "idSeccion"."id_turno" ASC, "idSeccion"."seccion" ASC';
$modelPeriodosSecciones=PeriodosAcademicosSecciones::model()->with('idSeccion')->findAll($opciones);

?>
<table class="table table-bordered">
	<tr class="active">
		<th>Sección</th>
		<th>Turno</th>
		<th>Trayecto</th>
		<th>Trimestre</th>
		<th>Malla</th>
		<?php if(isset($modificar)){ ?>
			<th>Acciones</th>
		<?php } ?>
	</tr>
	
	<?php
	foreach ($modelPeriodosSecciones as $periodoSeccion){
		?>
		<tr>
			<td><?php echo $periodoSeccion->idSeccion->seccion; ?></td>
			<td><?php echo $periodoSeccion->idSeccion->idTurno->turno; ?></td>
			<td><?php echo $periodoSeccion->idSeccion->trayecto; ?></td>
			<td><?php echo $periodoSeccion->trimestre; ?></td>
			<td><?php echo $periodoSeccion->idMalla->nombre_malla; ?></td>
			<?php if(isset($modificar)){ ?>
				<td>
					<a href='<?php echo Yii::app()->request->baseUrl."/index.php/PeriodosAcademicos/modificarPeriodoSeccion?idPeriodo=$idPeriodo&idSeccion=$periodoSeccion->id_seccion"?>' class="modificar"><span class="glyphicon glyphicon-pencil"></span></a>
					&nbsp;
					<a href="#" class="eliminar eliminar-ps" data-id-periodo="<?php echo $periodoSeccion->id_periodo ?>" data-id-seccion="<?php echo $periodoSeccion->id_seccion ?>"><span class="glyphicon glyphicon-remove"></span></a>
				</td>
			<?php } ?>
		</tr>
		<?php
	}
	?>
</table>