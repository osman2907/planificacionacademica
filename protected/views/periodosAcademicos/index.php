<?php
/* @var $this PeriodosAcademicosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Periodos Academicoses',
);

$this->menu=array(
	array('label'=>'Create PeriodosAcademicos', 'url'=>array('create')),
	array('label'=>'Manage PeriodosAcademicos', 'url'=>array('admin')),
);
?>

<h1>Periodos Academicoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
