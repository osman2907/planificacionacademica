<?php
/* @var $this PeriodosAcademicosController */
/* @var $model PeriodosAcademicos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'periodos-academicos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'anio'); ?>
			<?php echo $form->textField($model,'anio',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'anio'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'periodo'); ?>
			<?php echo $form->dropDownList($model,'periodo',$listPeriodos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'periodo'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'fecha_inicio'); ?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'model' => $model,
			    'language' => 'es',
			    'attribute' => 'fecha_inicio',
			    'htmlOptions' => array(
			        'class'=>'form-control',
			        'readonly'=>'readonly'
			    ),
			));
			?>
			<?php echo $form->error($model,'fecha_inicio'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'fecha_fin'); ?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'model' => $model,
			    'language' => 'es',
			    'attribute' => 'fecha_fin',
			    'htmlOptions' => array(
			        'class'=>'form-control',
			        'readonly'=>'readonly'
			    ),
			));
			?>
			<?php echo $form->error($model,'fecha_fin'); ?>
		</div>
	</div>


	<div class="row">
		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'fecha_inicio_plan'); ?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'model' => $model,
			    'language' => 'es',
			    'attribute' => 'fecha_inicio_plan',
			    'htmlOptions' => array(
			        'class'=>'form-control',
			        'readonly'=>'readonly'
			    ),
			));
			?>
			<?php echo $form->error($model,'fecha_inicio_plan'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'fecha_fin_plan'); ?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'model' => $model,
			    'language' => 'es',
			    'attribute' => 'fecha_fin_plan',
			    'htmlOptions' => array(
			        'class'=>'form-control',
			        'readonly'=>'readonly'
			    ),
			));
			?>
			<?php echo $form->error($model,'fecha_fin_plan'); ?>
		</div>

		<?php if(!$model->isNewRecord){ ?>
			<div class="col-xs-3">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkBox($model,'id_status'); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xs-3">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord?'btn btn-success':'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<?php if(!$model->isNewRecord){ ?>
	<br><br>
	<table class="table table-bordered">
		<tr class="active">
			<th class="centro">SECCIONES ACTIVAS PARA ESTE PERÍODO ACADÉMICO</th>
		</tr>

		<tr>
			<td>

				<ul class="nav nav-tabs" role="tablist">
					<?php
					$i=0;
					foreach ($modelDepartamentos as $departamento){
						$i++;
						$activo=$i==1?'active':'';
						$idDepartamento=$departamento->id_departamento;
						?>
						<li role="presentation" class="<?php echo $activo; ?>">
				        	<a href="#secciones-<?php echo $idDepartamento ?>" aria-controls="<?php echo $idDepartamento ?>" role="tab" data-toggle="tab">
				        		<?php echo $departamento->departamento; ?>
				        	</a>
				        </li>
						<?php	
					} 
					?>
			    </ul>
			    
			    <div class="tab-content" style="padding:10px;">
			    	<?php
			    	reset($modelDepartamentos);
			    	$i=0;
					foreach ($modelDepartamentos as $departamento){
						$i++;
						$activo=$i==1?'active':'';
						$idDepartamento=$departamento->id_departamento;
						?>
						<div role="tabpanel" class="tab-pane <?php echo $activo ?>" id="secciones-<?php echo $idDepartamento; ?>">
				            <?php 
				            echo "<div class='centro negrita'>".strtoupper($departamento->departamento)."</div>";
				            $this->renderPartial("_secciones",array("idPeriodo"=>$model->id_periodo,"idDepartamento"=>$idDepartamento,'modificar'=>1));
				            ?>
				        </div>	
						<?php	
					} 
					?>
			    </div>

			</td>
		</tr>
	</table>


	<br><br>
	<table class="table table-bordered">
		<tr class="active">
			<th class="centro">DOCENTES ACTIVOS PARA ESTE PERÍODO ACADÉMICO</th>
		</tr>

		<tr>
			<td>

				<ul class="nav nav-tabs" role="tablist">
					<?php
					$i=0;
					reset($modelDepartamentos);
					foreach ($modelDepartamentos as $departamento){
						$i++;
						$activo=$i==1?'active':'';
						$idDepartamento=$departamento->id_departamento;
						?>
						<li role="presentation" class="<?php echo $activo; ?>">
				        	<a href="#docentes-<?php echo $idDepartamento ?>" aria-controls="<?php echo $idDepartamento ?>" role="tab" data-toggle="tab">
				        		<?php echo $departamento->departamento; ?>
				        	</a>
				        </li>
						<?php	
					} 
					?>
			    </ul>
			    
			    <div class="tab-content" style="padding:10px;">
			    	<?php
			    	reset($modelDepartamentos);
			    	$i=0;
					foreach ($modelDepartamentos as $departamento){
						$i++;
						$activo=$i==1?'active':'';
						$idDepartamento=$departamento->id_departamento;
						?>
						<div role="tabpanel" class="tab-pane <?php echo $activo ?>" id="docentes-<?php echo $idDepartamento; ?>">
				            <?php 
				            echo "<div class='centro negrita'>".strtoupper($departamento->departamento)."</div>";
				            $this->renderPartial("_dedicacionesDocentes",array("idPeriodo"=>$model->id_periodo,"idDepartamento"=>$idDepartamento,'modificar'=>1));
				            ?>
				        </div>	
						<?php	
					} 
					?>
			    </div>

			</td>
		</tr>
	</table>
<?php } ?>