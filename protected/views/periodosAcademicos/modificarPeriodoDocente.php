<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click","#btn-modificar",function(){
			bootbox.confirm("¿Seguro que desea modificar este docente?",function(respuesta){
				if(respuesta){
					$("form").submit();
				}
			});
			return false;
		});
	});
</script>

<?php
$this->breadcrumbs=array(
	'Gestionar Períodos Académicos'=>array('admin'),
	'Modificar Períodos Académicos'=>array("update?id=$idPeriodo"),
	'Modificar Docente en Período Académico'
);
?>

<h1>Modificar Docente en Período Académico</h1>

<div>
	<?php
	echo CHtml::link(
		'Regresar',
		array("update?id=$idPeriodo'"),
		array('class'=>'btn btn-danger'));
	?>
</div>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'periodos-academicos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'periodo'); ?>
			<?php echo $form->hiddenField($model,'id_periodo',array()); ?>
			<?php echo $form->textField($model,'periodo',array('class'=>'form-control solo-numero','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'periodo'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'docente'); ?>
			<?php echo $form->textField($model,'docente',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'docente'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'departamento'); ?>
			<?php echo $form->textField($model,'departamento',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'departamento'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_tipo_contrato'); ?>
			<?php echo $form->dropDownList($model,'id_tipo_contrato',$listTiposContratos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_tipo_contrato'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_dedicacion_docente'); ?>
			<?php echo $form->dropDownList($model,'id_dedicacion_docente',$listTiposContratos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_dedicacion_docente'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_categoria_docente'); ?>
			<?php echo $form->dropDownList($model,'id_categoria_docente',$listCategorias,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_categoria_docente'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-3">
			<?php echo CHtml::submitButton('Modificar',array('class'=>'btn btn-primary','id'=>'btn-modificar')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->