<?php
/* @var $this PeriodosAcademicosController */
/* @var $data PeriodosAcademicos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_periodo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_periodo), array('view', 'id'=>$data->id_periodo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anio')); ?>:</b>
	<?php echo CHtml::encode($data->anio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('periodo')); ?>:</b>
	<?php echo CHtml::encode($data->periodo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_inicio')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_inicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_fin')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_fin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_inicio_plan')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_inicio_plan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_fin_plan')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_fin_plan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />

	*/ ?>

</div>