<style type="text/css">
.modificar, .modificar:hover, .modificar:focus{
	color: #000000;
}

.eliminar, .eliminar:hover, .eliminar:focus{
	color: #000000;
}	
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click",'.eliminar-ps',function(){
			idPeriodo=$(this).data('id-periodo');
			idSeccion=$(this).data('id-seccion');

			bootbox.confirm("¿Seguro que desea eliminar esta sección?",function(respuesta){
				if(respuesta){
					$.ajax({
						async:true,
						type: "POST",
						url:"<?php echo $this->createUrl('periodosAcademicos/eliminarPeriodoSeccion')?>",
						data:{id_periodo:idPeriodo,id_seccion:idSeccion},
						success: function(respuesta){
							if(respuesta){
								bootbox.alert(respuesta);
							}else{
								bootbox.alert("Sección eliminada con éxito",function(){
									location.reload();
								});
							}
						},
						error: function(){
							bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
								location.reload();
							});
						}
					});
				}
			});
			return false;

		});


		$(document).on("click",'.eliminar-pd',function(){
			idPeriodo=$(this).data('id-periodo');
			idDocente=$(this).data('id-docente');

			bootbox.confirm("¿Seguro que desea eliminar este docente?",function(respuesta){
				if(respuesta){
					$.ajax({
						async:true,
						type: "POST",
						url:"<?php echo $this->createUrl('periodosAcademicos/eliminarPeriodoDocente')?>",
						data:{id_periodo:idPeriodo,id_docente:idDocente},
						success: function(respuesta){
							if(respuesta){
								bootbox.alert(respuesta);
							}else{
								bootbox.alert("Docente eliminado con éxito",function(){
									location.reload();
								});
							}
						},
						error: function(){
							bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
								location.reload();
							});
						}
					});
				}
			});
			return false;

		});
	});
</script>

<?php
$this->breadcrumbs=array(
	'Gestionar Períodos Académicos'=>array('admin'),
	'Modificar Períodos Académicos',
);
?>

<h1>Modificar Períodos Académicos</h1>

<?php $this->renderPartial('_form', compact('model','listPeriodos','modelDepartamentos')); ?>