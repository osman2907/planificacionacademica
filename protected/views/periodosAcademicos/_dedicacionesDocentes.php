<?php
$opciones['condition']="id_periodo=$idPeriodo AND t.id_departamento=$idDepartamento";
$opciones['order']='"idDocente"."apellido1" ASC';
$modelPeriodosDocentes=PeriodosAcademicosDocentes::model()->with('idDocente')->findAll($opciones);

?>
<table class="table table-bordered">
	<tr class="active">
		<th>Cédula</th>
		<th>Apellidos</th>
		<th>Nombres</th>
		<th>Dedicación</th>
		<th>Tipo Contrato</th>
		<th>Categoría</th>
		<?php if(isset($modificar)){ ?>
			<th>Acciones</th>
		<?php } ?>
	</tr>
	
	<?php
	foreach ($modelPeriodosDocentes as $periodoDocente){
		?>
		<tr>
			<td><?php echo $periodoDocente->idDocente->cedula; ?></td>
			<td><?php echo $periodoDocente->idDocente->apellidos; ?></td>
			<td><?php echo $periodoDocente->idDocente->nombres; ?></td>
			<td><?php echo $periodoDocente->idDedicacionDocente->dedicacion_docente; ?></td>
			<td><?php echo $periodoDocente->idTipoContrato->tipo_contrato; ?></td>
			<td><?php echo $periodoDocente->idCategoriaDocente->categoria_docente; ?></td>
			<?php if(isset($modificar)){ ?>
				<td>
					<a href='<?php echo Yii::app()->request->baseUrl."/index.php/PeriodosAcademicos/modificarPeriodoDocente?idPeriodo=$idPeriodo&idDocente=$periodoDocente->id_docente"?>' class="modificar"><span class="glyphicon glyphicon-pencil"></span></a>
					&nbsp;
					<a href="#" class="eliminar eliminar-pd" data-id-periodo="<?php echo $periodoDocente->id_periodo ?>" data-id-docente="<?php echo $periodoDocente->id_docente ?>"><span class="glyphicon glyphicon-remove"></span></a>
				</td>
			<?php } ?>
		</tr>
		<?php
	}
	?>
</table>