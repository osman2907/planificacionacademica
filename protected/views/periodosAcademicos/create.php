<?php
$this->breadcrumbs=array(
	'Gestionar Períodos Académicos'=>array('admin'),
	'Registrar Períodos Académicos',
);
?>

<h1>Registrar Períodos Académicos</h1>

<?php $this->renderPartial('_form',compact('model','listPeriodos')); ?>