<?php /* @var $this Controller */ ?>
<?php
Yii::app()->clientScript->scriptMap=array(
    'jquery.js'=>false,
);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!--<meta name="language" content="en">-->

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu/styles.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery/jquery-ui-1.12.0/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery/jquery-ui-1.12.0/jquery-ui.theme.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/chosen/chosen.min.css">

	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery/jquery-ui-1.12.0/jquery-ui.min.js"></script>
	<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>-->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/css/menu/script.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/librerias.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar382/fullcalendar.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar382/fullcalendar.print.min.css" media="print">

	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar382/lib/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar382/fullcalendar.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar382/locale/es-do.js"></script>


	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/date.format.js"></script>

	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/chosen/chosen.jquery.min.js"></script>


	<script>
		$(document).ready(function(){

			/*$(document).ajaxStart(function(){
				$("#cargando").show();
			});

			$(document).ajaxStop(function(){
				$("#cargando").hide();
			});

			$.ajaxSetup({
				complete:function(){
					$('#cargando').fadeOut();
				},
				success:function(){
					$('#cargando').fadeOut();
				}
			});*/

			validarSesion=function(){
			    url="<?php echo $this->createUrl('site/validarSesion')?>";
			    respuesta=consultarPHP(url,{},'html',false);
			    if(!respuesta){
			    	bootbox.alert("Se ha vencido su sesión. Ingrese nuevamente",function(){
			    		document.location="<?php echo $this->createUrl('site/index')?>";
			    	});
			    }
			}
		});
	</script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<script>
		$(document).ready(function(){
			$("#cerrar-sesion").click(function(){
				bootbox.confirm("¿Seguro que desea cerrar sesión?",function(respuesta){
					if(respuesta){
						document.location="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/logout";
					}
				});
				return false;
			});

			bootbox.setDefaults({
          		locale: "es"
		    });
		});
	</script>
</head>

<body>
<div id="cargando">
	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" title="Cargando" width="350px">
</div>
<div class="container" id="page">

	<div id="header">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/membrete-iutoms.jpg" title="Instituto Universitario de Tecnología del Oeste Mariscal Sucre" style="width: 100%">
		<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<!-- Menu Principal-->
	<div id='cssmenu' style="z-index:10">
		<ul>
		   	<li><a href='<?php echo Yii::app()->request->baseUrl;?>'><span>Inicio</span></a></li>
		   	<?php  if(Yii::app()->user->isGuest){?>
		   		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/cruge/ui/login'><span>Iniciar Sesión</span></a></li>
	   		<?php } ?>
		   	<?php  if(!Yii::app()->user->isGuest){?>
				<li class='has-sub'><a href='#'><span>Planificación</span></a>
					<ul>
						<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/horarios/busqueda'><span>Horarios</span></a></li>
						<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/periodosAcademicos/admin'><span>Períodos Académicos</span></a></li>
						<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/docentes/admin'><span>Docentes</span></a></li>
						<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/secciones/admin'><span>Secciones</span></a></li>
					</ul>
			   	</li>


			   	<?php if(Yii::app()->user->checkAccess("administrador") || Yii::app()->user->checkAccess("director")){?>
					<li class='has-sub'><a href='#'><span>Reportes</span></a>
						<ul>
							<li>
								<a href='#'><span>Listados</span></a>
								<ul>
									<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/reportes/horariosSecciones'><span>Horarios por Secciones</span></a></li>
									<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/reportes/horariosAulas'><span>Horarios por Aulas</span></a></li>
									<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/reportes/horariosDocentes'><span>Horarios por Docentes</span></a></li>
									<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/reportes/auditoriaDocente'><span>Auditoría Docente</span></a></li>
									<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/reportes/vacantes'><span>Vacantes</span></a></li>
									<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/reportes/docentesSinProvidencia'><span>Docentes Sin Providencia</span></a></li>
									<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/reportes/cargaHorariaDocente'><span>Carga Horaria Docente</span></a></li>
								</ul>
							</li>
						</ul>
					</li>
				<?php }?>

				
				<?php if(Yii::app()->user->checkAccess("administrador")){?>
					<li class='has-sub'><a href='#'><span>Mantenimiento</span></a>
						<ul>
							<li><a href='<?php echo Yii::app()->user->ui->userManagementAdminUrl?>'><span>Gestión Usuarios</span></a></li>
							<li class='has-sub'><a href='#'><span>Tablas Referenciales</span></a>
							    <ul>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/aulas/admin'><span>Aulas</span></a></li>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/categoriasDocentes/admin'><span>Categorías Docentes</span></a></li>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/dedicacionesDocentes/admin'><span>Dedicaciones Docentes</span></a></li>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/departamentos/admin'><span>Departamentos</span></a></li>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/mallas/admin'><span>Mallas Curriculares</span></a></li>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/sedes/admin'><span>Sedes</span></a></li>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/tiposContratos/admin'><span>Tipos de Contratos</span></a></li>
						    		<li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/turnos/admin'><span>Turnos</span></a></li>
							    </ul>
							</li>
						</ul>
					</li>
				<?php } ?>

				<?php
				$userId=Yii::app()->user->getid();
				$usuario=CrugeUserModel::model()->findByPk($userId);
				?>
				<li class='has-sub'><a href='#'><span><?php echo $usuario->username?></span></a>
					<ul>
						<!--<li><a href='<?php //echo Yii::app()->request->baseUrl;?>/index.php/site/changePassword'><span>Cambiar Contraseña</span></a></li>-->
						<li><a id="cerrar-sesion" href='<?php echo Yii::app()->request->baseUrl;?>/index.php/site/logout'><span>Cerrar Sesión</span></a></li>
					</ul>
				</li>

			   	<!--<li class='last'>
			   		<a id="cerrar-sesion" href='<?php echo Yii::app()->request->baseUrl;?>/index.php/site/logout'>
			   			<span>Cerrar Sesión</span>
		   			</a>
		   		</li>-->
			<?php }?>
		</ul>
	</div>
	<?php  if(!Yii::app()->user->isGuest){?>
		<!--<div class="usuario-conectado">
			<h4><span class="label label-info">Usuario Conectado: <?php echo $usuario->username?></span></h4>
		</div>-->
	<?php }?>
	<!-- Fin Menú Principal-->

	
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message){
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
	?>
	
	<?php echo $content; ?>

	<div class="clear"></div>

	<?php echo Yii::app()->log->processLogs(null); ?>

	<!--pie de página-->
	<div id="footer">
		<!--Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>-->
		<!--All Rights Reserved.<br/>-->
		<?php //echo Yii::powered(); ?>
		<strong>
			Instituto Universitario de Tecnología del Oeste "Mariscal Sucre"<br>
			División de Planificación Académica
		</strong>
	</div><!-- footer -->
	<!--Fin pie de página-->
	

</div><!-- page -->
<?php //echo Yii::app()->user->ui->loginLink; ?>
<?php //echo Yii::app()->user->ui->displayErrorConsole(); ?>
</body>
</html>
