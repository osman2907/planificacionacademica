<?php
/* @var $this SedesController */
/* @var $data Sedes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sede')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_sede), array('view', 'id'=>$data->id_sede)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sede')); ?>:</b>
	<?php echo CHtml::encode($data->sede); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />


</div>