<?php
$this->breadcrumbs=array(
	'Gestionar Sedes'=>array('admin'),
	'Consultar Sedes',
);
?>

<h1>Consultar Sedes</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_sede',
		'sede',
		'direccion',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Sedes',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
