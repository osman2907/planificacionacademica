<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sedes-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-12">
			<?php echo $form->labelEx($model,'sede'); ?>
			<?php echo $form->textField($model,'sede',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'sede'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo $form->labelEx($model,'direccion'); ?>
			<?php echo $form->textArea($model,'direccion',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'direccion'); ?>
		</div>
	</div>


	<?php if(!$model->isNewRecord){ ?>
		<div class="row">
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkbox($model,'id_status'); ?> Activa
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->