<?php
$this->breadcrumbs=array(
	'Gestionar Tipos Contratos'=>array('admin'),
	'Registrar Tipos Contratos',
);
?>

<h1>Registrar Tipos Contratos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>