<?php
$this->breadcrumbs=array(
	'Gestionar Tipos Contratos'=>array('admin'),
	'Consultar Tipos Contratos',
);
?>

<h1>Consultar Tipos Contratos</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tipo_contrato',
		'tipo_contrato',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Tipos Contratos',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
