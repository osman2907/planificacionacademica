<?php
$this->breadcrumbs=array(
	'Gestionar Tipos Contratos'=>array('admin'),
	'Modificar Tipos Contratos',
);
?>

<h1>Modificar Tipos Contratos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>