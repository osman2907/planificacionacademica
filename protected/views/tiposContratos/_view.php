<?php
/* @var $this TiposContratosController */
/* @var $data TiposContratos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tipo_contrato')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tipo_contrato), array('view', 'id'=>$data->id_tipo_contrato)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_contrato')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_contrato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />


</div>