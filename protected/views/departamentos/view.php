<?php
$this->breadcrumbs=array(
	'Gestionar Departamentos'=>array('admin'),
	'Consultar Departamentos',
);
?>

<h1>Consultar Departamentos</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_departamento',
		'departamento',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Departamentos',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
