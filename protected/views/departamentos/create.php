<?php
$this->breadcrumbs=array(
	'Gestionar Departamentos'=>array('admin'),
	'Registrar Departamentos',
);
?>

<h1>Registrar Departamentos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>