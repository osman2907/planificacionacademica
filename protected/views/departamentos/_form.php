<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'departamentos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->textField($model,'id_departamento',array('class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>

		<div class="col-xs-8">
			<?php echo $form->labelEx($model,'departamento'); ?>
			<?php echo $form->textField($model,'departamento',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'departamento'); ?>
		</div>
	</div>

	<?php if(!$model->isNewRecord){ ?>
		<div class="row">
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkBox($model,'id_status',array('class'=>'')); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord?'btn btn-success':'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->