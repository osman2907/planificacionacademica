<style type="text/css">
	.modal-dialog{
		width: 75% !important;
	}
</style>

<script>
$(document).ready(function($){

    $('#calendar').fullCalendar({
		/* Se ocultaron por el CSS que está en este mismo archivo
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listWeek'
		},*/
		defaultDate: '2018-10-01',
		defaultView:'agendaWeek',
		navLinks: true,
		/*editable: true,*/
		selectable: true,
		eventLimit: true,
		allDaySlot:false,
		weekends:true,
		slotDuration:'00:30:00',
		minTime: "07:00:00",
		maxTime: "22:00:00",
		hiddenDays:[0],
		events:<?php echo $listEventos; ?>,
		businessHours:<?php echo $horas ?>,

		select: function(start, end, jsEvent) {
			idPeriodo=$("#Horarios_id_periodo").val();
			idSeccion=$("#Horarios_id_seccion").val();
			starttime = $.fullCalendar.moment(start).format('HH:mm');
			endtime = $.fullCalendar.moment(end).format('HH:mm');
			day=$.fullCalendar.moment(end).format('d');
			$.ajax({
				async:true,
				type: "POST",
				url:"<?php echo $this->createUrl('horarios/registrarHorario')?>",
				data:{id_periodo:idPeriodo,id_seccion:idSeccion,hora:starttime,dia:day},
				success: function(respuesta){
					bootbox.dialog({
		                size: 'large',
		                title: 'Registrar Horario',
		                message: respuesta
		            });
				},
				error: function(){
					bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
						location.reload();
					});
				}
			});
       	},

       	eventClick:  function(event, jsEvent, view){
            $.ajax({
				async:true,
				type: "POST",
				url:"<?php echo $this->createUrl('horarios/modificarHorario')?>",
				data:{id_periodo:event.id_periodo,id_seccion:event.id_seccion,id_dia:event.id_dia,
					id_hora:event.id_hora,cantidad_horas:event.cantidad_horas,id_asignatura:event.id_asignatura,id_docente:event.id_docente,id_aula:event.id_aula},
				success: function(respuesta){
					bootbox.dialog({
		                size: 'large',
		                title: 'Modificar Horario',
		                message: respuesta
		            });
				},
				error: function(){
					bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
						location.reload();
					});
				}
			});
        },

    });

    tituloDias=function(){
    	$($(".fc-day-header")[0]).html("Lunes");
    	$($(".fc-day-header")[1]).html("Martes");
    	$($(".fc-day-header")[2]).html("Miércoles");
    	$($(".fc-day-header")[3]).html("Jueves");
    	$($(".fc-day-header")[4]).html("Viernes");
    	$($(".fc-day-header")[5]).html("Sábado");
    }
    tituloDias();

    calcularHoraFin=function(){
    	horas=<?php echo $horas ?>;
    	horaInicio=$("#Horarios_hora_inicio").val();
    	cantidadHoras=$("#Horarios_cantidad_horas").val();
    	if(cantidadHoras > 6){
    		bootbox.alert("El máximo de horas por bloque es de 6",function(){
    			$("#Horarios_cantidad_horas").val(1);
    			calcularHoraFin();
    		});
    		return false;
    	}
    	idHora=$("#Horarios_id_hora").val();
    	ultimaHora=parseInt(idHora)+(parseInt(cantidadHoras)-1);
    	if(ultimaHora > 18){
    		bootbox.alert("La hora máxima de clases es a las 9:10pm, la cantidad seleccionada excede la misma",function(){
    			$("#Horarios_cantidad_horas").val(1);
    			calcularHoraFin();
    		});
    		return false;
    	}
    	idHora=$("#Horarios_hora_fin").val(horas[ultimaHora-1].end);
    };


    mostrarEvento=function(evento){
    	$("#calendar").fullCalendar('renderEvent',{
           title: evento.titulo,
           start: evento.start, 
           end: evento.end,
           id_periodo: evento.id_periodo,
           id_seccion: evento.id_seccion,
           id_dia: evento.id_dia,
           id_hora:evento.id_hora,
           id_asignatura: evento.id_asignatura,
           cantidad_horas: evento.cantidad_horas,
           id_docente: evento.id_docente,
           id_aula: evento.id_aula,
           backgroundColor: evento.background_color
       	},true);
    };


    registrarHoras=function(){
    	$.ajax({
			async:true,
			type: "POST",
			dataType:'json',
			url:"<?php echo $this->createUrl('horarios/registrarHoras')?>",
			data:$("#horas-form").serialize(),
			success:function(respuesta){
				if(!respuesta){
					bootbox.alert("Error Registrando Horas. Por favor intente nuevamente");
				}else{
					mostrarEvento(respuesta);
					bootbox.hideAll();
				}
			},
			error:function(){
				bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
					location.reload();
				});
			}
		});
    };


    modificarHoras=function(){
    	$.ajax({
			async:true,
			type: "POST",
			dataType:'html',
			url:"<?php echo $this->createUrl('horarios/modificarHoras')?>",
			data:$("#horas-modificar-form").serialize(),
			success:function(respuesta){
				if(respuesta){
					bootbox.alert("Error Modificando Horas. Por favor intente nuevamente");
				}else{
					bootbox.alert("Horas modificadas con éxito",function(){
						location.reload();
					});
				}
			},
			error:function(){
				bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
					location.reload();
				});
			}
		});
    };


    $(document).on("keyup","#Horarios_cantidad_horas",function(){
		valor=$("#Horarios_cantidad_horas").val();
		if(valor=='' || valor == "0"){
			$("#Horarios_hora_fin").val('');
			$("#Horarios_cantidad_horas").val('');
		}else{
			calcularHoraFin();
		}
	});


	$(document).on("change","#Horarios_id_hora",function(){
		horas=<?php echo $horas ?>;
		$("#Horarios_cantidad_horas").val(1);
		valor=$("#Horarios_id_hora").val();
		indice=valor-1;
		horaFin=new Date('2018-01-01 '+horas[indice].end);
		horaInicio=new Date('2018-01-01 '+horas[indice].start);
		$("#Horarios_hora_inicio").val(horaInicio.format('HH:MM:ss'));
		$("#Horarios_hora_fin").val(horaFin.format('h:MM tt'));
	});


	$(document).on("submit","#horas-form",function(){
		$.ajax({
			async:true,
			type: "POST",
			url:"<?php echo $this->createUrl('horarios/validarRegistrarHorario')?>",
			data:$("#horas-form").serialize(),
			success:function(respuesta){
				if(respuesta){
					bootbox.alert(respuesta);
				}else{
					registrarHoras();
				}
			},
			error:function(){
				bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
					location.reload();
				});
			}
		});

		return false;
	});


	$(document).on("submit","#horas-modificar-form",function(){
		$.ajax({
			async:true,
			type: "POST",
			url:"<?php echo $this->createUrl('horarios/validarModificarHorario')?>",
			data:$("#horas-modificar-form").serialize(),
			success:function(respuesta){
				if(respuesta){
					bootbox.alert(respuesta);
				}else{
					modificarHoras();
				}
			},
			error:function(){
				bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
					location.reload();
				});
			}
		});

		return false;
	});


	$(document).on("click",".eliminar-horas",function(){
		bootbox.confirm("¿Seguro que desea eliminar estas horas?",function(respuesta){
			if(respuesta){
				$.ajax({
					async:true,
					type: "POST",
					url:"<?php echo $this->createUrl('horarios/eliminarHoras')?>",
					data:$("#horas-modificar-form").serialize(),
					success:function(respuesta){
						if(!respuesta){
							bootbox.alert("Horas Eliminadas con éxito",function(){
								location.reload();
							});
						}
					},
					error:function(){
						bootbox.alert("Ha ocurrido un error, la página será recargada automáticamente. Por favor intente nuevamente",function(){
							location.reload();
						});
					}
				});
			}
		});
	});

	//$.noConflict($);
	$(".chosen-select").chosen();
});
</script>

<style type="text/css">
	.fc-left{ /*Para ocultar fecha actual*/
		display: none !important;
	}

	.fc-center{ /*Para ocultar fecha actual*/
		display: none !important;
	}

	.fc-right{ /*Para ocultar navegación de los días*/
		display: none !important;
	}
</style>

<?php
$this->breadcrumbs=array(
	'Búsqueda de Horarios'=>array('busqueda'),
	'Actualizar Horarios'
);
?>

<h1>Actualizar Horarios</h1>

<table class="table table-bordered">
	<tr class="active">
		<th>Año</th>
		<th>Período</th>
		<th>Fecha Inicio</th>
		<th>Fecha Fin</th>
		<th>Estatus</th>
	</tr>

	<tr>
		<td><?php echo $modelPeriodo->anio; ?></td>
		<td><?php echo $modelPeriodo->getPeriodoRomano(); ?></td>
		<td><?php echo date("d/m/Y",strtotime($modelPeriodo->fecha_inicio)); ?></td>
		<td><?php echo date("d/m/Y",strtotime($modelPeriodo->fecha_fin)); ?></td>
		<td><?php echo  $modelPeriodo->getStatusBandeja();?></td>
	</tr>
</table>
<br>
<table class="table table-bordered">
	<tr class="active">
		<th>Departamento</th>
		<th>Trayecto</th>
		<th>Trimestre</th>
		<th>Sección</th>
		<th>Turno</th>
	</tr>

	<tr>
		<td><?php echo $modelPeriodoSeccion->idSeccion->idDepartamento->departamento; ?></td>
		<td><?php echo $modelPeriodoSeccion->idSeccion->trayecto; ?></td>
		<td><?php echo $modelPeriodoSeccion->trimestre; ?></td>
		<td><?php echo $modelPeriodoSeccion->idSeccion->seccion; ?></td>
		<td><?php echo $modelPeriodoSeccion->idSeccion->idTurno->turno; ?></td>
	</tr>
</table>

<br>
<?php
echo CHtml::link(
	'Regresar',
	array('busqueda'),
	array('class'=>'btn btn-danger'));
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'horarios-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->hiddenField($model,'id_periodo',array('value'=>$modelPeriodo->id_periodo)); ?>
	<?php echo $form->hiddenField($model,'id_seccion',array('value'=>$modelPeriodoSeccion->id_seccion)); ?>
	<?php echo $form->hiddenField($model,'id_departamento',array('value'=>$modelPeriodoSeccion->idSeccion->idDepartamento->id_departamento)); ?>
	<?php echo $form->hiddenField($model,'trayecto',array('value'=>$modelPeriodoSeccion->idSeccion->trayecto)); ?>
	<?php echo $form->hiddenField($model,'trimestre',array('value'=>$modelPeriodoSeccion->trimestre)); ?>
	<?php echo $form->hiddenField($model,'id_malla',array('value'=>$modelPeriodoSeccion->id_malla)); ?>

<?php $this->endWidget(); ?>

<div id='calendar'></div>