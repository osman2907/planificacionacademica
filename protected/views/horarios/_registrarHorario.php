<script type="text/javascript">
	$(document).ready(function(){
		$(".chosen-select").chosen();
		$(".chosen-container-single").css('width','100%');
	});
</script>

<table class="table table-bordered">
	<tr class="active">
		<th>Año</th>
		<th>Período</th>
		<th>Departamento</th>
		<th>Trayecto</th>
		<th>Trimestre</th>
		<th>Sección</th>
	</tr>

	<tr>
		<td><?php echo $modelPeriodoSeccion->idPeriodo->anio; ?></td>
		<td><?php echo $modelPeriodoSeccion->idPeriodo->getPeriodoRomano(); ?></td>
		<td><?php echo $modelPeriodoSeccion->idSeccion->idDepartamento->departamento; ?></td>
		<td><?php echo $modelPeriodoSeccion->idSeccion->trayecto; ?></td>
		<td><?php echo $modelPeriodoSeccion->trimestre; ?></td>
		<td><?php echo $modelPeriodoSeccion->idSeccion->seccion; ?></td>
	</tr>
</table>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'horas-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<?php echo $form->hiddenField($model,'id_periodo',array('id'=>'id-periodo')); ?>
	<?php echo $form->hiddenField($model,'id_seccion',array('id'=>'id-seccion')); ?>
	<br>
	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>
	
	<div class="row">
		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'id_dia'); ?>
			<?php echo $form->hiddenField($model,'id_dia',array('class'=>'form-control')); ?>
			<?php echo $form->textField($model,'dia',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'id_dia'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'id_hora',array('label'=>'Hora Inicio')); ?>
			<?php echo $form->dropDownList($model,'id_hora',$listHorasInicio,array('class'=>'form-control')); ?>
			<?php echo $form->hiddenField($model,'hora_inicio'); ?>
			<?php echo $form->error($model,'id_hora'); ?>
		</div>
		
		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'cantidad_horas'); ?>
			<?php echo $form->textField($model,'cantidad_horas',array('class'=>'solo-numero form-control')); ?>
			<?php echo $form->error($model,'cantidad_horas'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'hora_fin',array('label'=>'Hora Fin')); ?>
			<?php echo $form->textField($model,'hora_fin',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'hora_fin'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6">
			<?php echo $form->labelEx($model,'id_asignatura'); ?>
			<?php echo $form->dropDownList($model,'id_asignatura',$listAsignaturas,array('class'=>'chosen-select form-control','empty'=>'SELECCIONE')); ?>
			<?php echo $form->error($model,'id_asignatura'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'id_docente'); ?>
			<?php echo $form->dropDownList($model,'id_docente',$listDocentes,array('class'=>'chosen-select form-control','empty'=>'SELECCIONE')); ?>
			<?php echo $form->error($model,'id_docente'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'id_aula'); ?>
			<?php echo $form->dropDownList($model,'id_aula',$listAulas,array('class'=>'chosen-select form-control','empty'=>'SELECCIONE')); ?>
			<?php echo $form->error($model,'id_aula'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-success')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

<?php
/*echo "<pre>";
print_r($listHorasInicio);
echo "</pre>";*/
?>
</div>
