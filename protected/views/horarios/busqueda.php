<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("change","#Horarios_id_departamento",function(){
			idDepartamento=$(this).val();
			if(idDepartamento == ''){
				$("#Horarios_trimestre").empty();
				$("#Horarios_id_seccion").empty();
			}else{
				idPeriodo=$("#Horarios_id_periodo").val();
				$.ajax({
					async:true,
					type: "POST",
					url:"<?php echo $this->createUrl('horarios/listarTrimestresDepartamentos')?>",
					data:{id_periodo:idPeriodo,id_departamento:idDepartamento},
					success: function(respuesta){
						$("#Horarios_trimestre").html(respuesta);
					}
				});
			}
		});


		$(document).on("change","#Horarios_trimestre",function(){
			idDepartamento=$("#Horarios_id_departamento").val();
			trimestre=$("#Horarios_trimestre").val();
			if(trimestre == ''){
				$("#Horarios_id_seccion").empty();
			}else{
				idPeriodo=$("#Horarios_id_periodo").val();
				$.ajax({
					async:true,
					type: "POST",
					url:"<?php echo $this->createUrl('horarios/listarSeccionesDepartamentos')?>",
					data:{id_periodo:idPeriodo,id_departamento:idDepartamento,trimestre:trimestre},
					success: function(respuesta){
						$("#Horarios_id_seccion").html(respuesta);
					}
				});
			}
		});
	});
</script>

<?php
$this->breadcrumbs=array(
	'Búsqueda de Horarios'
);
?>

<h1>Búsqueda de Horarios</h1>

<?php
if(!$modelPeriodo){
	?>
	<div class="alert alert-warning">
		En estos momentos no es posible actualizar horarios, es necesario que haya sólo <b>1</b> período académico activo.
	</div>
	<?php
}else{
	?>
	<br>
	<table class="table table-bordered">
		<tr class="active">
			<th>Año</th>
			<th>Período</th>
			<th>Fecha Inicio</th>
			<th>Fecha Fin</th>
		</tr>

		<tr>
			<td><?php echo $modelPeriodo->anio; ?></td>
			<td><?php echo $modelPeriodo->getPeriodoRomano(); ?></td>
			<td><?php echo date("d/m/Y",strtotime($modelPeriodo->fecha_inicio)); ?></td>
			<td><?php echo date("d/m/Y",strtotime($modelPeriodo->fecha_fin)); ?></td>
		</tr>

		<tr class="active">
			<th>Fecha Inicio Planif.</th>
			<th>Fecha Fin Planif.</th>
			<th colspan="2">Estatus</th>
		</tr>

		<tr>
			<td><?php echo date("d/m/Y",strtotime($modelPeriodo->fecha_inicio_plan)); ?></td>
			<td><?php echo date("d/m/Y",strtotime($modelPeriodo->fecha_fin_plan)); ?></td>
			<td colspan="2"><?php echo  $modelPeriodo->getStatusBandeja();?></td>
		</tr>
	</table>

	<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'horarios-form',
	'enableAjaxValidation'=>false,
)); ?>

	<br>
	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'id_periodo',array('value'=>$modelPeriodo->id_periodo)); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->dropDownList($model,'id_departamento',$listDepartamentos,array('empty'=>'SELECCIONE','class'=>'form-control')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'trimestre'); ?>
			<?php echo $form->dropDownList($model,'trimestre',$listTrimestres,array('empty'=>'SELECCIONE','class'=>'form-control')); ?>
			<?php echo $form->error($model,'trimestre'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_seccion'); ?>
			<?php echo $form->dropDownList($model,'id_seccion',$listSecciones,array('empty'=>'SELECCIONE','class'=>'form-control')); ?>
			<?php echo $form->error($model,'id_seccion'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-success')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php } ?>