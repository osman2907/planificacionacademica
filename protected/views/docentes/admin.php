<?php

$this->breadcrumbs=array(
	'Gestionar Docentes',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#docentes-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
var decorarGrilla=function(){
	$('.grid-view .items thead tr:first-child th:first-child').addClass('brsi');
    $('.grid-view .items thead tr:first-child th:last-child').addClass('brsd');
    $('.grid-view .items thead .filters td input').addClass('form-control');
    $('.grid-view .items thead .filters td select').addClass('form-control');
}
decorarGrilla();
");
?>

<h1>Gestionar Docentes</h1>

<?php echo CHtml::link(
			'Registrar Docentes',
			array('create'),
			array('class'=>'btn btn-success')
);
$template="{view}{update}";
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'docentes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'decorarGrilla',
	'columns'=>array(
		'cedula',
		array(
			'name'=>'nombre1',
			'header'=>'Nombres',
			'value'=>'strtoupper($data->nombres)'
		),
		array(
			'name'=>'apellido1',
			'header'=>'Apellidos',
			'value'=>'strtoupper($data->apellidos)'
		),
		array(
			'name'=>'id_sexo',
			'value'=>'strtoupper($data->sexo)',
			'filter'=>$listSexos
		),
		array(
			'name'=>'id_departamento',
			'value'=>'strtoupper($data->idDepartamento->departamento)',
			'filter'=>$listDepartamentos
		),
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>'$data->getStatusBandeja()',
			'filter'=>$listStatus
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>$template,
			'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),
	),
)); ?>
