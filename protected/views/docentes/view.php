<?php
$this->breadcrumbs=array(
	'Gestionar Docentes'=>array('admin'),
	'Consultar Docentes',
);
?>

<h1>Consultar Docentes</h1>

<?php echo CHtml::link(
	'Modificar Docente',
	array('docentes/update/'.$model->id_docente),
	array('class'=>'btn btn-primary')
);
?>
<br><br>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cedula',
		array(
			'name'=>'nombres',
			'value'=>strtoupper($model->nombres)
		),
		array(
			'name'=>'apellidos',
			'value'=>strtoupper($model->apellidos)
		),
		array(
			'name'=>'sexo',
			'value'=>strtoupper($model->sexo)
		),
		array(
			'name'=>'id_departamento',
			'value'=>strtoupper($model->idDepartamento->departamento)
		),
		array(
			'name'=>'tipoContrato',
			'value'=>strtoupper($model->tipoContrato)
		),
		array(
			'name'=>'dedicacionDocente',
			'value'=>strtoupper($model->dedicacionDocente)
		),
		array(
			'name'=>'categoriaDocente',
			'value'=>strtoupper($model->categoriaDocente)
		),
		array(
			'name'=>'providencia',
			'type'=>'raw',
			'value'=>$model->providencia?'<span class="negrita letra-verde mayuscula">Con Providencia</span>':'<span class=" negrita letra-roja mayuscula">Sin Providencia</span>'
		),
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Docentes',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
