<?php
/* @var $this DocentesController */
/* @var $data Docentes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_docente')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_docente), array('view', 'id'=>$data->id_docente)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula')); ?>:</b>
	<?php echo CHtml::encode($data->cedula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre1')); ?>:</b>
	<?php echo CHtml::encode($data->nombre1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre2')); ?>:</b>
	<?php echo CHtml::encode($data->nombre2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido1')); ?>:</b>
	<?php echo CHtml::encode($data->apellido1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido2')); ?>:</b>
	<?php echo CHtml::encode($data->apellido2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sexo')); ?>:</b>
	<?php echo CHtml::encode($data->id_sexo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tipo_contrato')); ?>:</b>
	<?php echo CHtml::encode($data->id_tipo_contrato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_dedicacion_docente')); ?>:</b>
	<?php echo CHtml::encode($data->id_dedicacion_docente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_categoria_docente')); ?>:</b>
	<?php echo CHtml::encode($data->id_categoria_docente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_departamento')); ?>:</b>
	<?php echo CHtml::encode($data->id_departamento); ?>
	<br />

	*/ ?>

</div>