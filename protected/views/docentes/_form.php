<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'docentes-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'cedula'); ?>
			<?php echo $form->textField($model,'cedula',array('class'=>'form-control solo-numero','maxlength'=>200)); ?>
			<?php echo $form->error($model,'cedula'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'nombre1'); ?>
			<?php echo $form->textField($model,'nombre1',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'nombre1'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'nombre2'); ?>
			<?php echo $form->textField($model,'nombre2',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'nombre2'); ?>
		</div>
	</div>


	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'apellido1'); ?>
			<?php echo $form->textField($model,'apellido1',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'apellido1'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'apellido2'); ?>
			<?php echo $form->textField($model,'apellido2',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'apellido2'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->dropDownList($model,'id_departamento',$listDepartamentos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>
	</div>


	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_sexo'); ?>
			<?php echo $form->dropDownList($model,'id_sexo',$listSexos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_sexo'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_tipo_contrato'); ?>
			<?php echo $form->dropDownList($model,'id_tipo_contrato',$listTiposContratos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_tipo_contrato'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_dedicacion_docente'); ?>
			<?php echo $form->dropDownList($model,'id_dedicacion_docente',$listDedicaciones,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_dedicacion_docente'); ?>
		</div>
	</div>


	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_categoria_docente'); ?>
			<?php echo $form->dropDownList($model,'id_categoria_docente',$listCategorias,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_categoria_docente'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'providencia'); ?>
			<?php echo $form->dropDownList($model,'providencia',$listProvidencias,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'providencia'); ?>
		</div>

		<?php if(!$model->isNewRecord){ ?>
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkbox($model,'id_status'); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->