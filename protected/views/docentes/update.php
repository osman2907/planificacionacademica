<?php
$this->breadcrumbs=array(
	'Gestionar Docentes'=>array('admin'),
	'Modificar Docentes',
);
?>

<h1>Modificar Docentes</h1>

<?php $this->renderPartial('_form',compact('model','listDepartamentos','listSexos','listTiposContratos','listDedicaciones','listCategorias','listProvidencias')); ?>