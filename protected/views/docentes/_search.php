<?php
/* @var $this DocentesController */
/* @var $model Docentes */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_docente'); ?>
		<?php echo $form->textField($model,'id_docente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cedula'); ?>
		<?php echo $form->textField($model,'cedula'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre1'); ?>
		<?php echo $form->textField($model,'nombre1',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre2'); ?>
		<?php echo $form->textField($model,'nombre2',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellido1'); ?>
		<?php echo $form->textField($model,'apellido1',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellido2'); ?>
		<?php echo $form->textField($model,'apellido2',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_sexo'); ?>
		<?php echo $form->textField($model,'id_sexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_tipo_contrato'); ?>
		<?php echo $form->textField($model,'id_tipo_contrato'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_dedicacion_docente'); ?>
		<?php echo $form->textField($model,'id_dedicacion_docente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_categoria_docente'); ?>
		<?php echo $form->textField($model,'id_categoria_docente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_status'); ?>
		<?php echo $form->checkBox($model,'id_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_departamento'); ?>
		<?php echo $form->textField($model,'id_departamento'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->