<?php
$this->breadcrumbs=array(
	'Gestionar Docentes'=>array('admin'),
	'Registrar Docentes',
);
?>

<h1>Registrar Docentes</h1>

<?php $this->renderPartial('_form',compact('model','listDepartamentos','listSexos','listTiposContratos','listDedicaciones','listCategorias','listProvidencias')); ?>