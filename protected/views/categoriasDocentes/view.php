<?php
$this->breadcrumbs=array(
	'Gestionar Categorías Docentes'=>array('admin'),
	'Consultar Categorías Docentes',
);
?>

<h1>Consultar Categorías Docentes</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_categoria_docente',
		'categoria_docente',
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Categorías Docentes',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
