<?php
/* @var $this CategoriasDocentesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Categorias Docentes',
);

$this->menu=array(
	array('label'=>'Create CategoriasDocentes', 'url'=>array('create')),
	array('label'=>'Manage CategoriasDocentes', 'url'=>array('admin')),
);
?>

<h1>Categorias Docentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
