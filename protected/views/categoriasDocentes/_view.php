<?php
/* @var $this CategoriasDocentesController */
/* @var $data CategoriasDocentes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_categoria_docente')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_categoria_docente), array('view', 'id'=>$data->id_categoria_docente)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoria_docente')); ?>:</b>
	<?php echo CHtml::encode($data->categoria_docente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />


</div>