<?php
$this->breadcrumbs=array(
	'Gestionar Categorías Docentes'=>array('admin'),
	'Modificar Categorías Docentes',
);
?>

<h1>Modificar Categorías Docentes</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>