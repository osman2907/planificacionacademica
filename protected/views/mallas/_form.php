<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mallas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'nombre_malla'); ?>
			<?php echo $form->textField($model,'nombre_malla',array('class'=>'form-control','maxlength'=>200)); ?>
			<?php echo $form->error($model,'nombre_malla'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'fecha_creacion'); ?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'model' => $model,
			    'language' => 'es',
			    'attribute' => 'fecha_creacion',
			    'options'=>array(
			    	'changeMonth'=>true,
			    	'changeYear'=>true,
			    	'yearRange'=>"2005:".date("Y")
			    ),
			    'htmlOptions' => array(
			        'class'=>'form-control',
			        'readonly'=>'readonly'
			    ),
			));
			?>
			<?php echo $form->error($model,'fecha_creacion'); ?>
		</div>

		<div class="col-xs-4">
			<?php echo $form->labelEx($model,'id_departamento'); ?>
			<?php echo $form->dropDownList($model,'id_departamento',$listDepartamentos,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'id_departamento'); ?>
		</div>
	</div>

	<div class="row">
		<?php if(!$model->isNewRecord){ ?>
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkbox($model,'id_status'); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->