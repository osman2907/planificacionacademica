<?php
$this->breadcrumbs=array(
	'Gestionar Mallas'=>array('admin'),
	'Asignaturas por Mallas'=>array('mallas/asignaturas/'.$model->id_malla),
	'Registrar Asignaturas'
);
?>

<h1>Registrar Asignaturas</h1>

<?php
echo CHtml::link(
	'Asignaturas por Mallas',
	array('mallas/asignaturas/'.$model->id_malla),
	array('class'=>'btn btn-danger'));
?>
<br><br>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'asignaturas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<table class="table table-bordered">
		<tr class="active">
			<th>Nombre Malla</th>
			<th>Fecha Creación</th>
			<th>Departamento</th>
			<th>Estatus</th>
		</tr>

		<tr>
			<td><?php echo $model->idMalla->nombre_malla; ?></td>
			<td><?php echo date("d/m/Y",strtotime($model->idMalla->fecha_creacion)); ?></td>
			<td><?php echo strtoupper($model->idMalla->idDepartamento->departamento); ?></td>
			<td><?php echo $model->idMalla->getStatusBandeja(); ?></td>
		</tr>
	</table>
	<br>
	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'codigo'); ?>
			<?php echo $form->textField($model,'codigo',array('class'=>'form-control mayuscula','maxlength'=>20)); ?>
			<?php echo $form->error($model,'codigo'); ?>
		</div>

		<div class="col-xs-9">
			<?php echo $form->labelEx($model,'asignatura'); ?>
			<?php echo $form->textField($model,'asignatura',array('class'=>'form-control mayuscula','maxlength'=>200)); ?>
			<?php echo $form->error($model,'asignatura'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'trayecto'); ?>
			<?php echo $form->textField($model,'trayecto',array('class'=>'form-control solo-numero','maxlength'=>2)); ?>
			<?php echo $form->error($model,'trayecto'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'trimestre'); ?>
			<?php echo $form->textField($model,'trimestre',array('class'=>'form-control solo-numero','maxlength'=>2)); ?>
			<?php echo $form->error($model,'trimestre'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'trimestre_ejecucion'); ?>
			<?php echo $form->textField($model,'trimestre_ejecucion',array('class'=>'form-control solo-numero','maxlength'=>2)); ?>
			<?php echo $form->error($model,'trimestre_ejecucion'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'unidades_credito'); ?>
			<?php echo $form->textField($model,'unidades_credito',array('class'=>'form-control solo-numero','maxlength'=>2)); ?>
			<?php echo $form->error($model,'unidades_credito'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'horas_teoricas'); ?>
			<?php echo $form->textField($model,'horas_teoricas',array('class'=>'form-control solo-numero','maxlength'=>2)); ?>
			<?php echo $form->error($model,'horas_teoricas'); ?>
		</div>

		<div class="col-xs-3">
			<?php echo $form->labelEx($model,'horas_practicas'); ?>
			<?php echo $form->textField($model,'horas_practicas',array('class'=>'form-control solo-numero','maxlength'=>2)); ?>
			<?php echo $form->error($model,'horas_practicas'); ?>
		</div>

		<?php if(!$model->isNewRecord){ ?>
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'id_status'); ?>
				<?php echo $form->checkbox($model,'id_status'); ?> Activo
				<?php echo $form->error($model,'id_status'); ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

