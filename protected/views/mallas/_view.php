<?php
/* @var $this MallasController */
/* @var $data Mallas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_malla')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_malla), array('view', 'id'=>$data->id_malla)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_malla')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_malla); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_creacion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_creacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_departamento')); ?>:</b>
	<?php echo CHtml::encode($data->id_departamento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />


</div>