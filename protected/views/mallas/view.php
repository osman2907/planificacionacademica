<?php
$this->breadcrumbs=array(
	'Gestionar Mallas'=>array('admin'),
	'Consultar Mallas',
);
?>

<h1>Consultar Mallas</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_malla',
		'nombre_malla',
		array(
			'name'=>'fecha_creacion',
			'value'=>date("d/m/Y",strtotime($model->fecha_creacion))
		),
		array(
			'name'=>'id_departamento',
			'value'=>$model->idDepartamento->departamento
		),
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>$model->getStatusBandeja()
		)
	),
)); ?>

<br>

<?php
if(count($modelAsignaturas) > 0){
	$trimestre=-1;
	$asignaturas=$modelAsignaturas;
	foreach ($asignaturas as $asignatura){
		if($trimestre != $asignatura->trimestre){
			if($trimestre != -1){
				?>
				</table>
				<br>
				<?php
			}

			$trimestre=$asignatura->trimestre;
			?>
			<table class="table table-bordered">
				<tr class="active">
					<th class="centro" colspan="6">Trimestre <?php echo $asignatura->trimestre;?></th>
				</tr>

				<tr class="active">
					<th>Código</th>
					<th width="60%">Nombre Asignatura</th>
					<th>HT</th>
					<th>HP</th>
					<th>HS</th>
					<th>UC</th>
				</tr>
			<?php 
		}
		?>
		<tr>
			<td><?php echo $asignatura->codigo; ?></td>
			<td><?php echo $asignatura->asignatura; ?></td>
			<td><?php echo $asignatura->horas_teoricas; ?></td>
			<td><?php echo $asignatura->horas_practicas; ?></td>
			<td><?php echo $asignatura->horas_teoricas+$asignatura->horas_practicas; ?></td>
			<td><?php echo $asignatura->unidades_credito; ?></td>
		</tr>
		<?php
	}
	echo "</table>";
}
?>

<br>
<div class="centro">
	<?php
	echo CHtml::link(
		'Gestionar Mallas',
		array('admin'),
		array('class'=>'btn btn-danger'));
	?>
</div>
