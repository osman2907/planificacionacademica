<?php

$this->breadcrumbs=array(
	'Gestionar Mallas Curriculares',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mallas-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
var decorarGrilla=function(){
	$('.grid-view .items thead tr:first-child th:first-child').addClass('brsi');
    $('.grid-view .items thead tr:first-child th:last-child').addClass('brsd');
    $('.grid-view .items thead .filters td input').addClass('form-control');
    $('.grid-view .items thead .filters td select').addClass('form-control');
}
decorarGrilla();
");
?>

<h1>Gestionar Mallas Curriculares</h1>

<?php echo CHtml::link(
			'Registrar Mallas Curriculares',
			array('create'),
			array('class'=>'btn btn-success')
);
$template="{view}{update}";
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'turnos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'decorarGrilla',
	'columns'=>array(
		'nombre_malla',
		array(
			'name'=>'fecha_creacion',
			'value'=>'date("d/m/Y",strtotime($data->fecha_creacion))',
            'filter'=>false
		),
		array(
			'name'=>'id_departamento',
			'value'=>'$data->idDepartamento->departamento',
            'filter'=>$listDepartamentos
		),
		array(
			'name'=>'id_status',
			'type'=>'raw',
			'value'=>'$data->getStatusBandeja()',
            'filter'=>$listStatus
		),
		array(
            'header'=>'Acciones',
            "type"=>'raw',
            'value'=>function($data){
                $ruta=Yii::app()->request->baseUrl."/index.php";
                $idMalla=$data->id_malla;
                
                $html="
                    <div class='dropdown'>
                        <button class='btn btn-default dropdown-toggle type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                            <i class='glyphicon glyphicon-cog'></i>
                            <span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu' aria-labelledby='dropdownMenu1' style='text-align:left'>
                            <li>
                                <a href='$ruta/mallas/view/$idMalla'>
                                    <i class='glyphicon glyphicon-eye-open'></i> Ver
                                </a>
                            </li>

                            <li>
                                <a href='$ruta/mallas/update/$idMalla'>
                                    <i class='glyphicon glyphicon-pencil'></i> Modificar
                                </a>
                            </li>

                            <li>
                                <a href='$ruta/mallas/asignaturas/$idMalla'>
                                    <i class='glyphicon glyphicon-book'></i> Asignaturas
                                </a>
                            </li>
                        </ul>
                    </div>
                ";
                return $html;
            },
            'htmlOptions'=>array('style'=>'text-align:center; width:80px;'),
        ),
	),
)); ?>
