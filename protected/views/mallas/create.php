<?php
$this->breadcrumbs=array(
	'Gestionar Mallas Curriculares'=>array('admin'),
	'Registrar Mallas Curriculares',
);
?>

<h1>Registrar Mallas Curriculares</h1>

<?php $this->renderPartial('_form', compact('model','listDepartamentos')); ?>