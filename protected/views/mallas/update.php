<?php
$this->breadcrumbs=array(
	'Gestionar Mallas Curriculares'=>array('admin'),
	'Modificar Mallas Curriculares',
);
?>

<h1>Modificar Mallas Curriculares</h1>

<?php $this->renderPartial('_form', compact('model','listDepartamentos')); ?>