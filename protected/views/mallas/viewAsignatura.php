<?php
$this->breadcrumbs=array(
	'Gestionar Mallas Curriculares'=>array('admin'),
	'Asignaturas por Mallas'=>array('mallas/asignaturas/'.$model->id_malla),
	'Asignaturas por Mallas',
);
?>

<h1>Consultar Asignaturas</h1>

<?php
echo CHtml::link(
	'Asignaturas por Mallas',
	array('mallas/asignaturas/'.$model->id_malla),
	array('class'=>'btn btn-danger'));
echo "&nbsp;";
echo CHtml::link(
	'Modificar Asignatura',
	array('mallas/updateAsignaturas/'.$model->id_asignatura),
	array('class'=>'btn btn-primary'));
echo "&nbsp;";
echo CHtml::link(
	'Registrar Asignaturas',
	array('mallas/registrarAsignaturas/'.$model->id_malla),
	array('class'=>'btn btn-success'));
?>
<br><br>
<table class="table table-bordered">
	<tr class="active">
		<th>Nombre Malla</th>
		<th>Fecha Creación</th>
		<th>Departamento</th>
		<th>Estatus</th>
	</tr>

	<tr>
		<td><?php echo $model->idMalla->nombre_malla; ?></td>
		<td><?php echo date("d/m/Y",strtotime($model->idMalla->fecha_creacion)); ?></td>
		<td><?php echo strtoupper($model->idMalla->idDepartamento->departamento); ?></td>
		<td><?php echo $model->idMalla->getStatusBandeja(); ?></td>
	</tr>
</table>
<br>
<table class="table table-bordered">
	<tr class="active">
		<th colspan="4" class="centro">Datos de la Asignatura</th>
	</tr>

	<tr class="active">
		<th>Código</th>
		<th colspan="3">Nombre Asignatura</th>
	</tr>

	<tr>
		<td><?php echo $model->codigo ?></td>
		<td colspan="3"><?php echo $model->asignatura ?></td>
	</tr>

	<tr class="active">
		<th>Trayecto</th>
		<th>Trimestre de la Carrera</th>
		<th>Trimestre del Año</th>
		<th>Unidades de Crédito</th>
	</tr>

	<tr>
		<td><?php echo $model->trayecto ?></td>
		<td><?php echo $model->trimestre ?></td>
		<td><?php echo $model->trimestre_ejecucion ?></td>
		<td><?php echo $model->unidades_credito ?></td>
	</tr>

	<tr class="active">
		<th>Horas Teóricas</th>
		<th>Horas Prácticas</th>
		<th>Horas Semanales</th>
		<th>Estatus</th>
	</tr>

	<tr>
		<td><?php echo $model->horas_teoricas ?></td>
		<td><?php echo $model->horas_practicas ?></td>
		<td><?php echo $model->horas_teoricas+$model->horas_practicas; ?></td>
		<td><?php echo $model->getStatusBandeja() ?></td>
	</tr>
</table>

