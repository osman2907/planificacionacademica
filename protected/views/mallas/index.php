<?php
/* @var $this MallasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mallases',
);

$this->menu=array(
	array('label'=>'Create Mallas', 'url'=>array('create')),
	array('label'=>'Manage Mallas', 'url'=>array('admin')),
);
?>

<h1>Mallases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
