<style type="text/css">
a, a:hover{
	color: #000000;
}	
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click",".eliminar",function(){
			elemento=$(this);
			bootbox.confirm("¿Seguro que desea eliminar esta asignatura?",function(resp){
				if(resp){
					document.location=elemento.attr('href');
				}
			})
			return false;
		});
	});
</script>

<?php
$this->breadcrumbs=array(
	'Gestionar Mallas Curriculares'=>array('admin'),
	'Asignaturas por Mallas',
);
?>

<h1>Asignaturas por Mallas</h1>

<?php
echo CHtml::link(
	'Gestionar Mallas Curriculares',
	array('admin'),
	array('class'=>'btn btn-danger'));
echo "&nbsp;";
echo CHtml::link(
	'Registrar Asignaturas',
	array('mallas/registrarAsignaturas/'.$model->id_malla),
	array('class'=>'btn btn-success'));
?>
<br><br>
<table class="table table-bordered">
	<tr class="active">
		<th>Nombre Malla</th>
		<th>Fecha Creación</th>
		<th>Departamento</th>
		<th>Estatus</th>
	</tr>

	<tr>
		<td><?php echo $model->nombre_malla; ?></td>
		<td><?php echo date("d/m/Y",strtotime($model->fecha_creacion)); ?></td>
		<td><?php echo strtoupper($model->idDepartamento->departamento); ?></td>
		<td><?php echo $model->getStatusBandeja(); ?></td>
	</tr>
</table>
<br>

<?php
if(count($modelAsignaturas) > 0){
	$trimestre=-1;
	$asignaturas=$modelAsignaturas;
	foreach ($asignaturas as $asignatura){
		if($trimestre != $asignatura->trimestre){
			if($trimestre != -1){
				?>
				</table>
				<br>
				<?php
			}

			$trimestre=$asignatura->trimestre;
			?>
			<table class="table table-bordered">
				<tr class="active">
					<th class="centro" colspan="7">Trimestre <?php echo $asignatura->trimestre;?></th>
				</tr>

				<tr class="active">
					<th>Código</th>
					<th width="60%">Nombre Asignatura</th>
					<th>HT</th>
					<th>HP</th>
					<th>HS</th>
					<th>UC</th>
					<th>Acciones</th>
				</tr>
			<?php 
		}
		?>
		<tr>
			<td><?php echo $asignatura->codigo; ?></td>
			<td><?php echo $asignatura->asignatura; ?></td>
			<td><?php echo $asignatura->horas_teoricas; ?></td>
			<td><?php echo $asignatura->horas_practicas; ?></td>
			<td><?php echo $asignatura->horas_teoricas+$asignatura->horas_practicas; ?></td>
			<td><?php echo $asignatura->unidades_credito; ?></td>
			<td>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/mallas/viewAsignatura/<?php echo $asignatura->id_asignatura; ?>"><span class="glyphicon glyphicon-search"></span></a>
				&nbsp;
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/mallas/updateAsignaturas/<?php echo $asignatura->id_asignatura; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
				&nbsp;
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/mallas/deleteAsignaturas/<?php echo $asignatura->id_asignatura; ?>" class="eliminar"><span class="glyphicon glyphicon-remove"></span></a>
			</td>
		</tr>
		<?php
	}
	echo "</table>";
}
?>
