<style type="text/css">
	div.form .form-group {
		border: 0px !important;
		box-shadow: none !important;
		padding: 0px !important;
	}
</style>

<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Inicio de Sesión')
    )
);
?>

<h1><?php //echo CrugeTranslator::t('logon',"Login"); ?></h1>
<?php if(Yii::app()->user->hasFlash('loginflash')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('loginflash'); ?>
</div>
<?php else: ?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'logon-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<div>

		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-warning">
					Para ingresar al sistema se debe utilizar el usuario y contraseña asignados en el Sistema Integrado de Admisión y Control de Estudios del IUTOMS (SIACE).<br>

					Si tiene algún inconveniente con respecto al sistema puede plantear su problemática a través de la dirección de correo electrónico <b>soporte.sigepsi@gmail.com</b>
				</div>
			</div>
		</div>

		<div class="panel panel-info" style='width:400px; margin: 0 auto;'>
	        <div class="panel-heading">
	            <div class="panel-title">Iniciar Sesión</div>
	            <!--<div style="float:right; font-size: 80%; position: relative; top:-10px">
	            	<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/recuperar">¿Olvidó su contraseña?</a>
	            </div>-->
	        </div>     

	        <div style="padding-top:30px" class="panel-body">

				<div class="form-group has-feedback has-feedback-left">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
			        <i class="form-control-feedback glyphicon glyphicon-user"></i>
			        <?php echo $form->error($model,'username'); ?>
			    </div>

			    <div class="form-group has-feedback has-feedback-left">
					<?php echo $form->labelEx($model,'password'); ?>
					<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
			        <i class="form-control-feedback glyphicon glyphicon-lock"></i>
			        <?php echo $form->error($model,'password'); ?>
			    </div>

				<!--<div class="row rememberMe">
					<?php echo $form->checkBox($model,'rememberMe'); ?>
					<?php echo $form->label($model,'rememberMe'); ?>
					<?php echo $form->error($model,'rememberMe'); ?>
				</div>-->

				<div class="row buttons" style="padding-top:20px;">
					<?php echo CHtml::submitButton('Iniciar Sesión',array('class'=>'btn btn-success')); ?>
					<?php //Yii::app()->user->ui->tbutton(CrugeTranslator::t('logon', "Login")); ?>
					<?php //echo Yii::app()->user->ui->passwordRecoveryLink; ?>
					<?php
						//if(Yii::app()->user->um->getDefaultSystem()->getn('registrationonlogin')===1)
							//echo Yii::app()->user->ui->registrationLink;
					?>
				</div>

				<?php
					//	si el componente CrugeConnector existe lo usa:
					//
					if(Yii::app()->getComponent('crugeconnector') != null){
					if(Yii::app()->crugeconnector->hasEnabledClients){ 
				?>
				<div class='crugeconnector'>
					<span><?php echo CrugeTranslator::t('logon', 'You also can login with');?>:</span>
					<ul>
					<?php 
						$cc = Yii::app()->crugeconnector;
						foreach($cc->enabledClients as $key=>$config){
							$image = CHtml::image($cc->getClientDefaultImage($key));
							echo "<li>".CHtml::link($image,
								$cc->getClientLoginUrl($key))."</li>";
						}
					?>
					</ul>
				</div>
				<?php }} ?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div>
<?php endif; ?>
