<?php
class TiposContratos extends CActiveRecord{
	
	public function tableName(){
		return 'tipos_contratos';
	}

	
	public function rules(){
		return array(
			array('id_tipo_contrato, tipo_contrato', 'required'),
			array('id_tipo_contrato', 'numerical', 'integerOnly'=>true),
			array('tipo_contrato', 'length', 'max'=>200),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tipo_contrato, tipo_contrato, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_tipo_contrato' => 'Id',
			'tipo_contrato' => 'Tipo Contrato',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){

		$criteria=new CDbCriteria;

		$criteria->compare('id_tipo_contrato',$this->id_tipo_contrato);
		$criteria->compare('tipo_contrato',$this->tipo_contrato,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activo</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactivo</span>";
			break;
		}
	}


	public static function getTiposContratos($idStatus = null){
   		$options['order']="tipo_contrato ASC";
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_status=$status";
   		}
   		$tiposContratos=TiposContratos::model()->findAll($options);
		return CHtml::listData($tiposContratos,'id_tipo_contrato','tipo_contrato');
   	}

}
