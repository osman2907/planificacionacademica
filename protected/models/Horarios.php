<?php
class Horarios extends CActiveRecord{
	
	public $id_departamento;
	public $trimestre;
	public $id_malla;
	public $dia;
	public $hora_inicio;
	public $hora_fin;
	public $cantidad_horas;
	public $asignatura;
	public $id_aula_old;
	public $id_docente_old;
	public $id_sede;
	public $id_tipo_contrato;
	public $trayecto_inicial; //campo utilizado sólo para generar reportes.


	public function tableName(){
		return 'horarios';
	}

	
	public function rules(){
		return array(
			array('id_periodo, id_seccion, id_dia, id_hora, id_asignatura', 'required','on'=>'registrar'),
			array('id_departamento,trimestre,id_seccion', 'required','on'=>'busqueda'),
			array('id_periodo,id_seccion,id_dia,cantidad_horas,id_hora,hora_inicio,hora_fin,id_asignatura','required','on'=>'registrarHorario'),
			array('id_periodo,id_seccion,id_dia,cantidad_horas,id_hora,hora_inicio,hora_fin,id_asignatura','required','on'=>'modificarHorario'),
			array('id_periodo, id_seccion, id_dia, id_hora, id_asignatura, id_docente, id_aula', 'numerical', 'integerOnly'=>true),
			array('id_periodo, id_seccion, id_dia, id_hora, id_asignatura, id_docente, id_aula','safe'),
			array('id_periodo, id_seccion, id_dia, id_hora, id_asignatura, id_docente, id_aula', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idAsignatura' => array(self::BELONGS_TO, 'Asignaturas', 'id_asignatura'),
			'idDia' => array(self::BELONGS_TO, 'Dias', 'id_dia'),
			'idHora' => array(self::BELONGS_TO, 'Horas', 'id_hora'),
			'idPeriodo' => array(self::BELONGS_TO, 'PeriodosAcademicos', 'id_periodo'),
			'idSeccion' => array(self::BELONGS_TO, 'Secciones', 'id_seccion'),
			'idAula' => array(self::BELONGS_TO, 'Aulas', 'id_aula'),
			'idDocente' => array(self::BELONGS_TO, 'Docentes', 'id_docente')
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_periodo' => 'Período Académico',
			'id_seccion' => 'Sección',
			'id_dia' => 'Día',
			'id_hora' => 'Hora',
			'id_asignatura' => 'Asignatura',
			'id_docente' => 'Docente',
			'id_aula' => 'Aula',
			'id_departamento'=>'Departamento',
			'trimestre'=>'Trimestre',
			'id_sede'=>'Sede',
			'id_tipo_contrato'=>'Tipo Contrato'
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_periodo',$this->id_periodo);
		$criteria->compare('id_seccion',$this->id_seccion);
		$criteria->compare('id_dia',$this->id_dia);
		$criteria->compare('id_hora',$this->id_hora);
		$criteria->compare('id_asignatura',$this->id_asignatura);
		$criteria->compare('id_docente',$this->id_docente);
		$criteria->compare('id_aula',$this->id_aula);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getTrimestresDepartamentos($idPeriodo,$idDepartamento){
		$sql="
			SELECT distinct(trimestre) FROM periodos_academicos_secciones pas
				INNER JOIN secciones s ON pas.id_seccion=s.id_seccion
			WHERE s.id_departamento=$idDepartamento AND pas.id_periodo=$idPeriodo
			ORDER BY trimestre ASC";
		$trayectos= Yii::app()->db->createCommand($sql)->queryAll();
		return $trayectos;
	}


	public function getListTrimestresDepartamentos($idPeriodo,$idDepartamento){
		$trimestres=$this->getTrimestresDepartamentos($idPeriodo,$idDepartamento);
		return CHtml::listData($trimestres,'trimestre','trimestre');
	}


	public function getSeccionesDepartamentos($idPeriodo,$idDepartamento,$trimestre){
		$sql="
			SELECT s.id_seccion,seccion FROM periodos_academicos_secciones pas
				INNER JOIN secciones s ON pas.id_seccion=s.id_seccion
			WHERE s.id_departamento=$idDepartamento AND pas.id_periodo=$idPeriodo 
				AND trimestre=$trimestre
			ORDER BY seccion ASC";
		$trayectos= Yii::app()->db->createCommand($sql)->queryAll();
		return $trayectos;
	}


	public function getListSeccionesDepartamentos($idPeriodo,$idDepartamento,$trimestre){
		$secciones=$this->getSeccionesDepartamentos($idPeriodo,$idDepartamento,$trimestre);
		return CHtml::listData($secciones,'id_seccion','seccion');
	}


	public function registrarHoras($post){
		$transaction=$this->dbConnection->beginTransaction();
		try{
			$cantidadHoras=$post['Horarios']['cantidad_horas'];
			$idHoraInicio=$post['Horarios']['id_hora'];
			$idHoraFin=$idHoraInicio+($cantidadHoras-1);
			for ($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
				$model=new Horarios;
				$model->attributes=$post['Horarios'];
				$model->id_hora=$idHora;
				if(!$model->save()){
					throw new CHttpException(403,'Error registrando Horarios');
				}
			}
	        $transaction->commit();
	        return true;
		}
		catch(Exception $e){
		    $transaction->rollBack();
		    return false;
		}
	}


	public function modificarHoras($post){
		$transaction=$this->dbConnection->beginTransaction();
		try{
			$cantidadHoras=$post['Horarios']['cantidad_horas'];
			$idHoraInicio=$post['Horarios']['id_hora'];
			$idPeriodo=$post['Horarios']['id_periodo'];
			$idSeccion=$post['Horarios']['id_seccion'];
			$idDia=$post['Horarios']['id_dia'];
			$idAsignatura=$post['Horarios']['id_asignatura'];
			$idDocente=$post['Horarios']['id_docente_old'];
			$idAula=$post['Horarios']['id_aula_old'];
			$idHoraFin=$idHoraInicio+($cantidadHoras-1);

			for ($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
				$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion AND id_dia=$idDia 
					AND id_asignatura=$idAsignatura AND id_hora=$idHora";
				$options['condition'].=empty($idDocente)?" AND id_docente IS NULL":" AND id_docente=$idDocente";
				$options['condition'].=empty($idAula)?" AND id_aula IS NULL":" AND id_aula=$idAula";
				$model=Horarios::model()->find($options);
				$model->attributes=$post['Horarios'];
				$model->id_hora=$idHora;
				if(!$model->save()){
					throw new CHttpException(403,'Error registrando Horarios');
				}
			}
	        $transaction->commit();
	        return true;
		}
		catch(Exception $e){
		    $transaction->rollBack();
		    return false;
		}
	}


	public function eliminarHoras($post){
		$transaction=$this->dbConnection->beginTransaction();
		try{
			$cantidadHoras=$post['Horarios']['cantidad_horas'];
			$idHoraInicio=$post['Horarios']['id_hora'];
			$idPeriodo=$post['Horarios']['id_periodo'];
			$idSeccion=$post['Horarios']['id_seccion'];
			$idDia=$post['Horarios']['id_dia'];
			$idAsignatura=$post['Horarios']['id_asignatura'];
			$idDocente=$post['Horarios']['id_docente_old'];
			$idAula=$post['Horarios']['id_aula_old'];
			$idHoraFin=$idHoraInicio+($cantidadHoras-1);

			for ($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
				$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion AND id_dia=$idDia 
					AND id_asignatura=$idAsignatura AND id_hora=$idHora";
				$options['condition'].=empty($idDocente)?" AND id_docente IS NULL":" AND id_docente=$idDocente";
				$options['condition'].=empty($idAula)?" AND id_aula IS NULL":" AND id_aula=$idAula";
				$model=Horarios::model()->find($options);
				if(!$model->delete()){
					throw new CHttpException(403,'Error eliminando Horarios');
				}
			}
	        $transaction->commit();
	        return true;
		}
		catch(Exception $e){
		    $transaction->rollBack();
		    return false;
		}
	}


	public function getEventoCalendario($post){
		$modelAsignatura=Asignaturas::model()->find(array('condition'=>"id_asignatura=".$post['Horarios']['id_asignatura']));
		$evento=array(
			'id_periodo'=>$post['Horarios']['id_periodo'],
			'id_seccion'=>$post['Horarios']['id_seccion'],
			'id_dia'=>$post['Horarios']['id_dia'],
			'id_hora'=>$post['Horarios']['id_hora'],
			'id_asignatura'=>$post['Horarios']['id_asignatura'],
			'cantidad_horas'=>$post['Horarios']['cantidad_horas'],
			'id_docente'=>$post['Horarios']['id_docente'],
			'id_aula'=>$post['Horarios']['id_aula'],
			'start'=>'2018-10-0'.$post['Horarios']['id_dia'].' '.$post['Horarios']['hora_inicio'],
			'end'=>'2018-10-0'.$post['Horarios']['id_dia'].' '.date("H:i:s",strtotime($post['Horarios']['hora_fin'])),
		);
		
		$titulo=$modelAsignatura->codigo;
		if(!empty($post['Horarios']['id_docente'])){
			$modelDocente=Docentes::model()->find(array('condition'=>"id_docente=".$post['Horarios']['id_docente']));
			$titulo.=" - ".$modelDocente->nombreCompleto;
		}

		if(!empty($post['Horarios']['id_aula'])){
			$modelAula=Aulas::model()->find(array('condition'=>"id_aula=".$post['Horarios']['id_aula']));
			$titulo.=" - Aula: ".$modelAula->nombre_aula;
		}

		if(empty($post['Horarios']['id_docente']) || empty($post['Horarios']['id_aula'])){
			$evento['background_color']="#FE2E2E";
		}else{
			$evento['background_color']=false;
		}

		$titulo.=" - Horas: ".$post['Horarios']['cantidad_horas'];
		$evento['titulo']=$titulo;

		return $evento;
	}


	public function getConsultarEventos(){
		$sql='
			SELECT id_periodo,id_seccion,id_dia,hs.id_asignatura,count(id_periodo) AS "cantidad_horas",
				min(hs.id_hora) AS "id_hora",min(h.hora_inicio) AS "hora_inicio", max(h.hora_fin) AS "hora_fin", 
				id_docente, min(asignatura) AS "asignatura",min(a.codigo) AS "codigo", 
				min(hs.id_aula) AS "id_aula", min(nombre_aula) AS "nombre_aula"
			FROM horarios hs
				INNER JOIN horas h ON hs.id_hora=h.id_hora
				INNER JOIN asignaturas a ON hs.id_asignatura=a.id_asignatura
				LEFT JOIN aulas au ON hs.id_aula=au.id_aula
			WHERE id_periodo='.$this->id_periodo.' AND id_seccion='.$this->id_seccion.'
			GROUP BY id_periodo,id_seccion,id_dia,id_docente,hs.id_asignatura
			ORDER BY id_dia,hora_inicio';
		$eventos= Yii::app()->db->createCommand($sql)->queryAll();

		$i=0;
		$evento=array();
		foreach ($eventos as $even) {
			$evento[$i]=array(
				'id_periodo'=>$even['id_periodo'],
				'id_seccion'=>$even['id_seccion'],
				'id_dia'=>$even['id_dia'],
				'id_asignatura'=>$even['id_asignatura'],
				'cantidad_horas'=>$even['cantidad_horas'],
				'id_hora'=>$even['id_hora'],
				'start'=>'2018-10-0'.$even['id_dia'].' '.$even['hora_inicio'],
				'end'=>'2018-10-0'.$even['id_dia'].' '.$even['hora_fin'],
				'id_docente'=>$even['id_docente'],
				'id_aula'=>$even['id_aula']
			);
			
			$titulo=$even['codigo'];
			if(!empty($even['id_docente'])){
				$modelDocente=Docentes::model()->find(array('condition'=>"id_docente=".$even['id_docente']));
				$titulo.=" - ".$modelDocente->nombreCompleto;
			}

			if(!empty($even['id_aula'])){
				$modelAula=Aulas::model()->find(array('condition'=>"id_aula=".$even['id_aula']));
				$titulo.=" - Aula: ".$modelAula->nombre_aula;
			}

			$titulo.=" - Horas: ".$even['cantidad_horas'];
			$evento[$i]['title']=$titulo;

			if(empty($even['id_aula']) || empty($even['id_docente'])){
				$evento[$i]['backgroundColor']="#FE2E2E";	
			}

			$i++;
		}
		return $evento;
	}


	public function validarHorarios($post){
		$idPeriodo=$post['id_periodo'];
		$idSeccion=$post['id_seccion'];
		$idDia=$post['id_dia'];
		$idAsignatura=$post['id_asignatura'];
		$idDocente=$post['id_docente_old'];
		$idAula=$post['id_aula_old'];
		$cantidadHoras=$post['cantidad_horas'];
		$idHoraInicio=$post['id_hora'];
		$idHoraFin=$idHoraInicio+($cantidadHoras-1);
		
		for ($idHora=$idHoraFin; $idHora>=$idHoraInicio; $idHora--){
			$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion AND id_dia=$idDia 
				AND id_asignatura=$idAsignatura AND id_hora=$idHora";
			$options['condition'].=empty($idDocente)?" AND id_docente IS NULL":" AND id_docente=$idDocente";
			$options['condition'].=empty($idAula)?" AND id_aula IS NULL":" AND id_aula=$idAula";
			$modelHorario=Horarios::model()->findAll($options);
			if(count($modelHorario) == 0){
				return false;
			}
		}
		return $modelHorario[0];
	}


	public function validarHoraSeccion($post){
		$idPeriodo=$post['id_periodo'];
		$idSeccion=$post['id_seccion'];
		$idDia=$post['id_dia'];
		$cantidadHoras=$post['cantidad_horas'];
		$idHoraInicio=$post['id_hora'];
		$idHoraFin=$idHoraInicio+($cantidadHoras-1);

		for($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
			$options['condition']="id_periodo=$idPeriodo AND id_seccion=$idSeccion AND id_dia=$idDia 
				AND id_hora=$idHora";
			$modelHorario=Horarios::model()->findAll($options);
			if(count($modelHorario) > 0){
				return false;
			}
		}
		return true;
	}


	public function validarHoraDocente($post){
		$idPeriodo=$post['id_periodo'];
		$idDocente=$post['id_docente'];
		$idDia=$post['id_dia'];
		$cantidadHoras=$post['cantidad_horas'];
		$idHoraInicio=$post['id_hora'];
		$idHoraFin=$idHoraInicio+($cantidadHoras-1);

		for($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
			$options['condition']="id_periodo=$idPeriodo AND id_docente=$idDocente AND id_dia=$idDia 
				AND id_hora=$idHora";
			$modelHorario=Horarios::model()->findAll($options);
			if(count($modelHorario) > 0){
				return false;
			}
		}
		return true;
	}


	public function validarHoraAula($post){
		$idPeriodo=$post['id_periodo'];
		$idAula=$post['id_aula'];
		$idDia=$post['id_dia'];
		$cantidadHoras=$post['cantidad_horas'];
		$idHoraInicio=$post['id_hora'];
		$idHoraFin=$idHoraInicio+($cantidadHoras-1);

		for($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
			$options['condition']="id_periodo=$idPeriodo AND id_aula=$idAula AND id_dia=$idDia 
				AND id_hora=$idHora";
			$modelHorario=Horarios::model()->findAll($options);
			if(count($modelHorario) > 0){
				return false;
			}
		}
		return true;
	}


	public function validarHoraDocenteModificar($post){
		$idPeriodo=$post['id_periodo'];
		$idDocente=$post['id_docente'];
		$idSeccion=$post['id_seccion'];
		$idDia=$post['id_dia'];
		$cantidadHoras=$post['cantidad_horas'];
		$idHoraInicio=$post['id_hora'];
		$idHoraFin=$idHoraInicio+($cantidadHoras-1);

		for($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
			$options['condition']="id_periodo=$idPeriodo AND id_docente=$idDocente AND id_dia=$idDia 
				AND id_hora=$idHora AND id_seccion<>$idSeccion";
			$modelHorario=Horarios::model()->findAll($options);
			if(count($modelHorario) > 0){
				return false;
			}
		}
		return true;
	}


	public function validarHoraAulaModificar($post){
		$idPeriodo=$post['id_periodo'];
		$idAula=$post['id_aula'];
		$idSeccion=$post['id_seccion'];
		$idDia=$post['id_dia'];
		$cantidadHoras=$post['cantidad_horas'];
		$idHoraInicio=$post['id_hora'];
		$idHoraFin=$idHoraInicio+($cantidadHoras-1);

		for($idHora=$idHoraInicio; $idHora<=$idHoraFin; $idHora++){
			$options['condition']="id_periodo=$idPeriodo AND id_aula=$idAula AND id_dia=$idDia 
				AND id_hora=$idHora AND id_seccion <> $idSeccion";
			$modelHorario=Horarios::model()->findAll($options);
			if(count($modelHorario) > 0){
				return false;
			}
		}
		return true;
	}

	public function getSeccionesPeriodos($idPeriodo,$idDepartamento,$trayectoInicial){
		$sql="
			SELECT distinct(h.id_seccion),seccion,trayecto,s.id_departamento,departamento,anio,periodo,h.id_periodo,
				fecha_inicio,fecha_fin
			FROM horarios h
				INNER JOIN secciones s ON h.id_seccion=s.id_seccion
				INNER JOIN departamentos d ON s.id_departamento=d.id_departamento
				INNER JOIN periodos_academicos pa ON h.id_periodo=pa.id_periodo
			WHERE h.id_periodo=$idPeriodo
		";

		if(!empty($idDepartamento)){
			$sql.=" AND s.id_departamento=$idDepartamento";
		}

		if(empty($trayectoInicial)){
			$sql.=" AND s.trayecto<>0";
		}

		$sql.=" ORDER BY s.id_departamento ASC, trayecto ASC ,id_seccion ASC";
		$secciones= Yii::app()->db->createCommand($sql)->queryAll();
		return $secciones;
	}


	public function getAulasPeriodos($idPeriodo,$idSede){
		$sql="
			SELECT distinct(h.id_aula),nombre_aula,anio,periodo,h.id_periodo,
				fecha_inicio,fecha_fin,sede
			FROM horarios h
				INNER JOIN aulas a ON h.id_aula=a.id_aula
				INNER JOIN periodos_academicos pa ON h.id_periodo=pa.id_periodo
				INNER JOIN sedes s ON a.id_sede=s.id_sede
			WHERE h.id_periodo=$idPeriodo
		";

		if(!empty($idDepartamento)){
			$sql.=" AND s.id_sede=$idSede";
		}

		$sql.=" ORDER BY nombre_aula ASC";
		$aulas= Yii::app()->db->createCommand($sql)->queryAll();
		return $aulas;
	}


	public function getDocentesPeriodos($idPeriodo,$idDepartamento){
		$sql="
			SELECT distinct(h.id_docente),concat(apellido1,' ',apellido2,' ',nombre1,' ',nombre2) AS nombre_completo,apellido1,pad.id_departamento,departamento,anio,periodo,h.id_periodo,fecha_inicio,fecha_fin,cedula
			FROM horarios h
				INNER JOIN docentes doc ON h.id_docente=doc.id_docente
				INNER JOIN periodos_academicos_docentes pad ON h.id_docente=pad.id_docente AND pad.id_periodo=$idPeriodo
				INNER JOIN departamentos d ON pad.id_departamento=d.id_departamento
				INNER JOIN periodos_academicos pa ON h.id_periodo=pa.id_periodo

			WHERE h.id_periodo=$idPeriodo
		";

		if(!empty($idDepartamento)){
			$sql.=" AND pad.id_departamento=$idDepartamento";
		}

		$sql.=" ORDER BY pad.id_departamento ASC, apellido1 ASC";
		$secciones= Yii::app()->db->createCommand($sql)->queryAll();
		return $secciones;
	}

}
