<?php
class Docentes extends CActiveRecord{
	
	public function tableName(){
		return 'docentes';
	}

	
	public function rules(){
		return array(
			array('cedula, nombre1, apellido1, id_departamento, id_sexo, id_tipo_contrato, id_dedicacion_docente, id_categoria_docente, providencia', 'required'),
			array('cedula','unique'),
			array('cedula, id_sexo, id_tipo_contrato, id_dedicacion_docente, id_categoria_docente, id_departamento', 'numerical', 'integerOnly'=>true),
			array('nombre1, nombre2, apellido1, apellido2', 'length', 'max'=>100),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_docente, cedula, nombre1, nombre2, apellido1, apellido2, id_sexo, id_tipo_contrato, id_dedicacion_docente, id_categoria_docente, id_status, id_departamento', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idDepartamento' => array(self::BELONGS_TO, 'Departamentos', 'id_departamento'),
			'idTipoContrato' => array(self::BELONGS_TO, 'TiposContratos', 'id_tipo_contrato'),
			'idDedicacionDocente' => array(self::BELONGS_TO, 'DedicacionesDocentes', 'id_dedicacion_docente'),
			'idCategoriaDocente' => array(self::BELONGS_TO, 'CategoriasDocentes', 'id_categoria_docente'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_docente' => 'Id',
			'cedula' => 'Cedula',
			'nombre1' => 'Primer Nombre',
			'nombre2' => 'Segundo Nombre',
			'apellido1' => 'Primer Apellido',
			'apellido2' => 'Segundo Apellido',
			'id_sexo' => 'Sexo',
			'id_tipo_contrato' => 'Tipo Contrato',
			'id_dedicacion_docente' => 'Dedicación Docente',
			'id_categoria_docente' => 'Categoría Docente',
			'id_status' => 'Estatus',
			'id_departamento' => 'Departamento',
		);
	}


	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_docente',$this->id_docente);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre1',strtoupper($this->nombre1),true);
		$criteria->compare('nombre2',strtoupper($this->nombre2),true);
		$criteria->compare('apellido1',strtoupper($this->apellido1),true);
		$criteria->compare('apellido2',strtoupper($this->apellido2),true);
		$criteria->compare('id_sexo',$this->id_sexo);
		$criteria->compare('id_tipo_contrato',$this->id_tipo_contrato);
		$criteria->compare('id_dedicacion_docente',$this->id_dedicacion_docente);
		$criteria->compare('id_categoria_docente',$this->id_categoria_docente);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('id_departamento',$this->id_departamento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getNombres(){
		return $this->nombre1." ".$this->nombre2;
	}


	public function getApellidos(){
		return $this->apellido1." ".$this->apellido2;
	}


	public function getSexo(){
		return $this->id_sexo==1?'Masculino':'Femenino';
	}


	public function getTipoContrato(){
		if(empty($this->id_tipo_contrato)){
			return "No Asignado";
		}
		return $this->idTipoContrato->tipo_contrato;
	}


	public function getDedicacionDocente(){
		if(empty($this->id_dedicacion_docente)){
			return "No Asignado";
		}
		return $this->idDedicacionDocente->dedicacion_docente;
	}


	public function getCategoriaDocente(){
		if(empty($this->id_categoria_docente)){
			return "No Asignado";
		}
		return $this->idCategoriaDocente->categoria_docente;
	}


	public function getCedulaDatos(){
		return $this->cedula." - ".$this->apellidos." ".$this->nombres;
	}


	public function getNombreCompleto(){
		return $this->apellidos." ".$this->nombres;
	}

	public function getApellidoNombre(){
		return $this->apellido1." ".$this->nombre1;
	}


	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activo</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactivo</span>";
			break;
		}
	}


	public function getListDocentes($idDepartamento = null){
   		$options['order']='"idDocente"."apellido1" ASC';
   		if(!is_null($idDepartamento)){
   			$options['condition']="t.id_departamento=$idDepartamento";
   		}
   		$docentes=PeriodosAcademicosDocentes::model()->with('idDocente')->findAll($options);
		return CHtml::listData($docentes,'id_docente','idDocente.cedulaDatos');
   	
	}

}
