<?php
class Turnos extends CActiveRecord{
	
	public function tableName(){
		return 'turnos';
	}

	
	public function rules(){
		return array(
			array('id_turno, turno', 'required'),
			array('id_turno', 'numerical', 'integerOnly'=>true),
			array('turno', 'length', 'max'=>200),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_turno, turno, id_status', 'safe', 'on'=>'search'),
		);
	}


	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_turno' => 'Id',
			'turno' => 'Turno',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_turno',$this->id_turno);
		$criteria->compare('turno',$this->turno,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activo</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactivo</span>";
			break;
		}
	}

	public static function getTurnos($idStatus = null){
   		$options['order']="id_turno ASC";
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_status=$status";
   		}
   		$turnos=Turnos::model()->findAll($options);
		return CHtml::listData($turnos,'id_turno','turno');
   	}

}
