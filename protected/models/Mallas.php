<?php
class Mallas extends CActiveRecord{
	
	public function tableName(){
		return 'mallas';
	}

	
	public function rules(){
		return array(
			array('nombre_malla, fecha_creacion, id_departamento', 'required'),
			array('id_departamento', 'numerical', 'integerOnly'=>true),
			array('nombre_malla', 'length', 'max'=>200),
			array('id_status', 'safe'),
			array('id_malla, nombre_malla, fecha_creacion, id_departamento, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idDepartamento' => array(self::BELONGS_TO, 'Departamentos', 'id_departamento'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_malla' => 'Id Malla',
			'nombre_malla' => 'Nombre Malla',
			'fecha_creacion' => 'Fecha Creación',
			'id_departamento' => 'Departamento',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_malla',$this->id_malla);
		$criteria->compare('nombre_malla',$this->nombre_malla,true);
		$criteria->compare('fecha_creacion',$this->fecha_creacion,true);
		$criteria->compare('id_departamento',$this->id_departamento);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}

	public function getMallasActivas($idDepartamento){
		$options['condition']="id_departamento=$idDepartamento AND id_status=TRUE";
		$options['order']="fecha_creacion DESC";
		$mallas=Mallas::model()->findAll($options);
		return $mallas;
	}


	public function getUltimaMalla($idDepartamento){
		$mallas=$this->getMallasActivas($idDepartamento);
		if(count($mallas) > 0){
			return $mallas[0]->id_malla;
		}else{
			return false;
		}
	}

}
