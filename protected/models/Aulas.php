<?php
class Aulas extends CActiveRecord{
	
	public function tableName(){
		return 'aulas';
	}

	
	public function rules(){
		return array(
			array('nombre_aula, capacidad, id_sede, piso', 'required'),
			array('capacidad, id_sede', 'numerical', 'integerOnly'=>true),
			array('nombre_aula, piso', 'length', 'max'=>100),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_aula, nombre_aula, capacidad, id_sede, piso, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idSede' => array(self::BELONGS_TO, 'Sedes', 'id_sede'),
		);
	}


	public function attributeLabels(){
		return array(
			'id_aula' => 'Id',
			'nombre_aula' => 'Nombre Aula',
			'capacidad' => 'Capacidad',
			'id_sede' => 'Sede',
			'piso' => 'Piso',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_aula',$this->id_aula);
		$criteria->compare('nombre_aula',$this->nombre_aula,true);
		$criteria->compare('capacidad',$this->capacidad);
		$criteria->compare('id_sede',$this->id_sede);
		$criteria->compare('piso',$this->piso,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}


	public function getListAulas($idStatus = null){
   		$options['order']='nombre_aula ASC';
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_status=$status";
   		}
   		$aulas=Aulas::model()->findAll($options);
		return CHtml::listData($aulas,'id_aula','nombre_aula');
   	
	}

}
