<?php
class DedicacionesDocentes extends CActiveRecord{
	
	public function tableName(){
		return 'dedicaciones_docentes';
	}

	
	public function rules(){
		return array(
			array('id_dedicacion_docente, dedicacion_docente, cantidad_horas', 'required'),
			array('id_dedicacion_docente', 'numerical', 'integerOnly'=>true),
			array('dedicacion_docente', 'length', 'max'=>200),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_dedicacion_docente, dedicacion_docente, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_dedicacion_docente' => 'Id',
			'dedicacion_docente' => 'Dedicacion Docente',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){

		$criteria=new CDbCriteria;

		$criteria->compare('id_dedicacion_docente',$this->id_dedicacion_docente);
		$criteria->compare('dedicacion_docente',$this->dedicacion_docente,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}


	public static function getDedicacionesDocentes($idStatus = null){
   		$options['order']="id_dedicacion_docente ASC";
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_status=$status";
   		}
   		$dedicacionesDocentes=DedicacionesDocentes::model()->findAll($options);
		return CHtml::listData($dedicacionesDocentes,'id_dedicacion_docente','dedicacion_docente');
   	}

}
