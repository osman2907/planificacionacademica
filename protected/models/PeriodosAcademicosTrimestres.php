<?php
class PeriodosAcademicosTrimestres extends CActiveRecord{
	
	public function tableName(){
		return 'periodos_academicos_trimestres';
	}

	
	public function rules(){
		return array(
			array('periodo, trayecto, trimestre_carrera', 'required'),
			array('periodo, trayecto, trimestre_carrera', 'numerical', 'integerOnly'=>true),
			array('periodo, trayecto, trimestre_carrera', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'periodo' => 'Periodo',
			'trayecto' => 'Trayecto',
			'trimestre_carrera' => 'Trimestre Carrera',
		);
	}

	
	public function search(){

		$criteria=new CDbCriteria;

		$criteria->compare('periodo',$this->periodo);
		$criteria->compare('trayecto',$this->trayecto);
		$criteria->compare('trimestre_carrera',$this->trimestre_carrera);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getTrimestre($periodo,$trayecto){
		$options['condition']="periodo=$periodo AND trayecto=$trayecto";
		$model=PeriodosAcademicosTrimestres::model()->find($options);
		return $model->trimestre_carrera;
	}
}
