<?php
class Horas extends CActiveRecord{
	
	public function tableName(){
		return 'horas';
	}

	
	public function rules(){
		return array(
			array('hora_inicio, hora_fin', 'required'),
			array('id_hora, hora_inicio, hora_fin', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'horarioses' => array(self::HAS_MANY, 'Horarios', 'id_hora'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_hora' => 'Id Hora',
			'hora_inicio' => 'Hora Inicio',
			'hora_fin' => 'Hora Fin',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_hora',$this->id_hora);
		$criteria->compare('hora_inicio',$this->hora_inicio,true);
		$criteria->compare('hora_fin',$this->hora_fin,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getHora_inicio_12(){
		return date("h.i a",strtotime($this->hora_inicio));
	}

	public static function getHorasClases(){
		$horasClases=Horas::model()->findAll(array('order'=>'hora_inicio ASC'));

		foreach ($horasClases as $hora){
			$retorno[]=array(
				'id_hora'=>$hora->id_hora,
				'dow'=>array(1,2,3,4,5,6),
				'start'=>$hora->hora_inicio,
				'end'=>$hora->hora_fin
			);
		}
		return $retorno;
	}

	
	public function getListHorasInicio($hora){
		$options['condition']="hora='$hora'";
		$options['order']="id_hora ASC";
		$horasInicio=HorasBloques::model()->findAll($options);
		return CHtml::listData($horasInicio,'id_hora',"idHora.hora_inicio_12");
	}
}
