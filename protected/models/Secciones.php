<?php
class Secciones extends CActiveRecord{
	
	public function tableName(){
		return 'secciones';
	}

	
	public function rules(){
		return array(
			array('seccion, id_departamento, id_turno, trayecto', 'required'),
			array('id_departamento, id_turno, trayecto', 'numerical', 'integerOnly'=>true),
			array('id_status', 'safe'),
			array('id_seccion, seccion, id_departamento, id_turno, trayecto, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idDepartamento' => array(self::BELONGS_TO, 'Departamentos', 'id_departamento'),
			'idTurno' => array(self::BELONGS_TO, 'Turnos', 'id_turno'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_seccion' => 'Id Seccion',
			'seccion' => 'Sección',
			'id_departamento' => 'Departamento',
			'id_turno' => 'Turno',
			'trayecto' => 'Trayecto',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_seccion',$this->id_seccion);
		$criteria->compare('seccion',$this->seccion,true);
		$criteria->compare('id_departamento',$this->id_departamento);
		$criteria->compare('id_turno',$this->id_turno);
		$criteria->compare('trayecto',$this->trayecto);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}

}
