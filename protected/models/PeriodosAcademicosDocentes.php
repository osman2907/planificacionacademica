<?php
class PeriodosAcademicosDocentes extends CActiveRecord{
	public $docente;
	public $periodo;
	public $departamento;
	
	public function tableName(){
		return 'periodos_academicos_docentes';
	}

	
	public function rules(){
		return array(
			array('id_periodo, id_docente, id_tipo_contrato, id_dedicacion_docente, id_categoria_docente, id_departamento', 'required'),
			array('id_periodo, id_docente, id_tipo_contrato, id_dedicacion_docente, id_categoria_docente, id_departamento', 'numerical', 'integerOnly'=>true),
			array('id_periodo, id_docente, id_tipo_contrato, id_dedicacion_docente, id_categoria_docente, id_departamento', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idTipoContrato' => array(self::BELONGS_TO, 'TiposContratos', 'id_tipo_contrato'),
			'idDedicacionDocente' => array(self::BELONGS_TO, 'DedicacionesDocentes', 'id_dedicacion_docente'),
			'idCategoriaDocente' => array(self::BELONGS_TO, 'CategoriasDocentes', 'id_categoria_docente'),
			'idDepartamento' => array(self::BELONGS_TO, 'Departamentos', 'id_departamento'),
			'idDocente' => array(self::BELONGS_TO, 'Docentes', 'id_docente'),
			'idPeriodo' => array(self::BELONGS_TO, 'PeriodosAcademicos', 'id_periodo')
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_periodo' => 'Id Periodo',
			'id_docente' => 'Id Docente',
			'id_tipo_contrato' => 'Tipo Contrato',
			'id_dedicacion_docente' => 'Dedicación Docente',
			'id_categoria_docente' => 'Categoría Docente',
			'id_departamento' => 'Id Departamento',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_periodo',$this->id_periodo);
		$criteria->compare('id_docente',$this->id_docente);
		$criteria->compare('id_tipo_contrato',$this->id_tipo_contrato);
		$criteria->compare('id_dedicacion_docente',$this->id_dedicacion_docente);
		$criteria->compare('id_categoria_docente',$this->id_categoria_docente);
		$criteria->compare('id_departamento',$this->id_departamento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function actualizarDocentes($id=null){
		$modelDocentes=Docentes::model()->findAll(array('condition'=>"id_status=true"));
		$transaction=$this->dbConnection->beginTransaction();
		try{
			
	        foreach ($modelDocentes as $docente){
	        	$options['condition']="id_periodo=$id AND id_docente=$docente->id_docente";
	        	$consulta=PeriodosAcademicosDocentes::model()->findAll($options);

	        	if(count($consulta) == 0){
					$modelPeriodoDocente=new PeriodosAcademicosDocentes;
					$modelPeriodoDocente->id_periodo=$id;
					$modelPeriodoDocente->id_docente=$docente->id_docente;
					$modelPeriodoDocente->id_tipo_contrato=$docente->id_tipo_contrato;
					$modelPeriodoDocente->id_dedicacion_docente=$docente->id_dedicacion_docente;
					$modelPeriodoDocente->id_categoria_docente=$docente->id_categoria_docente;
					$modelPeriodoDocente->id_departamento=$docente->id_departamento;
					if(!$modelPeriodoDocente->save()){
						throw new CHttpException(403,'Error registrando docentes por período');
					}
				}

			}
	        $transaction->commit();
	        return true;
		}
		catch(Exception $e){
		    $transaction->rollBack();
		    return false;
		}
	}

}
