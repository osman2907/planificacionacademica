<?php
class PeriodosAcademicosSecciones extends CActiveRecord{

	public $periodo;
	public $seccion;
	public $trayecto;
	
	public function tableName(){
		return 'periodos_academicos_secciones';
	}

	
	public function rules(){
		return array(
			array('id_periodo, id_seccion, id_malla, trimestre', 'required'),
			array('id_periodo, id_seccion', 'numerical', 'integerOnly'=>true),
			array('id_periodo, id_seccion, id_malla', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idSeccion' => array(self::BELONGS_TO, 'Secciones', 'id_seccion'),
			'idPeriodo' => array(self::BELONGS_TO, 'PeriodosAcademicos', 'id_periodo'),
			'idMalla' => array(self::BELONGS_TO, 'Mallas', 'id_malla')
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_periodo' => 'Id Periodo',
			'id_seccion' => 'Id Seccion',
			'id_malla' => 'Malla',
			'seccion' => 'Sección'
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_periodo',$this->id_periodo);
		$criteria->compare('id_seccion',$this->id_seccion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function actualizarSecciones($id=null){
		$modelSecciones=Secciones::model()->findAll(array('condition'=>"id_status=true"));
		$modelPeriodoAcademico=PeriodosAcademicos::model()->findByPk($id);
		$transaction=$this->dbConnection->beginTransaction();
		try{
	        foreach ($modelSecciones as $seccion){
	        	$options['condition']="id_periodo=$id AND id_seccion=$seccion->id_seccion";
	        	$consulta=PeriodosAcademicosSecciones::model()->findAll($options);

	        	if(count($consulta) == 0){
					$modelPeriodoSecciones=new PeriodosAcademicosSecciones;
					$modelPeriodoSecciones->id_periodo=$id;
					$modelPeriodoSecciones->id_seccion=$seccion->id_seccion;
					$modelMallas=new Mallas;
					$idMalla=$modelMallas->getUltimaMalla($seccion->id_departamento);
					if($idMalla){
						$trimestre=PeriodosAcademicosTrimestres::model()->getTrimestre($modelPeriodoAcademico->periodo,$seccion->trayecto);
						$modelPeriodoSecciones->id_malla=$idMalla;
						$modelPeriodoSecciones->trimestre=$trimestre;

						if(!$modelPeriodoSecciones->save()){
							throw new CHttpException(403,'Error registrando secciones por período');
						}
					}
				}
			}
	        $transaction->commit();
	        return true;
		}
		catch(Exception $e){
		    $transaction->rollBack();
		    return false;
		}
	}

}
