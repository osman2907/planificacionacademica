<?php
class PeriodosAcademicos extends CActiveRecord{
	
	public function tableName(){
		return 'periodos_academicos';
	}

	
	public function rules(){
		return array(
			array('anio, periodo, fecha_inicio, fecha_fin, fecha_inicio_plan, fecha_fin_plan', 'required'),
			array('anio, periodo', 'numerical', 'integerOnly'=>true),
			array('fecha_fin, fecha_inicio_plan, fecha_fin_plan, id_status', 'safe'),
			array('id_periodo, anio, periodo, fecha_inicio, fecha_fin, fecha_inicio_plan, fecha_fin_plan, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_periodo' => 'Id',
			'anio' => 'Año',
			'periodo' => 'Período',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_fin' => 'Fecha Fin',
			'fecha_inicio_plan' => 'Fecha Inicio Planificación',
			'fecha_fin_plan' => 'Fecha Fin Planificación',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){

		$criteria=new CDbCriteria;

		$criteria->compare('id_periodo',$this->id_periodo);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('periodo',$this->periodo);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_fin',$this->fecha_fin,true);
		$criteria->compare('fecha_inicio_plan',$this->fecha_inicio_plan,true);
		$criteria->compare('fecha_fin_plan',$this->fecha_fin_plan,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activo</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactivo</span>";
			break;
		}
	}


	public function getPeriodoRomano(){
		$romano=$this->getRomano();
		return "<span class='negrita'>$romano</span>";
	}

	public function getRomano(){
		switch ($this->periodo) {
			case 1:
				return "I";
			break;
			
			case 2:
				return "II";
			break;

			case 3:
				return "III";
			break;
		}
	}


	public function getPeriodoActivo(){
		$periodo=PeriodosAcademicos::model()->findAll(array('condition'=>"id_status=TRUE"));
		if(count($periodo) == 1){
			return $periodo[0];
		}else{
			return false;
		}
	}

	public function getAnioPeriodo(){
		return $this->anio."-".$this->romano;
	}

	public static function getPeriodosAcademicos(){
		$options['order']="anio DESC, periodo DESC";
   		$periodosAcademicos=PeriodosAcademicos::model()->findAll($options);
		return CHtml::listData($periodosAcademicos,'id_periodo','anioPeriodo');
   	}
}
