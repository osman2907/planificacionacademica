<?php
class Sedes extends CActiveRecord{

	public function tableName(){
		return 'sedes';
	}

	public function rules(){
		return array(
			array('sede, direccion', 'required'),
			array('sede', 'length', 'max'=>200),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_sede, sede, direccion, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_sede' => 'Id',
			'sede' => 'Sede',
			'direccion' => 'Dirección',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_sede',$this->id_sede);
		$criteria->compare('sede',$this->sede,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	
	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}


	public static function getSedes($idStatus = null){
   		$options['order']="sede ASC";
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_status=$status";
   		}
   		$sedes=Sedes::model()->findAll($options);
		return CHtml::listData($sedes,'id_sede','sede');
   	}
}
