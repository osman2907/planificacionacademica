<?php
class HorasBloques extends CActiveRecord{
	
	public function tableName(){
		return 'horas_bloques';
	}

	
	public function rules(){
		return array(
			array('hora, id_hora', 'required'),
			array('id_hora', 'numerical', 'integerOnly'=>true),
			array('hora, id_hora', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idHora' => array(self::BELONGS_TO, 'Horas', 'id_hora'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'hora' => 'Hora',
			'id_hora' => 'Id Hora',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('hora',$this->hora,true);
		$criteria->compare('id_hora',$this->id_hora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
	
}
