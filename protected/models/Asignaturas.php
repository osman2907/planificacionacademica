<?php
class Asignaturas extends CActiveRecord{
	
	public function tableName(){
		return 'asignaturas';
	}

	
	public function rules(){
		return array(
			array('id_malla, codigo, asignatura, trayecto, trimestre, trimestre_ejecucion, horas_teoricas, horas_practicas, unidades_credito', 'required'),
			array('codigo','unique'),
			array('id_malla, trayecto, trimestre, trimestre_ejecucion, horas_teoricas, horas_practicas, unidades_credito', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>20),
			array('asignatura', 'length', 'max'=>200),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_asignatura, id_malla, codigo, asignatura, trayecto, trimestre, trimestre_ejecucion, horas_teoricas, horas_practicas, unidades_credito, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idMalla' => array(self::BELONGS_TO, 'Mallas', 'id_malla'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_asignatura' => 'Id Asignatura',
			'id_malla' => 'Id Malla',
			'codigo' => 'Código',
			'asignatura' => 'Nombre Asignatura',
			'trayecto' => 'Trayecto',
			'trimestre' => 'Trimestre de la Carrera',
			'trimestre_ejecucion' => 'Trimestre del Año',
			'horas_teoricas' => 'Horas Teóricas',
			'horas_practicas' => 'Horas Prácticas',
			'unidades_credito' => 'Unidades de Crédito',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){

		$criteria=new CDbCriteria;

		$criteria->compare('id_asignatura',$this->id_asignatura);
		$criteria->compare('id_malla',$this->id_malla);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('asignatura',$this->asignatura,true);
		$criteria->compare('trayecto',$this->trayecto);
		$criteria->compare('trimestre',$this->trimestre);
		$criteria->compare('trimestre_ejecucion',$this->trimestre_ejecucion);
		$criteria->compare('horas_teoricas',$this->horas_teoricas);
		$criteria->compare('horas_practicas',$this->horas_practicas);
		$criteria->compare('unidades_credito',$this->unidades_credito);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function getCodigoAsignatura(){
		return $this->codigo." - ".$this->asignatura;
	}

	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}


	public function valNombreAsignatura(){
		$fechaInicio=$this->fecha_inicio;
		if(!empty($fechaInicio)){
			$fechaHoy=date("Y-m-d");
			if($fechaInicio <= $fechaHoy){
				$this->addError('fecha_inicio', 'Fecha de inicio debe ser mayor a la fecha actual');
			}
		}
	}


	public static function getListAsignaturasMallas($idMalla,$idStatus=null){
   		$options['order']="codigo ASC, asignatura ASC";
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_malla=$idMalla AND id_status=$status";
   		}else{
   			$options['condition']="id_malla=$idMalla";
   		}
   		$asignaturas=Asignaturas::model()->findAll($options);
		return CHtml::listData($asignaturas,'id_asignatura','codigoAsignatura');
   	}

}
