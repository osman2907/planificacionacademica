<?php
class CategoriasDocentes extends CActiveRecord{
	
	public function tableName(){
		return 'categorias_docentes';
	}

	
	public function rules(){
		return array(
			array('id_categoria_docente, categoria_docente', 'required'),
			array('id_categoria_docente', 'numerical', 'integerOnly'=>true),
			array('categoria_docente', 'length', 'max'=>200),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_categoria_docente, categoria_docente, id_status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_categoria_docente' => 'Id',
			'categoria_docente' => 'Categoría Docente',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_categoria_docente',$this->id_categoria_docente);
		$criteria->compare('categoria_docente',$this->categoria_docente,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}


	public static function getCategoriasDocentes($idStatus = null){
   		$options['order']="id_categoria_docente ASC";
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_status=$status";
   		}
   		$categoriasDocentes=CategoriasDocentes::model()->findAll($options);
		return CHtml::listData($categoriasDocentes,'id_categoria_docente','categoria_docente');
   	}


}
