<?php
class Departamentos extends CActiveRecord{
	
	public function tableName(){
		return 'departamentos';
	}

	
	public function rules(){
		return array(
			array('id_departamento, departamento', 'required'),
			array('id_departamento', 'numerical', 'integerOnly'=>true),
			array('departamento', 'length', 'max'=>100),
			array('id_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_departamento, departamento, id_status', 'safe', 'on'=>'search'),
		);
	}


	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_departamento' => 'Id',
			'departamento' => 'Departamento',
			'id_status' => 'Estatus',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_departamento',$this->id_departamento);
		$criteria->compare('departamento',$this->departamento,true);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function getStatusBandeja(){
		switch ($this->id_status) {
			case 1:
				return "<span class='letra-verde negrita'>Activa</span>";
			break;
			
			case 0:
				return "<span class='letra-roja negrita'>Inactiva</span>";
			break;
		}
	}


	public static function getDepartamentos($idStatus = null){
   		$options['order']="departamento ASC";
   		if(!is_null($idStatus)){
   			$status=$idStatus?"TRUE":"FALSE";
   			$options['condition']="id_status=$status";
   		}
   		$departamentos=Departamentos::model()->findAll($options);
		return CHtml::listData($departamentos,'id_departamento','departamento');
   	}


   	public static function obtenerDepartamentos($idDepartamento = null){
   		$options['order']="id_departamento ASC";
   		if(!is_null($idDepartamento)){
   			$options['condition']="id_departamento=$idDepartamento AND id_status=TRUE";
   		}else{
   			$options['condition']="id_status=TRUE";
   		}
   		$departamentos=Departamentos::model()->findAll($options);
		return $departamentos;
   	}


   	public function getModelDepartamentos(){
   		$departamentos=Departamentos::model()->findAll(array('order'=>'id_departamento ASC'));
   		return $departamentos;
   	}
	
}
