#!/bin/bash
# -*- ENCODING: UTF-8 -*-

#Creando la carpeta assets
mkdir assets
echo "Carpeta assets creada"

chmod -R 777 assets
echo "Permisología aplicada a la carpeta assets"

mkdir protected/runtime
echo "carpeta /protected/runtime creada"

chmod -R 777 protected/runtime
echo "permisología aplicada a la carpeta protected/runtime"

chmod -R 777 uploads
echo "permisología aplicada a la carpeta uploads"

chmod -R 777 reportes
echo "permisología aplicada a la carpeta reportes"

echo "Instalación finalizada"


