#!/bin/bash
# -*- ENCODING: UTF-8 -*-

#Aplicando Permiso a la carpeta assets
chmod -R 777 assets
echo "Permisología aplicada a la carpeta assets"

#Aplicando Permiso a la carpeta runtime
chmod -R 777 protected/runtime
echo "permisología aplicada a la carpeta protected/runtime"

#Aplicando Permiso a la carpeta uploads
chmod -R 777 uploads
echo "permisología aplicada a la carpeta uploads"

#Aplicando Permiso a la carpeta reportes
chmod -R 777 reportes
echo "permisología aplicada a la carpeta reportes"
