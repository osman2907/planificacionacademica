--Cambios 2018-02-25
CREATE TABLE periodos_academicos_docentes
(
  id_periodo integer NOT NULL,
  id_docente integer NOT NULL,
  id_tipo_contrato integer NOT NULL,
  id_dedicacion_docente integer NOT NULL,
  id_categoria_docente integer NOT NULL,
  id_departamento integer NOT NULL,
  CONSTRAINT periodos_academicos_docentes_pkey PRIMARY KEY (id_periodo, id_docente),
  CONSTRAINT periodos_academicos_docentes_id_categoria_docente_fkey FOREIGN KEY (id_categoria_docente)
      REFERENCES categorias_docentes (id_categoria_docente) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT periodos_academicos_docentes_id_dedicacion_docente_fkey FOREIGN KEY (id_dedicacion_docente)
      REFERENCES dedicaciones_docentes (id_dedicacion_docente) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT periodos_academicos_docentes_id_departamento_fkey FOREIGN KEY (id_departamento)
      REFERENCES departamentos (id_departamento) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT periodos_academicos_docentes_id_docente_fkey FOREIGN KEY (id_docente)
      REFERENCES docentes (id_docente) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT periodos_academicos_docentes_id_periodo_fkey FOREIGN KEY (id_periodo)
      REFERENCES periodos_academicos (id_periodo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT periodos_academicos_docentes_id_tipo_contrato_fkey FOREIGN KEY (id_tipo_contrato)
      REFERENCES tipos_contratos (id_tipo_contrato) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE periodos_academicos_docentes
  OWNER TO postgres;





CREATE TABLE secciones
(
  id_seccion serial NOT NULL,
  seccion character varying NOT NULL,
  id_departamento integer NOT NULL,
  id_turno integer NOT NULL,
  trayecto integer NOT NULL,
  id_status boolean NOT NULL DEFAULT true,
  CONSTRAINT secciones_pkey PRIMARY KEY (id_seccion),
  CONSTRAINT secciones_id_departamento_fkey FOREIGN KEY (id_departamento)
      REFERENCES departamentos (id_departamento) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT secciones_id_turno_fkey FOREIGN KEY (id_turno)
      REFERENCES turnos (id_turno) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE secciones
  OWNER TO postgres;



--Cambios 2018-02-26

CREATE TABLE public.dias
(
  id_dia integer NOT NULL,
  dia character varying NOT NULL,
  CONSTRAINT dias_pkey PRIMARY KEY (id_dia)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.dias
  OWNER TO postgres;



CREATE TABLE public.horas
(
  id_hora serial NOT NULL,
  hora_inicio time without time zone NOT NULL,
  hora_fin time without time zone NOT NULL,
  CONSTRAINT horas_pkey PRIMARY KEY (id_hora)
)
WITH (
  OIDS=FALSE
);

  
CREATE TABLE public.horarios
(
  id_periodo integer NOT NULL,
  id_seccion integer NOT NULL,
  id_dia integer NOT NULL,
  id_hora integer NOT NULL,
  id_asignatura integer NOT NULL,
  id_docente integer,
  id_aula integer,
  CONSTRAINT horarios_pkey PRIMARY KEY (id_periodo, id_seccion, id_dia, id_hora, id_asignatura),
  CONSTRAINT horarios_id_asignatura_fkey FOREIGN KEY (id_asignatura)
      REFERENCES public.asignaturas (id_asignatura) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT horarios_id_dia_fkey FOREIGN KEY (id_dia)
      REFERENCES public.dias (id_dia) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT horarios_id_hora_fkey FOREIGN KEY (id_hora)
      REFERENCES public.horas (id_hora) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT horarios_id_periodo_fkey FOREIGN KEY (id_periodo)
      REFERENCES public.periodos_academicos (id_periodo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT horarios_id_seccion_fkey FOREIGN KEY (id_seccion)
      REFERENCES public.secciones (id_seccion) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.horarios
  OWNER TO postgres;



--Cambios 2018-03-01
ALTER TABLE periodos_academicos_secciones
  ADD COLUMN id_malla integer NOT NULL;
ALTER TABLE periodos_academicos_secciones
  ADD FOREIGN KEY (id_malla) REFERENCES mallas (id_malla) ON UPDATE NO ACTION ON DELETE NO ACTION;



CREATE TABLE public.periodos_academicos_trimestres
(
  periodo integer NOT NULL,
  trayecto integer NOT NULL,
  trimestre_carrera integer NOT NULL,
  CONSTRAINT periodo_academicos_trimestres_pkey PRIMARY KEY (periodo, trayecto)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.periodos_academicos_trimestres
  OWNER TO postgres;
COMMENT ON TABLE public.periodos_academicos_trimestres
  IS 'Esta tabla especifica cuales son los trimestres de las carreras que arrancan en cada trimestre del año.';



ALTER TABLE public.periodos_academicos_secciones
  ADD COLUMN trimestre integer NOT NULL;


CREATE TABLE public.horas_bloques
(
   hora time without time zone NOT NULL, 
   id_hora integer NOT NULL, 
   PRIMARY KEY (hora, id_hora), 
   FOREIGN KEY (id_hora) REFERENCES public.horas (id_hora) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;


--Cambios 2018-03-16
ALTER TABLE dedicaciones_docentes
  ADD COLUMN cantidad_horas integer NOT NULL DEFAULT 0;

ALTER TABLE dedicaciones_docentes
   ALTER COLUMN cantidad_horas DROP DEFAULT;


--Cambios 2018-06-05
ALTER TABLE public.docentes
  ADD COLUMN providencia boolean;