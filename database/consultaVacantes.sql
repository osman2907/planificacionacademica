﻿SELECT min(seccion) AS "seccion",min(s.trayecto) AS "trayecto",min(pas.trimestre) AS "trimestre",
	min(asignatura) AS "asignatura",min(hora_inicio) AS "hora_inicio",max(hora_fin) AS "hora_fin"
FROM horarios h
	INNER JOIN secciones s ON h.id_seccion=s.id_seccion
	INNER JOIN asignaturas a ON h.id_asignatura=a.id_asignatura
	INNER JOIN horas hor ON h.id_hora=hor.id_hora
	INNER JOIN periodos_academicos_secciones pas ON s.id_seccion=pas.id_seccion AND pas.id_periodo=2
WHERE id_docente is null AND h.id_periodo=2 AND id_departamento=7
GROUP BY h.id_periodo,h.id_seccion,h.id_asignatura
ORDER BY trayecto ASC, trimestre ASC, asignatura